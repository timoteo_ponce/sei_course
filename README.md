modulo 1 - Gestion de proyectos de software
	Docente: Ing. José Antonio Benavente
	
modulo 2 - Ingenieria de requerimientos
	Docente: Ing. Vladimir Calderon
	
modulo 3 - NN
	Docente: Ing. Juan Banda
	
modulo 4 - Arquitectura del software
	Docente: Ing. Pablo Azero (Jalasoft)
	
modulo 5 - Desarrollo basado en componentes
	Docente: Ing. Renato Bejarano

modulo 6 - NN
	Docente: Ing. Davor Pavisic (Jalasoft)

modulo 7 - Aseguramiento de calidad
	Docente: Ing. Jhonny Paniagua
	
modulo 8 - Pruebas en el software
	Docente: NN (Jalasoft)
	
modulo 9 - 	Pruebas en el software II 
	Docente: NN (Jalasoft)
		
modulo 10 - Seguridad y redes
	Docente: Ing. Esteban Saavedra

modulo 11 - Taller de tesis I
	Docente: Ing. Javier Alanoca
	
modulo 12 - Seguridad en sistemas de software
	Docente: Ing. Esteban Saavedra

modulo 13 - misc
	Docente: Ing. Esteban Saavedra
	
modulo 14 - Plan de calidad 
	Docente: Ing. Jhonny Paniagua
	
modulo 15 - Taller de tesis II
	Docente: MSc. Saúl Walter Molina Gómez
	
modulo 16 - Data mining
	Docente: Ing. Jhonny Paniagua