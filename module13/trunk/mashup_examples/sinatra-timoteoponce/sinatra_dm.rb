# Sample Sinatra app with DataMapper
# Based on http://sinatra-book.gittr.com/ DataMapper example
require 'sinatra'
require "sinatra/jsonp"
require 'json'
require_relative 'model/database.rb'

set :protection, except: :ip_spoofing

get '/searchLote/:name' do
  content_type :json
  filter = "%" + params[:name] + "%"
  foundLotes = Lote.all(:glose.like => filter) | Lote.all(:address.like => filter, :limit => 10)
  puts "Found lotes : #{foundLotes.length} for '#{filter}'"
  results = []
    
  foundLotes.each{ | item |
  	results << { "info" => {
			"id" => "#{item.id}", 
			"address" => "#{item.address}", 
  			"surface" => "#{item.surface}", 
  			"glose" => "#{item.glose}", 
  			"type" => "#{item.type}"
		      },
		      "bounds" => [
			{ "northeast" =>  {"lat" => "#{item.lat}", "lng" => "#{item.lng}"}  },
			{ "southwest" =>  {"lat" => "#{item.lat}", "lng" => "#{item.lng}"}  }
		      ],
		      "location" => {"lat" => "#{item.lat}", "lng" => "#{item.lng}"} 
		    }
  }  
  data = { "results" => results}
  JSONP JSON.pretty_generate( data ) 
end


######################
# Render plain HTML pages
######################
get '/search' do
  send_file 'search.html'
end

get '/contacts' do
  send_file 'contacts.html'
end

get '/tucasa' do
  send_file 'tuacasa.html'
end

get '/' do
  send_file 'index.html'
end

##########################
get '/cv' do
	erb :cv_index
end

post '/cv' do
	post = params[:post]
	query = post.map{|k,v| "#{k}=#{v}"}.join('&')
	redirect "/cv_es?#{query}"
end


get '/cv_es' do
	@cv_title = params[ :title_name ]
	@params = params
	@experiences = params[ :experiences ].split(',')
	@references = params[ :references ].split(',')
	@skills = params[ :skills ].split(',')
	@contacts = params[ :contacts ].split(',')
	erb :cv_es, :layout => :cv_es_layout
end

get '/cv_en' do
	@cv_title = params[ :title_name ]
	@params = params
	@experiences = params[ :experiences ].split(',')
	@references = params[ :references ].split(',')
	@skills = params[ :skills ].split(',')
	@contacts = params[ :contacts ].split(',')
	erb :cv_en, :layout => :cv_en_layout
end

