require 'rubygems'
require 'bundler'
require 'data_mapper'
require_relative 'csv_reader.rb'

Bundler.require
DataMapper::Logger.new($stdout, :debug)

if ENV['VCAP_SERVICES'].nil?
  DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/blog.db")
  puts "Storing everything into a database in sqlite3://#{Dir.pwd}/blog.db"
else
    services = JSON.parse(ENV['VCAP_SERVICES'])
    puts "These are the services #{services}"
    postgresql_key = services.keys.select { |svc| svc =~ /postgresql/i }.first
    postgresql = services[postgresql_key].first['credentials']
    postgresql_conn = "postgres://"+postgresql['user']+":"+postgresql['password']+ 
      "@"+postgresql['host']+":"+postgresql['port'].to_s+"/"+postgresql['name']
    DataMapper.setup(:default, postgresql_conn)
end

class Post
  include DataMapper::Resource
  property :id, Serial
  property :title, String
  property :body, Text
  property :created_at, DateTime
end

class Lote 
  include DataMapper::Resource
  property :id, Integer, :key => true
  property :address, String
  property :surface, String
  property :glose, String
  property :type, String
  property :lat, String
  property :lng, String
end 

DataMapper.finalize
Post.auto_upgrade!
Lote.auto_upgrade!

if Lote.count == 0 
  puts "Starting to import CSV data"
  reader = CsvReader.new
  reader.read_lotes()
  reader.lotes.each{ | csvLote |
  	lote = Lote.new( 
  		:id => csvLote.id, 
  		:address => csvLote.address, 
  		:surface => csvLote.surface,
  		:glose => csvLote.glose, 
  		:type => csvLote.type, 
  		:lat => csvLote.lat, 
  		:lng => csvLote.lng) 
  	lote.save()
  } 
  puts "Read lotes : #{reader.lotes.length}"
end
