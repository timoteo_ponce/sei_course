require 'csv'
require_relative 'lote_csv.rb'

class CsvReader

  attr_reader :lotes
  
  def initialize
	@lotes = []
  end
  
  def read_lotes
  	read_csv( 'model/lotes.csv' )
  end
  
  def read_csv( csv_file_name )
	CSV.foreach( csv_file_name , headers: true , col_sep: ';' ) do |row|
	  lote = CsvLote.new( row )
	  @lotes << lote
	  #puts "#{lote.to_s}"
	end
  end  
end

#reader = CsvReader.new
#reader.read_csv( 'model/lotes.csv' )
