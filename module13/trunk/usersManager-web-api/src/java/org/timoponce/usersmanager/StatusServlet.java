/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.timoponce.usersmanager;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import usersmanager.core.UsersManager;
import usersmanager.core.UsersManagerFactory;

/**
 *
 * @author timoteo
 */
public class StatusServlet extends HttpServlet {

    private final UsersManager manager = UsersManagerFactory.newInstance();

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Runtime runtime = Runtime.getRuntime();
            appendStyle(out);
            out.println("<div class=\"div-table\">");
            
            appendRow(out, "Sistema: ", System.getProperty("os.name") + " - "
                    + System.getProperty("os.version") + " - "
                    + System.getProperty("os.arch"));
            appendRow(out, "Procesadores ", runtime.availableProcessors() + "");
            appendRow(out, "Memoria: ", runtime.totalMemory() / 1024 + " MB");
            appendRow(out, "Grupos: ", "" + manager.listGroups().size());
            appendRow(out, "Usuarios: ", "" + manager.listUsers().size());

            out.println("</div>");
        } finally {
            out.close();
        }
    }

    private void appendStyle(PrintWriter out) {
        out.println(
                "<style type=\"text/css\">"
                + ".div-table{"
                + "display:table;"
                + "width:auto;"
                + "background-color:#eee;"
                + "border:1px solid  #666666;"
                + "border-spacing:5px;"
                + "} "
                + ".div-table-row{"
                + "display:table-row;"
                + "width:auto;"
                + "clear:both;"
                + "} "
                + ".div-table-col{"
                + "float:left;"
                + "display:table-column;"
                + "width:100px;"
                + "background-color:#ccc;"
                + "font-size: 12px;"
                + "} "
                + "</style>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void appendRow(PrintWriter out, String param, String value) {
        out.println("<div class=\"div-table-row\">");

        out.println("<div class=\"div-table-col\">");
        out.println(param + ": ");
        out.println("</div>");

        out.println("<div class=\"div-table-col\">");
        out.println(value);
        out.println("</div>");

        out.println("</div>");
    }
}
