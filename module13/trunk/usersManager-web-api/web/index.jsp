<%-- 
    Document   : index
    Created on : Mar 23, 2013, 10:20:41 PM
    Author     : timoteo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type='text/javascript' 
            src='jquery-1.9.1.min.js'>
        </script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Widget local</h1>        
        
        <script type='text/javascript'>
            $(document).ready(function() {
                $('#widget').load('http://localhost:8084/usersManager-web-api/status');
                //
                $.ajax({
                    url: 'http://saas-example.herokuapp.com/statusWidget'
                }).done( function (data){
                    $('#widget2').html(data);
                });
            });
        </script>
        
        <div id="widget"></div>
        
        <h1>Widget remoto</h1>        
        
        <div id="widget2">
            <iframe src="http://saas-example.herokuapp.com/statusWidget"
                    style="width: 100%;height: 100%;border: 0;"></iframe>
        </div>
    </body>
</html>
