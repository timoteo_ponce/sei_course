﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
//using mojoPortal.Web;
//using Inmobiliaria.Business;

namespace RestService1.Web.Controls 
{
    public static class MultiGrupo
    {
        public enum enumMultiGrupo
        {
            Bloque = 0,
            Grupo = 1,
            GrupoTrabajador = 2,
            ProyectoAnalisis=3,
            TipoInmueble=4
        };
        public enum enumSistema
        {
            Avances = 0,
            Inmobiliaria = 8,
            ActivoFijo = 10
        };

        public static void Cargar(int ProjectId, Telerik.Web.UI.RadComboBox ctrl, enumMultiGrupo type)
        {
            //ctrl.Items.Clear();

            //switch (type)
            //{
            //    case enumMultiGrupo.Bloque:
            //        using (IDataReader reader = BSMultiGrupo.GetListaBloquebyProject(ProjectId))
            //        {
            //            ctrl.DataSource = reader;
            //            ctrl.DataBind();
            //        }
            //        break;
            //    case enumMultiGrupo.Grupo:
            //        using (IDataReader reader = BSMultiGrupo.GetListaGrupobyProject(ProjectId))
            //        {
            //            ctrl.DataSource = reader;
            //            ctrl.DataBind();
            //        }

            //        break;
            //}

        }

        public static void Cargar(Telerik.Web.UI.RadComboBox ctrl, enumMultiGrupo type)
        {
            //ctrl.Items.Clear();
            //string Usuario = SiteUtils.GetCurrentSiteUser().LoginName;

            //switch (type)
            //{
            //    case enumMultiGrupo.ProyectoAnalisis:
            //        using (IDataReader reader = BSMultiGrupo.GetListaProyectosAnalisis(Usuario))
            //        {
            //            ctrl.DataSource = reader;
            //            ctrl.DataBind();
            //        }
            //        break;
            //    case enumMultiGrupo.TipoInmueble:
            //        {
            //            DataTable dt = new DataTable();
            //            DataRow dr;
            //            dt.Columns.Add("Id");
            //            dt.Columns.Add("Nombre");

            //            dr = dt.NewRow();
            //            dr["Id"] = "T";
            //            dr["Nombre"] = "Terreno";
            //            dt.Rows.Add(dr);
            //            dr = dt.NewRow();
            //            dr["Id"] = "C";
            //            dr["Nombre"] = "Casa";
            //            dt.Rows.Add(dr);
            //            dr = dt.NewRow();
            //            dr["Id"] = "A";
            //            dr["Nombre"] = "Apartamento";
            //            dt.Rows.Add(dr);

            //            ctrl.DataSource = dt;
            //            ctrl.DataBind();
                    
            //        }
            //        break;
            //}

        }
        public static void CargarUsuarios(Telerik.Web.UI.RadComboBox ctrl, enumSistema Sistema)
        {
            ctrl.Items.Clear();
           
            //switch (Sistema)
            //{
            //    case enumSistema.Inmobiliaria:
            //        using (IDataReader reader = BSMultiGrupo.GetListaUsuarios(8))
            //        {
            //            ctrl.DataSource = reader;
            //            ctrl.DataBind();
            //        }
            //        break;
            //}
        }

        public static void Cargar(string TipoTrabajador, Telerik.Web.UI.RadComboBox ctrl, enumMultiGrupo type, string strDataTextField, string strDataValueField)
        {
            ctrl.Items.Clear();

            //switch (type)
            //{
            //    case enumMultiGrupo.GrupoTrabajador:
            //        using (IDataReader reader = BSMultiGrupo.GetListaGrupobyTipoTrabajador(TipoTrabajador))
            //        {
            //            ctrl.DataSource = reader;
            //            ctrl.DataTextField = strDataTextField;
            //            ctrl.DataValueField = strDataValueField;
            //            ctrl.DataBind();
            //        }
            //        break;
            //    //case enumMultiGrupo.Grupo:
            //    //using (IDataReader reader = BSMultiGrupo.GetListaGrupobyProject(ProjectId))
            //    //    {
            //    //        ctrl.DataSource = reader;
            //    //        ctrl.DataBind();
            //    //    }
            //    //    break;
            //}

        }

        public static ArrayList SelectListaIds(RadComboBox Combo, string NameCheckBox)
        {
            ArrayList ListIdChecked = new ArrayList();

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    ListIdChecked.Add(item.Value);

            }
            return ListIdChecked;
        }
        public static ArrayList SelectListaNombres(RadComboBox Combo, string NameCheckBox)
        {
            ArrayList ListNamesChecked = new ArrayList();

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    ListNamesChecked.Add(item.Text);

            }
            return ListNamesChecked;
        }

        public static string SelectCadenaIds(RadComboBox Combo, string NameCheckBox)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    Cadena += item.Value + ",";
            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }
        public static string SelectCadenaIdsString(RadComboBox Combo, string NameCheckBox)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    Cadena += "'"+item.Value + "',";
            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }

        public static string SelectCadenaIdsStringSimple(RadComboBox Combo, string NameCheckBox)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    Cadena += item.Value + ",";
            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }

        public static string SelectCadenaIds(DataTable dt)
        {
            string Cadena = string.Empty;

            for (int i = 0; i < dt.Rows.Count;i++ )
            {
                Cadena += dt.Rows[i]["Id"] + ",";
            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }
        public static string SelectCadenaIds(RadComboBox Combo, string NameCheckBox, bool Enabled)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked && checkBox.Enabled == Enabled)
                    Cadena += item.Value + ",";

            }

            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }
        public static string SelectCadenaNombres(RadComboBox Combo, string NameCheckBox)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    Cadena += item.Text + ",";

            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }
        public static void Activar(RadComboBox Combo,DataTable idSelect,string NameCheckBox)
        {
            int itemIndex=0;

            for (int i = 0; i < idSelect.Rows.Count; i++)
             {
                 itemIndex = Combo.Items.FindItemByValue(Convert.ToString(idSelect.Rows[i][0])).Index;
                 CheckBox chkh = (CheckBox)Combo.Items[itemIndex].FindControl(NameCheckBox);
                 chkh.Checked = true;
             }
            
        }
        public static void Activar(RadComboBox Combo, string NameCheckBox)
        {
            ArrayList ItProcesado = new ArrayList();
            string sID = SelectCadenaIds(Combo, NameCheckBox);
            foreach ( Telerik.Web.UI.RadComboBoxItem it in Combo.Items)
            {
                CheckBox chk = (CheckBox)Combo.Items[it.Index].FindControl(NameCheckBox);

                if (it.Value.ToString().Contains(",")) // favorito
                {
                    foreach (string strID in it.Value.Split(Convert.ToChar(",")))
                    {
                        if (!ItProcesado.Contains(strID))
                        {
                            if (chk.Checked)
                                ItProcesado.Add(strID);


                            int itemIndex = Combo.Items.FindItemByValue(strID).Index;
                            CheckBox chkh = (CheckBox)Combo.Items[itemIndex].FindControl(NameCheckBox);

                            if (!(chkh.Enabled && chkh.Checked))
                            {
                                chkh.Checked = chk.Checked;
                                chkh.Enabled = !chk.Checked;
                            }
                        }
                    }
                }
                else
                    break;
            }

        }
        public static void PintarFavoritos(RadComboBox Combo)
        {
            foreach (Telerik.Web.UI.RadComboBoxItem it in Combo.Items)
            {
                if (it.Value.Contains(","))
                {
                    it.ToolTip = it.Value;
                    it.Font.Bold = true;
                    it.Font.Italic = true;
                    it.ForeColor = Color.Red;
                    it.Font.Name = "Tahoma";
                }
            }

        }

        public static string SelectCadenaToSQL(RadComboBox Combo, string NameCheckBox)
        {
            string Cadena = string.Empty;

            foreach (RadComboBoxItem item in Combo.Items)
            {
                CheckBox checkBox = (CheckBox)item.FindControl(NameCheckBox);
                if (checkBox.Checked)

                    Cadena += "'" + item.Text + "',";

            }
            return Cadena == string.Empty ? Cadena : Cadena.Substring(0, Cadena.Length - 1);
        }

    }
}
