﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
//using Inmobiliaria.Business;

namespace RestService1.Web.Controls 
{
    public class Helpers
    {
        public static List<Dictionary<string, object>> FixType(object obj)
        {
            if (obj is List<Dictionary<string, object>>)
                return (List<Dictionary<string, object>>)obj;

            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

            if (obj is object[])
            {
                foreach (object o in (object[])obj)
                {
                    data.Add((Dictionary<string, object>)o);
                }
            }

            string tipo = obj.GetType().ToString();

            if (obj is ArrayList)
            {
                foreach (Dictionary<string, object> o in (ArrayList)obj)
                {
                    data.Add(o);
                }
            }

            return data;
        }

        public static string UIFmtDecimal(object valor)
        {
            if (valor == null)
                return string.Empty;

            decimal result = Convert.ToDecimal(valor);
            return result.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string UIFmtFechaCorta(object valor)
        {
            if (valor == null)
                return string.Empty;

            DateTime result = Convert.ToDateTime(valor);
            return result.ToString("dd/MM/yyyy");
        }

        public static List<Coordenada> CoordenadasToList(System.Data.DataTable puntosDtt)
        {
            List<Coordenada> puntos = new List<Coordenada>();
            for (int i = 0; i < puntosDtt.Rows.Count; i++)
            {
                Coordenada pto = new Coordenada();
                pto.Latitud = Convert.ToDecimal(puntosDtt.Rows[i]["Latitud"]);
                pto.Longitud = Convert.ToDecimal(puntosDtt.Rows[i]["Longitud"]);
                puntos.Add(pto);
            }
            return puntos;
        }

        public static int AlturaCiudadGoogleEarth()
        {
            return 8000;
        }
    }
}
