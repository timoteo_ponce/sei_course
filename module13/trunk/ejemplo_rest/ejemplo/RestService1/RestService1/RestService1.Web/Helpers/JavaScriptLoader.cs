﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Configuration;
using System.Web.UI;
//using mojoPortal.Web;

namespace RestService1.Web.Controls 
{
    public class JavaScriptLoader
    {
        public static void LoadGoogleEarth(Page page)
        { 
            //if (!page.ClientScript.IsClientScriptIncludeRegistered("GoogleEarthComponent"))
            //    page.ClientScript.RegisterClientScriptInclude(typeof(Page), "GoogleEarthComponent", 
            //            "http://www.google.com/jsapi?key=ABQIAAAA5El50zA4PeDTEMlv-sXFfRSsTL4WIgxhMZ0ZK_kHjwHeQuOD4xTdBhxbkZWuzyYTVeclkwYHpb17ZQ");
            if (!page.ClientScript.IsClientScriptIncludeRegistered("GoogleEarthComponent"))
                page.ClientScript.RegisterClientScriptInclude(typeof(Page), "GoogleEarthComponent",
                        "http://www.google.com/jsapi");
        }

        public static void LoadGoogleEarthExtensions(Page page)
        {
            if (!page.ClientScript.IsClientScriptIncludeRegistered("GoogleEarthExtensions"))
                page.ClientScript.RegisterClientScriptInclude(typeof(Page), "GoogleEarthExtensions", 
                        "/Inmobiliaria/ClientScript/ge-extensions-0.2.1.js");
        }

        public static void LoadSimpleModal(Page page)
        {
            if (!page.ClientScript.IsClientScriptIncludeRegistered("SimpleModal"))
                page.ClientScript.RegisterClientScriptInclude(typeof(Page), "SimpleModal", 
                        "/Inmobiliaria/ClientScript/jquery.simplemodal.1.4.1.min.js");
        }

        public static void LoadMain(Page page)
        {
            if (!page.ClientScript.IsClientScriptIncludeRegistered("Main"))
                page.ClientScript.RegisterClientScriptInclude(typeof(Page), "Main",
                        "/Inmobiliaria/ClientScript/main.js");
        }

        public static void LoadDatePickerES(Page page)
        {
            if (!page.ClientScript.IsClientScriptIncludeRegistered("DatePickerES"))
                page.ClientScript.RegisterClientScriptInclude(typeof(Page), "DatePickerES",
                        "/ClientScript/jquery/i18n/ui.datepicker-"+CultureInfo.CurrentUICulture.TwoLetterISOLanguageName+".js");
        }
    }
}
