﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.Net;
using System.Xml.Linq;
using RestService1.Service;


namespace RestService1.Web
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // First start the web project, then the client

            WebClient client = new WebClient();

            client.BaseAddress = "http://localhost:2795/";

            string xmlString = client.DownloadString("SampleService");
            
            string xml_out =  lblTexto.Text.ToString();

            XElement xml = XElement.Parse(xmlString);
            
            XNamespace ns = "http://schemas.datacontract.org/2004/07/RestService1.Entities";
            
            var items = from i in xml.Elements()
                        select new SampleItem
                        {                               
                            Id = (int)i.Element(ns + "Id"), 
                            StringValue = i.Element(ns + "StringValue").Value                                                         
                        };

            foreach (var item in items)
            {
                //Console.WriteLine("{0} {1}", item.Id, item.StringValue);
                xml_out = xml_out + " " + item.Id + " " + item.StringValue + "//n";
                lblTexto.Text = xml_out;
            }
        }
    }
}