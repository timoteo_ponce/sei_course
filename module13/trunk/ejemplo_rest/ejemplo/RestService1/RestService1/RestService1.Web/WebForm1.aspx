﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="WebForm1.aspx.cs" Inherits="RestService1.Web.WebForm1" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<%@ Register TagPrefix="uc" TagName="DibujarPoligonoGoogle" Src="/Controls/DibujarPoligono.ascx" %>
<%@ Register src="/Controls/VisorProyectoAnalisis.ascx" tagname="VisorProyectoAnalisis" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Prueba 1 - REST </title>
</head>
<body>
    <form id="form1" runat="server">
    <div>  Prueba 1 - REST  </div>
    
    <asp:Panel ID="Panel1" runat="server" Height="413px" Width="830px">
        <br /> 
        <asp:Label ID="lblMostrar" runat="server" Text="Mostrar Prueba"></asp:Label>
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
        <br />       
        <asp:Label ID="lblTexto" runat="server" Text="Texto"></asp:Label>        
        <div style="height: 641px">
        <uc:DibujarPoligonoGoogle ID="dpgMapa" runat="server" />
        </div>
       
        <div style='padding:0.2em 1em;margin:0.5em 0'>
		    <div id="trace-error" class="ui-state-error ui-corner-all">

	    </div>
	</div>
    </asp:Panel>

    

    </form>
</body>
</html>
