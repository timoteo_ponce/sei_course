﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RestService1.Web.Controls
{
   public class Coordenada
    {

        private decimal _Latitud;
        private decimal _Longitud;

        public decimal Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        public decimal Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        public Data ToDB()
        {
            Data objDb = new Data();
            objDb.Latitud = _Latitud;
            objDb.Longitud = _Longitud;
            return objDb;
        }

    }
}
