﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DibujarPoligono.ascx.cs" 
Inherits="RestService1.Web.Controls.DibujarPoligono" %>

<script type="text/javascript">
    var ge;
    var nroPtos = 0;
    var dibujando = false;

    google.load("earth", "1");

    function init() {
        google.earth.createInstance('<% Response.Write(map3d.ClientID); %>', initCB, failureCB);
    }

    function initCB(instance) {
        ge = instance;
        ge.getWindow().setVisibility(true);
        ge.getOptions().setStatusBarVisibility(true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);
        ge.getNavigationControl().setVisibility(true);
        ge.getNavigationControl().getScreenXY().setYUnits(ge.UNITS_INSET_PIXELS);
        ge.getNavigationControl().getScreenXY().setXUnits(ge.UNITS_INSET_PIXELS);
        google.earth.addEventListener(ge.getGlobe(), 'click', ClickPunto);
        DibujarPoligono();
        DibujarPuntos();
        EnfocarCentro();
        DibujarInmuebles();
    }

    function failureCB(errorCode) {
    }

    function ClickPunto(event) {

        if (dibujando) {
            nroPtos++;

            var placemark = ge.createPlacemark('');
            placemark.setName("P" + nroPtos);
            ge.getFeatures().appendChild(placemark);

            // Create style map for placemark
            var icon = ge.createIcon('');
            icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            // Create point
            var lat = event.getLatitude();
            var lon = event.getLongitude();
            var point = ge.createPoint('');
            point.setLatitude(lat);
            point.setLongitude(lon);
            placemark.setGeometry(point);

            var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
            var puntos = jsDatos.Puntos;
            var pos = puntos.length;
            puntos[pos] = { "latitud": lat, "longitud": lon };
            jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val(JSON.stringify(jsDatos))
        }
    }

    function ClickEmpezar() {
        EnableButton('<%= BtnEmpezar.ClientID %>', false);
        EnableButton('<%= BtnTerminar.ClientID %>', true);
        dibujando = true;
        BorrarTodo();
    }

    function ClickTerminar() {
        EnableButton('<%= BtnEmpezar.ClientID %>', true);
        EnableButton('<%= BtnTerminar.ClientID %>', false);
        dibujando = false;
        DibujarPoligono();
    }

    function ClickReiniciar() {
        BorrarTodo();
    }

    function DibujarPoligono() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            var poligono = ge.createPlacemark('');
            var lineString = ge.createLineString('');
            poligono.setGeometry(lineString);
            for (var p in puntos) {
                lineString.getCoordinates().pushLatLngAlt(
                        parseFloat(puntos[p].latitud),
                        parseFloat(puntos[p].longitud),
                        0);
            }
            lineString.getCoordinates().pushLatLngAlt(
                    parseFloat(puntos[0].latitud),
                    parseFloat(puntos[0].longitud),
                    0);
            poligono.setStyleSelector(ge.createStyle(''));
            var lineStyle = poligono.getStyleSelector().getLineStyle();
            lineStyle.setWidth(5);
            lineStyle.getColor().set('9900ffff');  // aabbggrr format
            // Add the feature to Earth
            ge.getFeatures().appendChild(poligono);
        }
    }

    function DibujarInmuebles() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnInmuebles.ClientID); %>').val());
        var inmuebles = jsDatos.inmuebles;
        if (inmuebles.length == 0) return;

        for (var i in inmuebles) {
            var poligono = ge.createPlacemark('');
            var lineString = ge.createLineString('');
            poligono.setGeometry(lineString);

            var coordenadas = inmuebles[i].coordenadas;
            for (var c in coordenadas) {
                lineString.getCoordinates().pushLatLngAlt(
                        parseFloat(coordenadas[c].latitud),
                        parseFloat(coordenadas[c].longitud),
                        0);
            }
            lineString.getCoordinates().pushLatLngAlt(
                    parseFloat(coordenadas[0].latitud),
                    parseFloat(coordenadas[0].longitud),
                    0);
            poligono.setStyleSelector(ge.createStyle(''));
            var lineStyle = poligono.getStyleSelector().getLineStyle();
            lineStyle.setWidth(5);
            lineStyle.getColor().set('BBF7D358');  // aabbggrr format
            // Add the feature to Earth
            ge.getFeatures().appendChild(poligono);
        }
    }

    function DibujarPuntos() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;

        for (var p in puntos) {
            nroPtos++;
            var placemark = ge.createPlacemark('');
            placemark.setName("P" + nroPtos);
            ge.getFeatures().appendChild(placemark);
            // Create style map for placemark
            var icon = ge.createIcon('');
            icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            var point = ge.createPoint('');
            point.setLatitude(puntos[p].latitud);
            point.setLongitude(puntos[p].longitud);
            placemark.setGeometry(point);
        }
    }

    function EnfocarCentro() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            // Enfoca el centro del poligono con zoom relativo.
            var gex = new GEarthExtensions(ge);
            var bounds = new geo.Bounds();
            for (var p in puntos) {
                bounds.extend(new geo.Point(puntos[p].latitud, puntos[p].longitud));
            }
            var view = gex.view.createBoundsView(bounds, { aspectRatio: 1.0 });
            view.setTilt(0);
            ge.getView().setAbstractView(view);
        }
        else {
            // Enfoca al centro y altura especificado.
            var altitud = jQuery('#<% Response.Write(hdnAltura.ClientID); %>').val();
            jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnCentro.ClientID); %>').val());
            puntos = jsDatos.Puntos;
            if (puntos.length > 0) {
                var lookAt = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
                lookAt.setLatitude(puntos[0].latitud);
                lookAt.setLongitude(puntos[0].longitud);
                lookAt.setRange(parseFloat(altitud));
                ge.getView().setAbstractView(lookAt);
            }
        }
    }

    function BorrarTodo() {
        // Borra las capas
        var features = ge.getFeatures();
        while (features.getFirstChild()) {
            features.removeChild(features.getFirstChild());
        }
        // Borra la tabla de puntos
        var vacio = '{ "Puntos":[] }';
        jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val(vacio);
        
        nroPtos = 0;
    }

    function EnableButton(boton, estado) {
        document.getElementById(boton).disabled = !estado;
    }

    google.setOnLoadCallback(init);
</script>

<asp:HiddenField ID="hdnPtos" runat="server" />
<asp:HiddenField ID="hdnCentro" runat="server" />
<asp:HiddenField ID="hdnAltura" runat="server" />
<asp:HiddenField ID="hdnInmuebles" runat="server" />
<asp:HiddenField ID="HiddenField_Puntos" runat="server" />


<div style="height: 52px; width: 893px;">
    <div>
    <asp:Button ID="BtnEmpezar" runat="server" 
            OnClientClick="ClickEmpezar(); return false;" onclick="BtnEmpezar_Click" />
    <asp:Button ID="BtnTerminar" runat="server" OnClientClick="ClickTerminar(); return false;" />
    <asp:Button ID="BtnReiniciar" runat="server" OnClientClick="ClickReiniciar(); return false;" />
    </div>
    <div id="map3d" runat="server" style="height: 441px"></div>
</div>
