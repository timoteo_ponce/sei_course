﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using mojoPortal.Web;
//using mojoPortal.Web.Framework;
//using Inmobiliaria.Business;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Web.Script;
using System.Collections;

namespace RestService1.Web.Controls 
{
    public partial class VisorProyectoAnalisis : System.Web.UI.UserControl
    {
        private int _pageId = -1;
        private int _moduleId = -1;
        private int _idProyecto = -1;


        protected void Page_Load(object sender, EventArgs e)
        {
                LoadParams();
                LoadSettings();
                PopulateLabels();
                PopulateControls();
           
        }
        private void LoadParams()
        {
            //_pageId = WebUtils.ParseInt32FromQueryString("pageid", _pageId);
            //_moduleId = WebUtils.ParseInt32FromQueryString("mid", _moduleId);
            //_idProyecto = WebUtils.ParseInt32FromQueryString("idProyecto", _idProyecto);

        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            lnkBtnGuardar.Click += new EventHandler(lnkAceptar_Click);
        }
        private void lnkAceptar_Click(object sender, EventArgs e)
        {
            DataTable dtcache = (DataTable)Session["INMUEBLESCACHE"];
                        
            DataTable dtInmuebles = new DataTable();
            dtInmuebles.Columns.Add("Id");
            dtInmuebles.Columns.Add("Activo");
            DataRow dr;

            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datos = js.Deserialize<Dictionary<string, object>>(hdnDetailData.Value);
            List<Dictionary<string, object>> listaInmuebles = FixType(datos["dataRows"]);

            foreach (Dictionary<string, object> inmueble in listaInmuebles)
            {
                dr = dtInmuebles.NewRow();
                dr["Id"] = Convert.ToInt32(inmueble["Id"]);
                dr["Activo"] = Convert.ToInt32(inmueble["chkSel"]);
                dtInmuebles.Rows.Add(dr);
            }
            
            //ProyectoAnalisis proy = new ProyectoAnalisis();

            //proy.Id = _idProyecto;
            //proy.Nombre = hdnNombreProyecto.Value;
            //proy.Glosa = hdnGlosaProyecto.Value;
            //proy.UsuarioCreador = SiteUtils.GetCurrentSiteUser().LoginName;
            //proy.dtInmuebles = dtInmuebles;

            //lblAnalisis.Text = "Proyecto Analisis:" + proy.GuardarProyecto();

            //txbNombre.Text = proy.Nombre;
            //txbGlosa.Text = proy.Glosa;

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            JQuery.Register(this.Page);
            JavaScriptLoader.LoadGoogleEarth(this.Page);
            JavaScriptLoader.LoadGoogleEarthExtensions(this.Page);
            JavaScriptLoader.LoadSimpleModal(this.Page);
            Page.ClientScript.RegisterClientScriptInclude(GetType(), GetType().ToString(), ResolveUrl("../ClientScript/ListaCache.js"));   
        }
        private void PopulateControls()
        {
            if (!IsPostBack)
            {
                lblAnalisis.Text = "Proyecto Analisis:" + _idProyecto;
            }        
        }
        private void PopulateLabels()
        {
            lblNombre.Text = "Nombre:";
            lblGlosa.Text = "Glosa:";
        }
        private void LoadSettings()
        {
           
        }
        public void resetHdnData()
        {
            hdnDetailData.Value = "";
        }
        public void setInmueblesCache(DataTable dtcache)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datos = new Dictionary<string, object>();
            List<Dictionary<string, object>> listainmuebles = new List<Dictionary<string, object>>();
            Dictionary<string, object> fila;
            int cont = 0;

            foreach (DataRow dr in dtcache.Rows)
            {
                fila = new Dictionary<string, object>();
                fila.Add("rIndex", cont.ToString());
                fila.Add("Id", dr["Id"]);
                fila.Add("Direccion", dr["Direccion"]);
                fila.Add("Superficie", dr["Superficie"]);
                fila.Add("mid", dr["mid"]);
                fila.Add("pageid", dr["pageid"]);
                fila.Add("chkSel", dr["chkSel"]);
                listainmuebles.Add(fila);
                cont++;
            }
            datos.Add("dataRows", listainmuebles);
            hdnDetailData.Value = js.Serialize(datos);
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            DataTable dtCache = (DataTable)Session["INMUEBLESCACHE"];
            
            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datos = js.Deserialize<Dictionary<string, object>>(hdnDetailData.Value);
            List<Dictionary<string, object>> listaInmuebles = FixType(datos["dataRows"]);
            int index = 0;
            foreach (Dictionary<string, object> inmueble in listaInmuebles)
            {
                if (Convert.ToInt32(inmueble["chkSel"]) == 1)
                {
                    dtCache.Rows.RemoveAt(index);
                }
                index++;
            }

            setInmueblesCache(dtCache);

        }
        private List<Dictionary<string, object>> FixType(object obj)
        {
            if (obj is List<Dictionary<string, object>>)
                return (List<Dictionary<string, object>>)obj;

            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

            if (obj is object[])
            {
                foreach (object o in (object[])obj)
                {
                    data.Add((Dictionary<string, object>)o);
                }
            }

            string tipo = obj.GetType().ToString();

            if (obj is ArrayList)
            {
                foreach (Dictionary<string, object> o in (ArrayList)obj)
                {
                    data.Add(o);
                }
            }

            return data;
        }

        protected void btnAnalizar_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("AnalisisProyecto.aspx?mid={0}&pageid={1}", _moduleId, _pageId));
        }
        public void setVisibleBtnAnalizar(bool sw)
        {
            btnAnalizar.Visible = sw;
        }
        public void setEnableBtnModificarEliminar(bool sw)
        {
            btnEliminar.Enabled = sw;

            lnkBtnGuardar.Enabled = sw;
        }
        public void settxbNombre(string NombreProyecto)
        {
            txbNombre.Text = NombreProyecto;
        }
        public void settxbGlosa(string GlosaProyecto)
        {
            txbGlosa.Text = GlosaProyecto;
        }
    }
}