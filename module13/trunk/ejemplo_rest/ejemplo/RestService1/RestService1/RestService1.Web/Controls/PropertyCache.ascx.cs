﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RestService1.Web.Controls 
{
    public partial class PropertyCache : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateLabels();
        }
        private void PopulateLabels()
        {
            lnkRefresh.Text = "Actualizar Cache";
            lnkReset.Text = "Borrar Cache";
        }

    }
}