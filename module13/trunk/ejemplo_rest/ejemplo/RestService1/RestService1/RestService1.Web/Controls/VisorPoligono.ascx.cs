﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using RestService1.Web.Controls;
//using Inmobiliaria.Business;

namespace RestService1.Web.Controls 
{
    public partial class VisorPoligono : System.Web.UI.UserControl
    {
        
        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadSettings();
            PopulateLabels();
            PopulateControls();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            JQuery.Register(this.Page);
            JavaScriptLoader.LoadGoogleEarth(this.Page);
            JavaScriptLoader.LoadGoogleEarthExtensions(this.Page);
        }

        protected void LoadSettings()
        {
            map3d.Style.Add("height", _Height.ToString() + "px");
            map3d.Style.Add("width", _Width.ToString() + "px");
        }

        protected void PopulateLabels()
        {

        }

        protected void PopulateControls()
        {
            if (!IsPostBack)
            {
                hdnPtos.Value = "{\"Puntos\":[]}";
                hdnCentro.Value = "{\"Puntos\":[]}";

                hdnAltura.Value = _Altura.ToString();
                CargarCentro(_Centro);
                CargarCoordenadas(_Coordenadas);
            }
            else
            {
                _Centro = LeerCentro();
                _Coordenadas = LeerCoordenadas();
            }
        }

        #endregion

        #region Publico

        public Coordenada Centro
        {
            get { return _Centro; }
            set
            {
                _Centro = value;
                CargarCentro(value);
            }
        }

        public int Altura
        {
            get { return _Altura; }
            set
            {
                _Altura = value;
                hdnAltura.Value = value.ToString();
            }
        }

        public List<Coordenada> Coordenadas
        {
            get { return _Coordenadas; }
            set
            {
                _Coordenadas = value;
                CargarCoordenadas(value);
            }
        }

        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        #endregion

        #region Privado

        private Coordenada _Centro;
        private int _Altura = 500;
        private List<Coordenada> _Coordenadas;
        private int _Width;
        private int _Height;

        private void CargarCentro(Coordenada pto)
        {
            if (pto == null) return;

            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datosJs = new Dictionary<string, object>();
            List<Dictionary<string, object>> puntosJs = new List<Dictionary<string, object>>();
            Dictionary<string, object> fila;

            datosJs = new Dictionary<string, object>();
            puntosJs = new List<Dictionary<string, object>>();
            fila = new Dictionary<string, object>();
            fila.Add("latitud", pto.Latitud);
            fila.Add("longitud", pto.Longitud);
            puntosJs.Add(fila);
            datosJs.Add("Puntos", puntosJs);
            hdnCentro.Value = js.Serialize(datosJs);
        }

        private void CargarCoordenadas(List<Coordenada> puntos)
        {
            if (puntos == null) return;

            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datosJs = new Dictionary<string, object>();
            List<Dictionary<string, object>> puntosJs = new List<Dictionary<string, object>>();
            Dictionary<string, object> fila;
            for (int i = 0; i < puntos.Count; i++)
            {
                fila = new Dictionary<string, object>();
                fila.Add("latitud", puntos[i].Latitud);
                fila.Add("longitud", puntos[i].Longitud);
                puntosJs.Add(fila);
            }
            datosJs.Add("Puntos", puntosJs);
            hdnPtos.Value = js.Serialize(datosJs);
        }

        private List<Coordenada> LeerCoordenadas()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datosJs = js.Deserialize<Dictionary<string, object>>(hdnPtos.Value);
            List<Dictionary<string, object>> puntosJs = Helpers.FixType(datosJs["Puntos"]);
            List<Coordenada> puntos = new List<Coordenada>();

            foreach (Dictionary<string, object> ptoJs in puntosJs)
            {
                Coordenada pto = new Coordenada();
                pto.Latitud = Convert.ToDecimal(ptoJs["latitud"]);
                pto.Longitud = Convert.ToDecimal(ptoJs["longitud"]);
                puntos.Add(pto);
            }
            return puntos;
        }

        private Coordenada LeerCentro()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Dictionary<string, object> datosJs = js.Deserialize<Dictionary<string, object>>(hdnPtos.Value);
            List<Dictionary<string, object>> puntosJs = Helpers.FixType(datosJs["Puntos"]);
            Coordenada pto = new Coordenada();

            if (puntosJs.Count > 0)
            {
                Dictionary<string, object> ptoJs = puntosJs[0];
                pto.Latitud = Convert.ToDecimal(ptoJs["latitud"]);
                pto.Longitud = Convert.ToDecimal(ptoJs["longitud"]);
            }
            return pto;
        }

        #endregion
    }
}