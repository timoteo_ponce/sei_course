﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisorProyectoAnalisis.ascx.cs" 
Inherits="RestService1.Web.Controls.VisorProyectoAnalisis" %>

<style type="text/css" media="screen">    
    #simplemodal-overlay {background-color:#000;}
    #simplemodal-container {background-color:#FFFFFF; border:8px solid #444; padding:12px;}
</style> 

<script type="text/javascript">

function bmCancelar() {
    $.modal.close();
}
function bmModal() {
    jQuery('#NuevoProyecto').modal({ minHeight: 200, minWidth: 400 });
    return false;
}

jQuery(document).ready(function() {
bmFillInmuebleDetail('<% Response.Write(tblInmuebles.ClientID); %>', '<% Response.Write(hdnDetailData.ClientID); %>');
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest);
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(onBeginRequest);
});

function bmAceptar() {

    var Proyecto = jQuery('#<% Response.Write(txbNombre.ClientID); %>').val();
    var Glosa = jQuery('#<% Response.Write(txbGlosa.ClientID); %>').val();

    jQuery('#<% Response.Write(hdnNombreProyecto.ClientID); %>').val(Proyecto);
    jQuery('#<% Response.Write(hdnGlosaProyecto.ClientID); %>').val(Glosa);


}

</script>
<asp:HiddenField ID="hdnNombreProyecto" runat="server" />
<asp:HiddenField ID="hdnGlosaProyecto" runat="server" />
<div>
   <div>
       <asp:Label ID="lblAnalisis" runat="server" Text="Proyecto Analisis"></asp:Label>
   </div>
    <div id="tableHeader">
    </div>
    <div style="overflow: scroll; height: 124px; width: 341px">
    
    <asp:Table ID="tblInmuebles" runat="server" CssClass="bm-table" EnableViewState="false">
	<asp:TableHeaderRow TableSection="TableHeader">
		<asp:TableHeaderCell >&nbsp;</asp:TableHeaderCell>
		<asp:TableHeaderCell >Direccion</asp:TableHeaderCell>
		<asp:TableHeaderCell >Superficie</asp:TableHeaderCell>
    	<asp:TableHeaderCell >&nbsp;</asp:TableHeaderCell>
	</asp:TableHeaderRow>
	
</asp:Table>

    </div>
<div>
<asp:HiddenField ID="hdnDetailData" runat="server" />

 </div>
    <div>
    <table>
        <tr>
         <td>
             <asp:Button ID="btnAnalizar" runat="server" Text="Analizar" 
                 onclick="btnAnalizar_Click" />
            
         </td>
            <td>
            
                <input type="button"  name="Guardar" value="Guardar" onclick="bmModal()" />
            
            </td>
            <td>
            
                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" 
                    onclick="btnEliminar_Click" />
            
            </td>
           
        </tr>
    </table>
    </div>
    
    <div id="NuevoProyecto" style="background-color:White; display:none; text-align:left">
    
    <fieldset>
        <legend>Proyecto de Analisis</legend>
	    
        <table class="bm-table"> 
        <tr> 
            <td align="left" style="width:25%"><asp:Label ID="lblNombre" runat="server" /></td> 
            <td align="left" style="width:60%"><asp:TextBox ID="txbNombre" runat="server" /></td> 
        </tr> 
        <tr> 
            <td align="left">
                <asp:Label ID="lblGlosa" runat="server" /></td> 
            <td align="left" colspan="2">
                <asp:TextBox ID="txbGlosa" runat="server" /></td> 		       
        </tr> 
        </table>
    </fieldset>
    
    <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="clear:both;margin-top:0.5em">--%>
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="lnkBtnGuardar" runat="server" OnClientClick="return bmAceptar();">Guardar</asp:LinkButton> 
                </td>
                <td>|</td>
                <td>
                <a href="javascript:bmCancelar()">Cancelar</a>
                </td>
            </tr>
        </table>
           
        <div style='padding:0.2em 1em;margin:0.5em 0'>
            <div id="Div1" class="ui-state-error ui-corner-all"><asp:Literal ID="Literal2" runat="server" EnableViewState="false" /></div>
            <div id="Div2" class="ui-state-highlight ui-corner-all"><asp:Literal ID="Literal3" runat="server" EnableViewState="false" /></div>
        </div>
    </div>
    	
</div>
    
</div>
