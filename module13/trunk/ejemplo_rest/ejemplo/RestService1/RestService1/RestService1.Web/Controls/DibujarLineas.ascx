﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DibujarLineas.ascx.cs" 
Inherits="RestService1.Web.Controls.DibujarLineas" %>


<script type="text/javascript">
    var ge;
    var nroPtos = 0;
    var dibujando = false;
    var ultimoPto = {"latitud":0, "longitud":0};

    google.load("earth", "1");

    function init() {
        google.earth.createInstance('<% Response.Write(map3d.ClientID); %>', initCB, failureCB);
    }

    function initCB(instance) {
        ge = instance;
        ge.getWindow().setVisibility(true);
        ge.getOptions().setStatusBarVisibility(true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);
        ge.getNavigationControl().setVisibility(true);
        ge.getNavigationControl().getScreenXY().setYUnits(ge.UNITS_INSET_PIXELS);
        ge.getNavigationControl().getScreenXY().setXUnits(ge.UNITS_INSET_PIXELS);
        google.earth.addEventListener(ge.getGlobe(), 'click', ClickPunto);
        DibujarPoligono();
        DibujarPuntos();
        EnfocarCentro();
    }

    function failureCB(errorCode) {
    }

    function ClickPunto(event) {
        if (dibujando) {
            nroPtos++;
            var pto = { "latitud": event.getLatitude(), "longitud": event.getLongitud() };
            PintarPunto(pto);
            PintarLinea(pto);
            ultimoPto = pto;

            // guarda la linea  (origen, destino) TERMINAR
            var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
            var puntos = jsDatos.Puntos;
            var pos = puntos.length;
            puntos[pos] = { "latitud": lat, "longitud": lon };
            jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val(JSON.stringify(jsDatos))
        }
    }

    function ClickEmpezar() {
        EnableButton('<%= BtnEmpezar.ClientID %>', false);
        EnableButton('<%= BtnTerminar.ClientID %>', true);
        dibujando = true;
    }

    function ClickTerminar() {
        EnableButton('<%= BtnEmpezar.ClientID %>', true);
        EnableButton('<%= BtnTerminar.ClientID %>', false);
        dibujando = false;
    }

    function ClickBorrarTodo() {
        BorrarTodo();
    }

    function DibujarPoligono() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            var poligono = ge.createPlacemark('');
            var lineString = ge.createLineString('');
            poligono.setGeometry(lineString);
            for (var p in puntos) {
                lineString.getCoordinates().pushLatLngAlt(
                        parseFloat(puntos[p].latitud),
                        parseFloat(puntos[p].longitud),
                        0);
            }
            lineString.getCoordinates().pushLatLngAlt(
                    parseFloat(puntos[0].latitud),
                    parseFloat(puntos[0].longitud),
                    0);
            poligono.setStyleSelector(ge.createStyle(''));
            var lineStyle = poligono.getStyleSelector().getLineStyle();
            lineStyle.setWidth(5);
            lineStyle.getColor().set('9900ffff');  // aabbggrr format
            // Add the feature to Earth
            ge.getFeatures().appendChild(poligono);
        }
    }

    function PintarPunto(punto) {
        var placemark = ge.createPlacemark('');
        placemark.setName("P" + nroPtos);
        ge.getFeatures().appendChild(placemark);

        // Create style map for placemark
        var icon = ge.createIcon('');
        icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
        var style = ge.createStyle('');
        style.getIconStyle().setIcon(icon);
        placemark.setStyleSelector(style);

        // Create point
        var lat = punto.latitud;
        var lon = punto.longitud;
        var point = ge.createPoint('');
        point.setLatitude(lat);
        point.setLongitude(lon);
        placemark.setGeometry(point);

        
    }

    function PintarLinea(punto) {
        if (ultimoPto.latitud != 0 && ultimoPto.longitud != 0) {
            
        }
    }

    function DibujarPuntos() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;

        for (var p in puntos) {
            nroPtos++;
            var placemark = ge.createPlacemark('');
            placemark.setName("P" + nroPtos);
            ge.getFeatures().appendChild(placemark);
            // Create style map for placemark
            var icon = ge.createIcon('');
            icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            var point = ge.createPoint('');
            point.setLatitude(puntos[p].latitud);
            point.setLongitude(puntos[p].longitud);
            placemark.setGeometry(point);
        }
    }

    function EnfocarCentro() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            // Enfoca el centro del poligono con zoom relativo.
            var gex = new GEarthExtensions(ge);
            var bounds = new geo.Bounds();
            for (var p in puntos) {
                bounds.extend(new geo.Point(puntos[p].latitud, puntos[p].longitud));
            }
            var view = gex.view.createBoundsView(bounds, { aspectRatio: 1.0 });
            view.setTilt(0);
            ge.getView().setAbstractView(view);
        }
        else {
            // Enfoca al centro y altura especificado.
            var altitud = jQuery('#<% Response.Write(hdnAltura.ClientID); %>').val();
            jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnCentro.ClientID); %>').val());
            puntos = jsDatos.Puntos;
            if (puntos.length > 0) {
                var lookAt = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
                lookAt.setLatitude(puntos[0].latitud);
                lookAt.setLongitude(puntos[0].longitud);
                lookAt.setRange(parseFloat(altitud));
                ge.getView().setAbstractView(lookAt);
            }
        }
    }

    function BorrarTodo() {
        // Borra las capas
        var features = ge.getFeatures();
        while (features.getFirstChild()) {
            features.removeChild(features.getFirstChild());
        }
        // Borra la tabla de puntos
        var vacio = '{ "Puntos":[] }';
        jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val(vacio);
        
        nroPtos = 0;
    }

    function EnableButton(boton, estado) {
        document.getElementById(boton).disabled = !estado;
    }

    google.setOnLoadCallback(init);
</script>

<asp:HiddenField ID="hdnPtos" runat="server" />
<asp:HiddenField ID="hdnCentro" runat="server" />
<asp:HiddenField ID="hdnAltura" runat="server" />

<div>
    <div>
    <asp:Button ID="BtnEmpezar" runat="server" OnClientClick="ClickEmpezar(); return false;" />
    <asp:Button ID="BtnTerminar" runat="server" OnClientClick="ClickTerminar(); return false;" />
    <asp:Button ID="BtnBorrarTodo" runat="server" OnClientClick="ClickBorrarTodo(); return false;" />
    </div>
    <div id="map3d" runat="server"></div>
</div>
