﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisorPoligono.ascx.cs" 
Inherits="RestService1.Web.Controls.VisorPoligono" %>

<script type="text/javascript">
    var ge;
    google.load("earth", "1");

    function init() {
        google.earth.createInstance('<% Response.Write(map3d.ClientID); %>', initCB, failureCB);
    }

    function initCB(instance) {
        ge = instance;
        ge.getWindow().setVisibility(true);
        ge.getOptions().setStatusBarVisibility(true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
        ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);
        ge.getNavigationControl().setVisibility(true);
        ge.getNavigationControl().getScreenXY().setYUnits(ge.UNITS_INSET_PIXELS);
        ge.getNavigationControl().getScreenXY().setXUnits(ge.UNITS_INSET_PIXELS);
        DibujarPoligono();
         DibujarPuntos();
        EnfocarCentro();
    }

    function failureCB(errorCode) {
    }

    function DibujarPoligono() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            var poligono = ge.createPlacemark('');
            var lineString = ge.createLineString('');
            poligono.setGeometry(lineString);
            for (var p in puntos) {
                lineString.getCoordinates().pushLatLngAlt(
                        parseFloat(puntos[p].latitud),
                        parseFloat(puntos[p].longitud),
                        0);
            }
            lineString.getCoordinates().pushLatLngAlt(
                    parseFloat(puntos[0].latitud),
                    parseFloat(puntos[0].longitud),
                    0);
            poligono.setStyleSelector(ge.createStyle(''));
            var lineStyle = poligono.getStyleSelector().getLineStyle();
            lineStyle.setWidth(5);
            lineStyle.getColor().set('9900ffff');  // aabbggrr format
            // Add the feature to Earth
            ge.getFeatures().appendChild(poligono);
        }
    }

    function DibujarPuntos() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;

        for (var p in puntos) {
            nroPtos++;
            var placemark = ge.createPlacemark('');
            placemark.setName("P" + nroPtos);
            ge.getFeatures().appendChild(placemark);
            // Create style map for placemark
            var icon = ge.createIcon('');
            icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            var point = ge.createPoint('');
            point.setLatitude(puntos[p].latitud);
            point.setLongitude(puntos[p].longitud);
            placemark.setGeometry(point);
        }
    }

    function EnfocarCentro() {
        var jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val());
        var puntos = jsDatos.Puntos;
        if (puntos.length > 0) {
            // Enfoca el centro del poligono con zoom relativo.
            var gex = new GEarthExtensions(ge);
            var bounds = new geo.Bounds();
            for (var p in puntos) {
                bounds.extend(new geo.Point(puntos[p].latitud, puntos[p].longitud));
            }
            var view = gex.view.createBoundsView(bounds, { aspectRatio: 1.0 });
            view.setTilt(0);
            ge.getView().setAbstractView(view);
        }
        else {
            // Enfoca al centro y altura especificado.
            var altitud = jQuery('#<% Response.Write(hdnAltura.ClientID); %>').val();
            jsDatos = JSON.parse(jQuery('#<% Response.Write(hdnCentro.ClientID); %>').val());
            puntos = jsDatos.Puntos;
            if (puntos.length > 0) {
                var lookAt = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
                lookAt.setLatitude(puntos[0].latitud);
                lookAt.setLongitude(puntos[0].longitud);
                lookAt.setRange(parseFloat(altitud));
                ge.getView().setAbstractView(lookAt);
            }
        }
    }

    function BorrarTodo() {
        // Borra las capas
        var features = ge.getFeatures();
        while (features.getFirstChild()) {
            features.removeChild(features.getFirstChild());
        }
        // Borra la tabla de puntos
        var vacio = '{ "Puntos":[] }';
        jQuery('#<% Response.Write(hdnPtos.ClientID); %>').val(vacio);
    }

    google.setOnLoadCallback(init);
</script>

<asp:HiddenField ID="hdnPtos" runat="server" />
<asp:HiddenField ID="hdnCentro" runat="server" />
<asp:HiddenField ID="hdnAltura" runat="server" />

<div id="map3d" runat="server"></div>
