﻿function bmFillInmuebleDetail(tableId, inputId) {
    var table = jQuery("#" + tableId);
    var data = '';
    var tbody = table.children("tbody");
    var html = '';
    var id, direccion, superficie;
    id = -1;
    direccion = '';
    superficie = '';

    try {
        data = JSON.parse(jQuery('#' + inputId).val());
    }
    catch (ex) {
        return;
    }

    if (tbody.length == 0) {
        table.append('<tbody></tbody>')
        tbody = table.children('tbody');
    }
    else
        tbody.empty();

    var strChecked;
    for (var r in data.dataRows) {
        var dataRow = data.dataRows[r];

        if (dataRow.chkSel == 1)
            strChecked = 'checked';
        else
            strChecked = '';
        html = '<tr>';
        //html += '<td><input type="checkbox" class="bm-row-check"/><input type="hidden" name="subitem-data" value="' + Encoder.htmlEncode(JSON.stringify(rowData)) + '"/></td>';
        html += '<td><a href="CasaDetail.aspx?mid=' + dataRow.mid + '&pageid=' + dataRow.pageid + '&idinmueble=' + dataRow.Id + '">' + dataRow.Id + '</a></td>';
        html += '<td>' + dataRow.Direccion + '</td>';
        html += '<td class="bm-number">' + dataRow.Superficie + '</td>';
        html += '<td><input type="checkbox" name="chkSubitem" class="bm-row-check" ' + strChecked + ' onClick="bmCheckboxClick(' + dataRow.rIndex + ', this, \'' + inputId + '\')" /><input type="hidden" name="hdnRow" value="' + dataRow.rIndex + '" /></td>';
        html += '</tr>';
        tbody.append(html);
    }
}
function bmInputDecimalByObj(obj) {
    if (!obj)
        return;

    obj = jQuery(obj);

    obj.keypress(function(e) {
        return bmBlockNonNumbers(this, e, true, false);
    });

    obj.blur(function(e) {
        bmExtractNumber(this, 2, false);
    });

    obj.keyup(function(e) {
        bmExtractNumber(this, 2, false);
    });

    obj.attr('maxlength', '15');
}

function bmCheckboxClick(row, checkbox, inputId) {
    var data = '';

    try {
        data = JSON.parse(jQuery('#' + inputId).val());
    }
    catch (ex) {
        return;
    }

    for (var r in data.dataRows) {
        var dataRow = data.dataRows[r];
        if (dataRow.rIndex == row) {
            if (checkbox.checked)
                dataRow.chkSel = 1;
            else
                dataRow.chkSel = 0;
        }
    }
    document.getElementById(inputId).value = JSON.stringify(data);
}

function bmChangeAllCheckboxes(tableId, value, inputId) {
    if (value == 0)
        jQuery('#' + tableId + ' input:checkbox').attr('checked', false);
    else
        jQuery('#' + tableId + ' input:checkbox').attr('checked', true);

    var data = '';
    try {
        data = JSON.parse(jQuery('#' + inputId).val());
    }
    catch (ex) {
        return;
    }

    for (var r in data.dataRows) {
        var dataRow = data.dataRows[r];
        dataRow.chkSel = value;
    }
    document.getElementById(inputId).value = JSON.stringify(data);
}

