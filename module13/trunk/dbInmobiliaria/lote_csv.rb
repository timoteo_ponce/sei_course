class Lote
  attr_accessor :id, :address, :surface, :glose, :type, :lat, :lng
  
  def initialize( row )
	@id = row['ID']
	@address = row['ADDRESS']
	@surface = row['SURFACE'] 
	@glose = row['GLOSE'] 
	@type = row['TYPE'] 
	@lat = row['LAT']
	@lng = row['LNG']
  end
  
  def to_s
	return "ID: #{@id}, address: #{@address}, surface: #{@surface}, " +
		"glose: #{@glose}, type: #{@type}, lat: #{@lat}, lng: #{@lng} "
  end
  
end 
