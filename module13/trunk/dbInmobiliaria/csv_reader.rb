require 'csv'
require './lote_csv.rb'

class CsvReader
  
  def initialize
	@lotes = []
  end
  
  def read_csv( csv_file_name )
	CSV.foreach( csv_file_name , headers: true , col_sep: ';' ) do |row|
	  lote = Lote.new( row )
	  @lotes << lote
	  puts "#{lote.to_s}"
	end
  end  
end

reader = CsvReader.new
reader.read_csv( 'lotes.csv' )
