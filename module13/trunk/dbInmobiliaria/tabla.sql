USE [map_prueba]
GO

/****** Object:  Table [dbo].[ConsultaCarla]    Script Date: 04/03/2013 15:47:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ConsultaCarla](
	[Idinmueble] [int] NOT NULL,
	[direccion] [varchar](200) NOT NULL,
	[superficie] [decimal](18, 2) NOT NULL,
	[glosa] [varchar](1000) NOT NULL,
	[tipoInmueble] [char](1) NULL,
	[latitud] [decimal](20, 15) NULL,
	[longitud] [decimal](20, 15) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


