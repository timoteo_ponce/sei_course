<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : ListUser
    Created on : 17-mar-2013, 1:18:03
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Manage Users">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:label id="label1" style="left: 144px; top: 24px; position: absolute" text="Ususarios"/>
                        <webuijsf:button actionExpression="#{ListUser.btManageGroup_action}" id="btManageGroup"
                            style="left: 23px; top: 168px; position: absolute" text="Manage Group"/>
                        <webuijsf:button actionExpression="#{ListUser.btDeleteUser_action}" id="btDeleteUser" style="left: 23px; top: 104px; position: absolute" text="Delete User"/>
                        <webuijsf:button actionExpression="#{ListUser.btLoad_action}" id="btLoad"
                            style="left: 23px; top: 48px; position: absolute; z-index: 500" text="Load List"/>
                        <webuijsf:button actionExpression="#{ListUser.btNewUser_action}" id="btNewUser" style="left: 23px; top: 76px; position: absolute" text="New User"/>
                        <webuijsf:textArea binding="#{ListUser.textAreaUsers}" columns="40" id="textAreaUsers" rows="10" style="height: 144px; left: 120px; top: 48px; position: absolute; width: 144px"/>
                        <webuijsf:button actionExpression="#{ListUser.btAssign_action}" id="btAssign" style="left: 24px; top: 134px; position: absolute" text="Assing Group"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
