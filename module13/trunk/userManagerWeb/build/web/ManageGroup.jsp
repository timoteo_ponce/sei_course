<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : ManageGroup
    Created on : 22-mar-2013, 11:59:33
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Manage Groups">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:label id="label1" style="left: 144px; top: 24px; position: absolute" text=" Groups"/>
                        <webuijsf:textArea binding="#{ManageGroup.textAreaGroups}" columns="40" id="textAreaGroups" rows="10" style="height: 144px; left: 120px; top: 48px; position: absolute; width: 144px"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btLoad_action}" id="btLoad1"
                            style="left: 23px; top: 48px; position: absolute; z-index: 500" text="List Groups"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btNewUser_action}" id="btNewGroup" style="left: 23px; top: 77px; position: absolute" text="New Group"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btDeleteUser_action}" id="btDeleteGroup"
                            style="left: 23px; top: 106px; position: absolute" text="Delete Group"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btManageGroup_action}" id="btManageUsers"
                            style="left: 95px; top: 240px; position: absolute" text="Manage Users"/>
                        <webuijsf:textArea binding="#{ManageGroup.textAreaUsers1}" columns="40" id="textAreaUsers1" rows="8" style="height: 124px; left: 504px; top: 72px; position: absolute; width: 160px"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btListUSers_action}" id="btListUSers"
                            style="left: 695px; top: 24px; position: absolute" text="List Users"/>
                        <webuijsf:textField binding="#{ManageGroup.txGroup}" columns="28" id="txGroup" label="Group :" style="left: 456px; top: 24px; position: absolute"/>
                        <webuijsf:label id="label2" style="left: 456px; top: 72px; position: absolute" text="User's"/>
                        <webuijsf:label id="label4" style="left: 456px; top: 96px; position: absolute" text="Group"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btRermoveUserG_action}" id="btRermoveUserG"
                            style="left: 695px; top: 216px; position: absolute" text="Remove User"/>
                        <webuijsf:textField binding="#{ManageGroup.txUser}" columns="28" id="txUser" label="User :" style="left: 456px; top: 216px; position: absolute"/>
                        <webuijsf:button actionExpression="#{ManageGroup.btAssignUsers_action}" id="btAssignUsers"
                            style="position: absolute; left: 240px; top: 240px" text="Assign Users"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
