<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : NewGroup
    Created on : 22-mar-2013, 16:21:52
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="New Group">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:textField binding="#{NewGroup.txName1}" id="txName1" label="Name :" style="left: 48px; top: 64px; position: absolute" valueChangeListenerExpression="#{NewGroup.txName_processValueChange}"/>
                        <webuijsf:button actionExpression="#{NewGroup.btNew_action}" id="btNew1" style="left: 47px; top: 136px; position: absolute" text="Save"/>
                        <webuijsf:button actionExpression="#{NewGroup.btCancel_action}" id="btCancel1" style="left: 167px; top: 136px; position: absolute" text="Cancel"/>
                        <webuijsf:label id="label1" style="left: 48px; top: 24px; position: absolute" text="Creacion de un nuevo grupo"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
