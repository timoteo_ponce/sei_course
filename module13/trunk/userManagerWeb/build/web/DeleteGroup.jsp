<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : DeleteGroup
    Created on : 22-mar-2013, 16:22:12
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:textField binding="#{DeleteGroup.txName1}" id="txName1" label="Group :" style="left: 48px; top: 72px; position: absolute" valueChangeListenerExpression="#{DeleteGroup.txName_processValueChange}"/>
                        <webuijsf:button actionExpression="#{DeleteGroup.btDelete_action}" id="btDelete" style="left: 47px; top: 144px; position: absolute" text="Delete"/>
                        <webuijsf:button actionExpression="#{DeleteGroup.btCancel_action}" id="btCancel1" style="left: 167px; top: 144px; position: absolute" text="Cancel"/>
                        <webuijsf:label id="label1" style="left: 48px; top: 24px; position: absolute" text="Esta usted seguro que desea eliminar el grupo?"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
