<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : AssingUserGroup
    Created on : 17-mar-2013, 9:38:11
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:textField binding="#{AssingUserGroup.txName}" id="txName" label="Name :" style="position: absolute; left: 72px; top: 24px"/>
                        <webuijsf:listbox binding="#{AssingUserGroup.listboxGroups}" id="listboxGroups"
                            items="#{AssingUserGroup.listboxGroupsDefaultOptions.options}" label="Groups" style="position: absolute; left: 72px; top: 72px"/>
                        <webuijsf:button id="button1" style="position: absolute; left: 72px; top: 288px" text="Assign"/>
                        <webuijsf:button actionExpression="#{AssingUserGroup.button2_action}" id="button2" style="position: absolute; left: 168px; top: 288px" text="Cancel"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
