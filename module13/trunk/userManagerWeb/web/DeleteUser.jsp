<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : DeleteUser
    Created on : 19-mar-2013, 19:41:22
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:textField binding="#{DeleteUser.txName1}" id="txName1" label="User :" style="left: 48px; top: 72px; position: absolute" valueChangeListenerExpression="#{DeleteUser.txName_processValueChange}"/>
                        <webuijsf:button actionExpression="#{DeleteUser.btDelete_action}" id="btDelete" style="left: 47px; top: 144px; position: absolute" text="Delete"/>
                        <webuijsf:button actionExpression="#{DeleteUser.btCancel_action}" id="btCancel1" style="left: 167px; top: 144px; position: absolute" text="Cancel"/>
                        <webuijsf:label id="label1" style="position: absolute; left: 48px; top: 24px" text="Esta usted seguro que desea eliminar el usuario?"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
