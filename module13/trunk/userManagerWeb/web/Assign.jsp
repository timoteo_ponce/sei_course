<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Assign
    Created on : 22-mar-2013, 22:19:53
    Author     : mary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:textField binding="#{Assign.txUser1}" columns="33" id="txUser1" label="User :" style="left: 24px; top: 24px; position: absolute"/>
                        <webuijsf:textArea binding="#{Assign.txAreaGroupsUser}" columns="40" id="txAreaGroupsUser" rows="10" style="height: 144px; left: 72px; top: 120px; position: absolute; width: 144px"/>
                        <webuijsf:label id="label1" style="left: 24px; top: 144px; position: absolute" text="User"/>
                        <webuijsf:label id="label2" style="left: 14px; top: 120px; position: absolute" text="Groupr's"/>
                        <webuijsf:button actionExpression="#{Assign.btList_action}" id="btList" style="left: 119px; top: 72px; position: absolute" text="Lis Group Assigned"/>
                        <webuijsf:label id="label3" style="left: 480px; top: 96px; position: absolute" text="All  Groups"/>
                        <webuijsf:textArea binding="#{Assign.txAreaAllGroups}" columns="40" id="txAreaAllGroups" rows="10" style="height: 144px; left: 408px; top: 120px; position: absolute; width: 144px"/>
                        <webuijsf:textField binding="#{Assign.txGroup1}" columns="28" id="txGroup1" label="Group :" style="left: 384px; top: 24px; position: absolute"/>
                        <webuijsf:button actionExpression="#{Assign.btAssign_action}" id="btAssign" style="left: 455px; top: 72px; position: absolute" text="Assign Group"/>
                        <webuijsf:button actionExpression="#{Assign.btManageGroup_action}" id="btManageUsers1"
                            style="left: 287px; top: 312px; position: absolute" text="Manage Users"/>
                        <webuijsf:button actionExpression="#{Assign.btManageGroup_action}" id="btManageGroup1"
                            style="left: 383px; top: 312px; position: absolute" text="Manage Group"/>
                        <webuijsf:button actionExpression="#{Assign.btRermoveUserG_action}" id="btRermoveUserG1"
                            style="left: 287px; top: 72px; position: absolute" text="Remove User"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
