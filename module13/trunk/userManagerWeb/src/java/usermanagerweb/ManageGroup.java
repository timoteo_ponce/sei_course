/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package usermanagerweb;

import com.sun.rave.faces.data.DefaultSelectItemsArray;
import com.sun.rave.web.ui.appbase.AbstractPageBean;
import com.sun.webui.jsf.component.TextArea;
import com.sun.webui.jsf.component.TextField;
import java.util.Iterator;
import java.util.List;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.ValueChangeEvent;
import usersmanager.core.UsersManager;
import usersmanager.core.UsersManagerFactory;
import usersmanager.model.Group;
import usersmanager.model.User;

/**
 * <p>Page bean that corresponds to a similarly named JSP page.  This
 * class contains component definitions (and initialization code) for
 * all components that you have defined on this page, as well as
 * lifecycle methods and event handlers where you may add behavior
 * to respond to incoming events.</p>
 *
 * @version ManageGroup.java
 * @version Created on 22-mar-2013, 11:59:34
 * @author mary
 */

public class ManageGroup extends AbstractPageBean {
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">

    /**
     * <p>Automatically managed component initialization.  <strong>WARNING:</strong>
     * This method is automatically generated, so any user-specified code inserted
     * here is subject to being replaced.</p>
     */
    private void _init() throws Exception {
    }
    private TextArea textAreaGroups = new TextArea();

    public TextArea getTextAreaGroups() {
        return textAreaGroups;
    }

    public void setTextAreaGroups(TextArea ta) {
        this.textAreaGroups = ta;
    }
    private TextField txGroup = new TextField();

    public TextField getTxGroup() {
        return txGroup;
    }

    public void setTxGroup(TextField tf) {
        this.txGroup = tf;
    }
    private TextArea textAreaUsers1 = new TextArea();

    public TextArea getTextAreaUsers1() {
        return textAreaUsers1;
    }

    public void setTextAreaUsers1(TextArea ta) {
        this.textAreaUsers1 = ta;
    }
    private DefaultSelectItemsArray dropdown1DefaultItems1 = new DefaultSelectItemsArray();

    public DefaultSelectItemsArray getDropdown1DefaultItems1() {
        return dropdown1DefaultItems1;
    }

    public void setDropdown1DefaultItems1(DefaultSelectItemsArray dsia) {
        this.dropdown1DefaultItems1 = dsia;
    }
    private DefaultSelectItemsArray dropdown1DefaultItems2 = new DefaultSelectItemsArray();

    public DefaultSelectItemsArray getDropdown1DefaultItems2() {
        return dropdown1DefaultItems2;
    }

    public void setDropdown1DefaultItems2(DefaultSelectItemsArray dsia) {
        this.dropdown1DefaultItems2 = dsia;
    }
    private DefaultSelectItemsArray dropdown1DefaultItems3 = new DefaultSelectItemsArray();

    public DefaultSelectItemsArray getDropdown1DefaultItems3() {
        return dropdown1DefaultItems3;
    }

    public void setDropdown1DefaultItems3(DefaultSelectItemsArray dsia) {
        this.dropdown1DefaultItems3 = dsia;
    }
    private TextField txUser = new TextField();

    public TextField getTxUser() {
        return txUser;
    }

    public void setTxUser(TextField tf) {
        this.txUser = tf;
    }

    // </editor-fold>

    /**
     * <p>Construct a new Page bean instance.</p>
     */
    public ManageGroup() {
    }

    /**
     * <p>Callback method that is called whenever a page is navigated to,
     * either directly via a URL, or indirectly via page navigation.
     * Customize this method to acquire resources that will be needed
     * for event handlers and lifecycle methods, whether or not this
     * page is performing post back processing.</p>
     * 
     * <p>Note that, if the current request is a postback, the property
     * values of the components do <strong>not</strong> represent any
     * values submitted with this request.  Instead, they represent the
     * property values that were saved for this view when it was rendered.</p>
     */
    @Override
    public void init() {
        // Perform initializations inherited from our superclass
        super.init();
        // Perform application initialization that must complete
        // *before* managed components are initialized
        // TODO - add your own initialiation code here
        
        // <editor-fold defaultstate="collapsed" desc="Managed Component Initialization">
        // Initialize automatically managed components
        // *Note* - this logic should NOT be modified
        try {
            _init();
        } catch (Exception e) {
            log("ManageGroup Initialization Failure", e);
            throw e instanceof FacesException ? (FacesException) e: new FacesException(e);
        }
        
        // </editor-fold>
        // Perform application initialization that must complete
        // *after* managed components are initialized
        // TODO - add your own initialization code here
        loadListGroups();
    }

    /**
     * <p>Callback method that is called after the component tree has been
     * restored, but before any event processing takes place.  This method
     * will <strong>only</strong> be called on a postback request that
     * is processing a form submit.  Customize this method to allocate
     * resources that will be required in your event handlers.</p>
     */
    @Override
    public void preprocess() {
    }

    /**
     * <p>Callback method that is called just before rendering takes place.
     * This method will <strong>only</strong> be called for the page that
     * will actually be rendered (and not, for example, on a page that
     * handled a postback and then navigated to a different page).  Customize
     * this method to allocate resources that will be required for rendering
     * this page.</p>
     */
    @Override
    public void prerender() {
    }

    /**
     * <p>Callback method that is called after rendering is completed for
     * this request, if <code>init()</code> was called (regardless of whether
     * or not this was the page that was actually rendered).  Customize this
     * method to release resources acquired in the <code>init()</code>,
     * <code>preprocess()</code>, or <code>prerender()</code> methods (or
     * acquired during execution of an event handler).</p>
     */
    @Override
    public void destroy() {
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    protected SessionBean1 getSessionBean1() {
        return (SessionBean1) getBean("SessionBean1");
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    protected RequestBean1 getRequestBean1() {
        return (RequestBean1) getBean("RequestBean1");
    }

    /**
     * <p>Return a reference to the scoped data bean.</p>
     *
     * @return reference to the scoped data bean
     */
    protected ApplicationBean1 getApplicationBean1() {
        return (ApplicationBean1) getBean("ApplicationBean1");
    }

    public String btLoad_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        loadListGroups();
        return null;
    }

    public String btNewUser_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        return "caseNewGroup";
    }

    public String btDeleteUser_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        return "caseDeleteGroup";
    }

    public String btManageGroup_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        return "goUsers";
    }

    private void loadListGroups() {
        String texto="";
        UsersManager userMa = UsersManagerFactory.newInstance();
        List<Group> liGroup= userMa.listGroups();
        System.out.println("lista de ="+liGroup.size());
        for (Iterator<Group> it = liGroup.iterator(); it.hasNext();) {
            Group group = it.next();
            if(texto.equals(""))
                texto=texto+group.getName();
            else
                texto=texto+"\n"+group.getName();
        }
        textAreaGroups.setText(texto);
    }

    public String btListUSers_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        String groupName = txGroup.getText().toString();
        String texto="";
        UsersManager userMa = UsersManagerFactory.newInstance();
        List<User> liGroup= userMa.listUsers(new Group(groupName));
        System.out.println("lista de ="+liGroup.size());
        for (Iterator<User> it = liGroup.iterator(); it.hasNext();) {
            User user = it.next();
            if(texto.equals(""))
                texto=texto+user.getName();
            else
                texto=texto+"\n"+user.getName();
        }
        textAreaUsers1.setText(texto);
        return null;
    }

    public String btRermoveUserG_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        String userName = txUser.getText().toString();
        String groupName = txGroup.getText().toString();
        UsersManager userMa = UsersManagerFactory.newInstance();
        userMa.removeGroup(new User(userName), new Group(groupName));
        btListUSers_action();
        return null;
    }

    public String btAssignUsers_action() {
        // TODO: Process the action. Return value is a navigation
        // case name where null will return to the same page.
        return "assignUsers";
    }

}

