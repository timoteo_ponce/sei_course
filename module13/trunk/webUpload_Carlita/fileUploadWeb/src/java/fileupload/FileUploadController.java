/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author root
 */
@ManagedBean
public class FileUploadController {

    public static final String IMAGES_HOME = System.getProperty("user.home") + "/file_uploads";

    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        FacesMessage msg = new FacesMessage("Exito", file.getFileName() + " cargado.");
        if (isValidImage(file)) {
            copyFileToSharedFolder(file);
        } else {
            msg = new FacesMessage("Error", file.getFileName() + " no es una imagen.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    private void copyFileToSharedFolder(UploadedFile file) {
        File dir = new File(IMAGES_HOME);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        String cleanFilename = file.getFileName().replaceAll(" ", "_");
        writeToFile(file.getContents(), new File(dir.getAbsolutePath() + "/" + cleanFilename));
    }

    private void close(Closeable item) {
        try {
            if (item != null) {
                item.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(FileUploadController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isValidImage(UploadedFile file) {        
        String type = file.getContentType().split("/")[0];
        return type.equals("image");
    }

    private void writeToFile(byte[] bytes, File file) {
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
            bos.flush();
        } catch (Exception ex) {
            Logger.getLogger(FileUploadController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            close(fos);
            close(bos);
        }
    }
}
