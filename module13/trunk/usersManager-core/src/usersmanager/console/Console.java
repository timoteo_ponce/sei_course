/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import usersmanager.model.Group;
import usersmanager.model.User;
import usersmanager.core.UsersManager;
import usersmanager.core.UsersManagerFactory;

/**
 *
 * @author timoteo
 */
public class Console {

    /*
     * > usersManager --create-user {username}
     * > usersManager --delete-user {username}
     * > usersManager --list-users 
     * > usersManager --assign-group {username} {groupName} 
     * > usersManager --remove-group {username} {groupName} 
     * > usersManager --list-groups {username (optional)} 
     * > usersManager --add-group {groupName} 
     * > usersManager --delete-group {groupName} 
     */
    @Option(name = "--create-user")
    private boolean createUser;
    @Option(name = "--delete-user")
    private boolean deleteUser;
    @Option(name = "--list-users")
    private boolean listUsers;
    @Option(name = "--assign-group")
    private boolean assignGroup;
    @Option(name = "--remove-group")
    private boolean removeGroup;
    @Option(name = "--delete-group")
    private boolean deleteGroup;
    @Option(name = "--add-group")
    private boolean addGroup;
    @Option(name = "--list-groups")
    private boolean listGroups;
    @Argument
    private List<String> arguments = new ArrayList<String>();
    private UsersManager manager = UsersManagerFactory.newInstance();

    public static void main(String[] args) {
        try {
            new Console().doMain(args);
        } catch (CmdLineException e) {
            print("Invalid command \n");
            print("USAGE : \n",
                    "\t > usersManager --create-user {username}\n",
                    "\t > usersManager --delete-user {username}\n",
                    "\t > usersManager --list-users \n",
                    "\t > usersManager --assign-group {username} {groupName} \n",
                    "\t > usersManager --remove-group {username} {groupName} \n",
                    "\t > usersManager --list-groups {username (optional)} \n",
                    "\t > usersManager --add-group {groupName} \n",
                    "\t > usersManager --delete-group {groupName} \n");
        }
    }

    public void doMain(String[] args) throws CmdLineException {
        CmdLineParser parser = new CmdLineParser(this);
        parser.setUsageWidth(80);

        parser.parseArgument(args);

        if (listGroups) {
            if (arguments.isEmpty()) {
                List<Group> groups = manager.listGroups();
                print(groups);
                print("* Listed [ " + groups.size() + " ] groups");
            } else {
                List<Group> groups = manager.listGroups(new User(arguments.get(0)));
                print(groups);
                print("* Listed [ " + groups.size() + " ] groups for user: " + arguments.get(0));
            }
        } else if (listUsers) {
            List<User> users = manager.listUsers();
            print(users);
            print("* Listed [ " + users.size() + " ] users");
        }
        executeArgumentedCommands(listGroups || listUsers, parser);
    }

    private static void print(String... strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }

    private <T> void print(Collection<T> objects) {
        for (T item : objects) {
            System.out.println(item);
        }
    }

    private void executeArgumentedCommands(boolean alreadyExecuted, CmdLineParser parser) throws CmdLineException {
        if (!alreadyExecuted && arguments.isEmpty()) {
            throw new CmdLineException(parser, "No arguments provided");
        }
        String firstArg = arguments.isEmpty() ? "" : arguments.get(0);
        String secondArg = arguments.size() < 2 ? "" : arguments.get(1);

        if (createUser) {
            print("User added: " + manager.addUser(firstArg));
        } else if (assignGroup) {
            User user = new User(firstArg);
            manager.assignGroup(user, new Group(secondArg));
            List<Group> groups = manager.listGroups(user);
            print("User [ " + user + " ] groups: " + groups);
        } else if (removeGroup) {
            User user = new User(firstArg);
            manager.removeGroup(user, new Group(secondArg));
            List<Group> groups = manager.listGroups(user);
            print("User [ " + user + " ] groups: " + groups);
        } else if (deleteGroup) {
            manager.deleteGroup(new Group(firstArg));
            print("Group deleted: " + firstArg);
        } else if (addGroup) {
            manager.addGroup(firstArg);
            print("Group added: " + firstArg);
        } else if (deleteUser) {
            manager.deleteUser(new User(firstArg));
            print("User deleted: " + firstArg);
        }
    }
}
