/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import usersmanager.model.User;
import usersmanager.model.Group;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author timoteo
 */
public class Win2SystemInterface implements SystemInterface {

    private static Logger log = Logger.getLogger(Win2SystemInterface.class.getName());

    public Win2SystemInterface() {
    }

    @Override
    public User addUser(String name) {
        try {
            CoreUtils.createProcess("net", "user", name, "/ADD");
            log.info("user added : " + name);
            return new User(name);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void deleteUser(User user) {
        try {
            CoreUtils.createProcess("net", "user", user.getName(), "/DELETE");
            log.info("user deleted : " + user);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<User> listUsers() {
        try {
            Process process = CoreUtils.createProcess("net", "user");
            List<String> lines = CoreUtils.toStringLines(process.getInputStream());
            List<User> users = new ArrayList<User>();
            for (int i = 4; i < lines.size()-2; i++) {
                System.out.println("## "+lines.get(i));
                String[] items = lines.get(i).split(" ");
                int t=items.length;
                for(int j=0;j<t;j++){
                    String token=items[j];
                    token=token.trim();
                    if((token!=null) && !(token.equalsIgnoreCase("")) && !(token.equalsIgnoreCase(" "))){
                        users.add(new User(token));
                        System.out.println("usuario="+token);
                    }
                }
            }
            return users;
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void assignGroup(User user, Group group) {
        try {
            CoreUtils.createProcess("net","localgroup",group.getName(),user.getName(),"/ADD");
            log.info("group assigned : "+user+">"+group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void removeGroup(User user, Group group) {
        try {
            CoreUtils.createProcess("net","localgroup",group.getName(),user.getName(),"/DELETE");
            log.info("group removed : "+user+">"+group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<Group> listGroups(User user) {        
        try {
            Process process = CoreUtils.createRawProcess("net", "user",user.getName());            
            List<String> lines = CoreUtils.toStringLines(process.getInputStream());            
            List<Group> groups = new ArrayList<Group>();            
            
            for (String item:lines) {                
                if(item.contains("*")){                    
                    String[] array = item.split("\\s+");
                    groups.add(new Group(array[array.length-1].replaceAll("\\*", "")));                    
                }
            }            
            return groups;
        } catch (Exception ex) {            
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<Group> listGroups() {
        try {
            Process process = CoreUtils.createProcess("net", "localgroup");
            List<String> lines = CoreUtils.toStringLines(process.getInputStream());
            List<Group> groups = new ArrayList<Group>();

            for (int i = 4; i < lines.size()-2; i++) {
                System.out.println(lines.get(i).replaceAll("\\*","").trim());
                groups.add(new Group(lines.get(i).replaceAll("\\*","").trim()));
            }
            return groups;
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<User> listUsers(Group group) {
        try {
            Process process = CoreUtils.createRawProcess("net", "localgroup",group.getName());
            List<String> lines = CoreUtils.toStringLines(process.getInputStream());
            List<User> users = new ArrayList<User>();
            System.out.println(lines);
            for (int i = 6; i < lines.size()-2; i++) {
                users.add(new User(lines.get(i).trim()));
                System.out.println("usuario = "+lines.get(i).trim());
            }
            return users;
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Group addGroup(String name) {
        try {
            CoreUtils.createProcess("net", "localgroup", name, "/ADD");
            log.info("group added : " + name);
            return new Group(name);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void deleteGroup(Group group) {
        try {
            CoreUtils.createProcess("net", "localgroup", group.getName(), "/DELETE");
            log.info("group deleted : " + group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }
}
