/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import java.util.Collections;
import usersmanager.model.User;
import usersmanager.model.Group;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author timoteo
 */
public class UsersManager {

    private static final Logger log = Logger.getLogger(UsersManager.class.getName());
    private final SystemInterface sysInterface;

    public UsersManager(SystemInterface sysInterface) {
        this.sysInterface = sysInterface;
    }

    public User addUser(String name) {
        User user = new User(name);
        if (userExists(user)) {
            log.info("User already exists : " + user);
            user = user.NULL;
        } else {
            user = sysInterface.addUser(name);
        }
        return user;
    }

    public void deleteUser(User user) {
        if (userExists(user)) {
            sysInterface.deleteUser(user);
        } else {
            log.info("Inexistent user : " + user);
        }
    }

    public List<User> listUsers() {
        return sysInterface.listUsers();
    }

    public List<User> listUsers(Group group) {
        return sysInterface.listUsers(group);
    }
    public void assignGroup(User user, Group group) {
        if (userExists(user) && groupExists(group)) {
            sysInterface.assignGroup(user, group);
        } else {
            log.info("Both user and group must exists: " + user + " , " + group);
        }
    }

    public void removeGroup(User user, Group group) {
        if (userExists(user) && groupExists(group)) {
            sysInterface.removeGroup(user, group);
        } else {
            log.info("Both user and group must exists: " + user + " , " + group);
        }
    }

    public List<Group> listGroups(User user) {
        if (userExists(user)) {
            return sysInterface.listGroups(user);
        } else {
            log.info("Inexistent user : " + user);
        }
        return Collections.EMPTY_LIST;
    }

    public List<Group> listGroups() {
        return sysInterface.listGroups();
    }

    public Group addGroup(String name){
        Group group = new Group(name);
        if (groupExists(group)) {
            log.info("Group already exists : " + group);
            group = group.NULL;
        } else {
            group = sysInterface.addGroup(name);
        }
        return group;
    }

    public void deleteGroup(Group group){
        if(groupExists(group)){
            sysInterface.deleteGroup(group);
        } else {
            log.info("Inexistent group : " + group);
        }
    }

    private boolean groupExists(Group group) {
        return sysInterface.listGroups().contains(group);
    }

    private boolean userExists(User user) {
        return sysInterface.listUsers().contains(user);
    }
}
