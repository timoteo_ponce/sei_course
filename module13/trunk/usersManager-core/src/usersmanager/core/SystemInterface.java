/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import java.util.List;
import usersmanager.model.Group;
import usersmanager.model.User;

/**
 *
 * @author root
 */
public interface SystemInterface {
 
    User addUser(String name);
    
    void deleteUser(User user);
    
    List<User> listUsers();
    
    void assignGroup(User user,Group group);
    
    void removeGroup(User user,Group  group);
    
    List<Group> listGroups(User user);

    List<Group> listGroups();

    List<User> listUsers(Group group);
    
    Group addGroup(String name);
    
    void deleteGroup(Group group);
}
