/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author timoteo
 */
public class CoreUtils {

    public static Process createProcess(String... commands) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder(commands);
        Process process = builder.start();
        if (process.waitFor() != 0) {
            throw new IllegalStateException("use sudo");
        }
        return process;
    }
    
    public static Process createRawProcess(String... commands) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder(commands);
        return builder.start();
    }

    public static List<String> readLines(String filename) {
        File file = new File(filename);
        InputStream stream = null;
        try {
            stream = new FileInputStream(file);
            return toStringLines(stream);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(stream);
        }
    }

    public static String toString(InputStream stream) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder buffer = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }
            return buffer.toString();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        } finally {
            close(reader);
        }
    }

    static List<String> toStringLines(InputStream inputStream) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            List<String> lines = new ArrayList<String>();
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            return lines;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        } finally {
            close(reader);
        }
    }

    static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
        }
    }
}
