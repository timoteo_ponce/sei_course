/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import java.util.logging.Logger;

/**
 *
 * @author timoteo
 */
public class UsersManagerFactory {

    private static Logger log = Logger.getLogger(UsersManagerFactory.class.getName());

    public static UsersManager newInstance() {
        String osName = System.getProperty("os.name").toLowerCase();
        log.info("OS name : " + osName);
        SystemInterface sysInterface;
        if (osName.contains("windows")) {
            sysInterface = new Win2SystemInterface();
        } else {
            sysInterface = new UnixSystemInterface();
        }
        return new UsersManager(sysInterface);
    }
}
