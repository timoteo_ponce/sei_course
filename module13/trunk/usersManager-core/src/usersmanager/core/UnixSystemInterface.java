/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import usersmanager.model.User;
import usersmanager.model.Group;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author timoteo
 */
public class UnixSystemInterface implements SystemInterface {

    private static Logger log = Logger.getLogger(UnixSystemInterface.class.getName());

    public UnixSystemInterface() {
    }

    @Override
    public User addUser(String name) {
        try {
            CoreUtils.createProcess("useradd", "-M", "-N", name);
            log.info("user added : " + name);
            return new User(name);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void deleteUser(User user) {
        try {
            CoreUtils.createProcess("userdel", user.getName());
            log.info("user deleted : " + user);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<User> listUsers() {
        List<String> lines = CoreUtils.readLines("/etc/passwd");
        List<User> users = new ArrayList<User>();
        for (String line : lines) {
            String username = line.substring(0, line.indexOf(":"));
            users.add(new User(username));
        }
        return users;
    }

    @Override
    public List<Group> listGroups() {
        List<String> lines = CoreUtils.readLines("/etc/group");
        List<Group> group = new ArrayList<Group>();
        for (String line : lines) {
            String name = line.substring(0, line.indexOf(":"));
            group.add(new Group(name));
        }
        return group;
    }

    @Override
    public void assignGroup(User user, Group group) {
        try {
            CoreUtils.createProcess("usermod", "-a", "-G", group.getName(), user.getName());
            log.info("group assigned : " + user + ">" + group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void removeGroup(User user, Group group) {
        try {
            CoreUtils.createProcess("gpasswd", "-d", user.getName(), group.getName());
            log.info("group removed : " + user + ">" + group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<Group> listGroups(User user) {
        try {
            Process process = CoreUtils.createProcess("groups", user.getName());
            String output = CoreUtils.toString(process.getInputStream());
            String groupsRaw = output.substring(output.indexOf(":") + 1);
            List<Group> groups = new ArrayList<Group>();

            for (String item : groupsRaw.split(" ")) {
                if (!item.trim().isEmpty()) {
                    groups.add(new Group(item.trim()));
                }
            }
            return groups;
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Group addGroup(String name) {
        try {
            CoreUtils.createProcess("groupadd", name);
            log.info("Group added : " + name);
            return new Group(name);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void deleteGroup(Group group) {
        try {
            CoreUtils.createRawProcess("groupdel", group.getName());
            log.info("group deleted : " + group);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public List<User> listUsers(Group group) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
