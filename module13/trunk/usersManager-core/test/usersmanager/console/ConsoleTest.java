/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.console;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.kohsuke.args4j.CmdLineException;
import usersmanager.model.Group;
import usersmanager.model.User;
import usersmanager.core.UsersManager;
import usersmanager.core.UsersManagerFactory;

/**
 *
 * @author root
 */
public class ConsoleTest {

    Console console = new Console();
            
            
    final String testUser = "myTestUser";
    
    final String testGroup = "myTestGroup";
    
    private UsersManager manager = UsersManagerFactory.newInstance();
    
    @Before
    @After
    public void clean(){
        User user = new User(testUser);
        Group group = new Group(testGroup);
        if(manager.listUsers().contains(user)){
            manager.deleteUser(user);
        }
        if(manager.listGroups().contains(group)){
            manager.deleteGroup(group);
        }
    }


    @Test
    public void testCreateUser() throws CmdLineException {
        console.doMain(new String[]{"--create-user", "myTestUser"});
    }
    
     @Test
    public void testListUsers() throws CmdLineException {
        console.doMain(new String[]{"--list-users"});
    }
}
