/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersmanager.core;

import usersmanager.model.Group;
import usersmanager.model.User;
import org.junit.After;
import org.junit.Before;
import static junit.framework.Assert.*;
import org.junit.Test;

/**
 *
 * @author timoteo
 */
public class UsersManagerTest {

    private UsersManager manager = UsersManagerFactory.newInstance();
    
    final String testUser = "myTestUser";
    
    final String testGroup = "myTestGroup";
    
    @Before
    @After
    public void clean(){
        User user = new User(testUser);
        Group group = new Group(testGroup);
        if(manager.listUsers().contains(user)){
            manager.deleteUser(user);
        }
        if(manager.listGroups().contains(group)){
            manager.deleteGroup(group);
        }
    }

    @Test
    public void testAddUser() {
        User user = manager.addUser(testUser);        
        assertTrue(manager.listUsers().contains(user));
    }
    
    @Test
    public void testDeleteUser() {
        User user = manager.addUser(testUser);        
        manager.deleteUser(user);        
        assertFalse(manager.listUsers().contains(user));
    }
    
    @Test
    public void testAssignGroup() {
        Group group = manager.addGroup(testGroup);
        User user = manager.addUser(testUser);        
        manager.assignGroup(user, group);
        assertTrue(manager.listGroups(user).contains(group));
    }
    
    @Test
    public void testRemoveFromGroup() {
        Group group = manager.addGroup(testGroup);
        User user = manager.addUser(testUser);        
        manager.assignGroup(user, group);
        manager.removeGroup(user, group);
        assertFalse(manager.listGroups(user).contains(group));
    }
}
