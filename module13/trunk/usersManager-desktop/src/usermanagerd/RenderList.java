/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package usermanagerd;

import java.awt.Component;
import java.awt.Font;
import java.util.Hashtable;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Roger
 */
public class RenderList extends JLabel implements ListCellRenderer{

    Hashtable<Object, ImageIcon> elementos;
    ImageIcon icononulo=new ImageIcon(this.getClass().getResource("/img/null.jpg"));

    public RenderList(Iterator items, int size, int type){
        Iterator usersList = items;
        elementos=new Hashtable<Object, ImageIcon>();
        ImageIcon icons[] = new ImageIcon[size];
        int i = 0;
        while(usersList.hasNext()) {
            if(type == 1)
                icons[i] = new ImageIcon(this.getClass().getResource("/img/maleIcon0.png"));
            else
                icons[i] = new ImageIcon(this.getClass().getResource("/img/group0.png"));
            Object user = usersList.next();
            elementos.put(user, icons[i]);
            i++;
        }
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,int index, boolean isSelected, boolean cellHasFocus) {
        if(elementos.get(value)!=null){
            setIcon(elementos.get(value));
            setText(""+value);
            if(isSelected){
                setFont(new Font("Verdana",Font.BOLD,16));
            }else{
                setFont(null);
            }
        }else{
            setIcon(icononulo);
            setText(""+value);
            if(isSelected){
                setFont(new Font("Verdana",Font.BOLD,16));
            }else{
                setFont(null);
            }
        }
        return this;
    }
}
