/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;

/**
 *
 * @author timoteo
 */
@ManagedBean
@RequestScoped
public class ImageOperations {

    private static final Logger log = Logger.getLogger(ImageOperations.class.getName());
    //transform format -> check http://stackoverflow.com/questions/4207896/java-convert-gif-image-to-png-format
    // DONE
    //watermark image -> 
    // DONE
    //crop -> input new dimensions and update current image
    //resize -> input new dimensions and update current image
    //DONE
    //security -> crop images after upload
    //DONE
    private String targetFormat;
    private String selectedImage;
    private String height = "400", width = "400", 
            pointX = "020", pointY = "020";
    private String watermarkText = "letargo";

    public void delete() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String image = params.get("image");
        File toDelete = new File(FileUploadController.IMAGES_HOME + "/" + image);
        toDelete.delete();
        log.info("File deleted : " + image);
    }

    public void transform() {
        try {
            File input = new File(FileUploadController.IMAGES_HOME + "/" + selectedImage);
            File output = new File(replaceExtension(input.getPath(), targetFormat));
            ImageIO.write(ImageIO.read(input), targetFormat, output);
            log.info("Transformed image saved: " + output.getPath());
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    String replaceExtension(String filename, String newExtension) {
        String base = filename.substring(0, filename.lastIndexOf(".") + 1);
        return base + newExtension;
    }

    public void resize() {
        try {
            File file = new File(FileUploadController.IMAGES_HOME + "/" + selectedImage);
            BufferedImage img = ImageIO.read(file);
            BufferedImage newImg = Scalr.resize(img, Integer.valueOf(width), Integer.valueOf(height));
            ImageIO.write(newImg, FileUploadController.getExtension(file), file);
            log.info("Resized image saved: " + file.getPath() + " to width=" + width
                    + "height=" + height);
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    public void waterMark() {
        String imgFile = FileUploadController.IMAGES_HOME + "/" + selectedImage;
        Font font = new Font("Arial", Font.BOLD, 30);
        ImageWatermark.markText(imgFile, watermarkText, font, Color.gray, 5, 5);
        log.info("Watermark applied to: " + imgFile);
    }

    public void crop() {
        try {
            File file = new File(FileUploadController.IMAGES_HOME + "/" + selectedImage);
            File newFile = new File(FileUploadController.IMAGES_HOME + "/" + System.nanoTime() + selectedImage);
            BufferedImage img = ImageIO.read(file);
            BufferedImage newImg = Scalr.crop(img,
                    Integer.valueOf(pointX), Integer.valueOf(pointY),
                    Integer.valueOf(width), Integer.valueOf(height));
            ImageIO.write(newImg,
                    FileUploadController.getExtension(file),
                    newFile);
            log.info("Cropped image saved: " + newFile.getPath() + " to width=" + width
                    + "height=" + height);
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    public String getTargetFormat() {
        return targetFormat;
    }

    public void setTargetFormat(String targetFormat) {
        this.targetFormat = targetFormat;
    }

    public String getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(String selectedImage) {
        this.selectedImage = selectedImage;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getWatermarkText() {
        return watermarkText;
    }

    public void setWatermarkText(String watermarkText) {
        this.watermarkText = watermarkText;
    }

    public String getPointX() {
        return pointX;
    }

    public void setPointX(String pointX) {
        this.pointX = pointX;
    }

    public String getPointY() {
        return pointY;
    }

    public void setPointY(String pointY) {
        this.pointY = pointY;
    }
}
