/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author root
 */
@ManagedBean
public class FileUploadController {

    private static final Logger log = Logger.getLogger(FileUploadController.class.getName());
    public static final String IMAGES_HOME = System.getProperty("user.home") + "/file_uploads";

    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        FacesMessage msg = new FacesMessage("Exito", file.getFileName() + " cargado.");
        if (isValidImage(file)) {
            File localFile = copyFileToSharedFolder(file);
            secureUploadedImage(localFile);
        } else {
            msg = new FacesMessage("Error", file.getFileName() + " no es una imagen.");
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    private File copyFileToSharedFolder(UploadedFile file) {
        File dir = new File(IMAGES_HOME);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        String cleanFilename = file.getFileName().replaceAll(" ", "_");
        File imageFile = new File(dir.getAbsolutePath() + "/" + cleanFilename);
        writeToFile(file.getContents(), imageFile);
        return imageFile;
    }

    private void close(Closeable item) {
        try {
            if (item != null) {
                item.close();
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    private boolean isValidImage(UploadedFile file) {
        String type = file.getContentType().split("/")[0];
        return type.equals("image");
    }

    private void writeToFile(byte[] bytes, File file) {
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
            bos.flush();
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
        } finally {
            close(fos);
            close(bos);
        }
    }

    private void secureUploadedImage(File localFile) {
        try {
            log.info("Cropping image file for security: " + getMetadata(localFile));
            BufferedImage image = ImageIO.read(localFile);
            BufferedImage newImage = Scalr.crop(image, image.getWidth(), image.getHeight() - 1);
            Graphics2D graphics = newImage.createGraphics();
            graphics.drawImage(newImage, 0, 0, null);
            graphics.dispose();
            
            ImageIO.write(newImage, getExtension(localFile), localFile);
            log.info("Saved cropped [ " + getExtension(localFile) + " ]image to: " + getMetadata(localFile));
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    public static String getExtension(File file) {
        String name = file.getName();
        return name.substring(name.lastIndexOf(".") + 1, name.length());
    }

    private String getMetadata(File localFile) {
        return "[ name = " + localFile.getName() + ",size = " + localFile.length() + "]";
    }
}
