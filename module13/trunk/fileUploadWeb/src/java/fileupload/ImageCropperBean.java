/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

/**
 *
 * @author timoteo
 */
import java.io.File;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.imageio.stream.FileImageOutputStream;

import org.primefaces.model.CroppedImage;

@ManagedBean
@ViewScoped
public class ImageCropperBean implements Serializable{
    
    private static final Logger log = Logger.getLogger(ImageCropperBean.class.getName());

    private CroppedImage croppedImage;
    private String newImageName = "a";
    private String imageToCrop;

    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    public String crop() throws Exception{
        log.fine("cropping....");
        if (croppedImage == null) {
            log.fine("dafuq");
            return null;
        }

        setNewImageName(getRandomImageName());
        String newFileName = FileUploadController.IMAGES_HOME + File.separator + getNewImageName() + ".jpg";

        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(newFileName));
            imageOutput.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
            imageOutput.close();
            log.fine("new: " + newFileName);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } 

        return null;
    }

    private String getRandomImageName() {
        int i = (int) (Math.random() * 100000);

        return String.valueOf(i);
    }

    public String getNewImageName() {
        return newImageName;
    }

    public void setNewImageName(String newImageName) {
        this.newImageName = newImageName;
    }

    public String getImageToCrop() {
        return imageToCrop;
    }

    public void setImageToCrop(String imageToCrop) {
        this.imageToCrop = imageToCrop;
    }
}
