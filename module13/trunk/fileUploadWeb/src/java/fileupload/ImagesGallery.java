/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileupload;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author timoteo
 */
@javax.faces.bean.ManagedBean
public class ImagesGallery {
    
    public List<String> getImages(){
        File dir = new File(FileUploadController.IMAGES_HOME);
        List<String> result = Collections.EMPTY_LIST;
        
        if(dir.exists()){
            result = new ArrayList<String>();
            for(File file:dir.listFiles()){
                result.add(file.getName());
            }
        }
        return result;
    }
    
    public List<SelectItem> getImageSelects(){
        List<SelectItem> result = new ArrayList<SelectItem>();
        for(String item:getImages()){
            result.add(new SelectItem(item, item));
        }
        return result;
    }
    
}
