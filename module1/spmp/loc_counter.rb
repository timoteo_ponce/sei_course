# lines of code
def count_files( base_dir )
	file_names = Dir.glob( base_dir + "/**/*.{java,py,sh,pl}")
	
	puts "Counting LOC in [ #{file_names.length} ] files in folder : #{base_dir}"
	total_count = 0
	file_names.each do |file|
		total_count += count_file file
	end 
	puts "Total LOC: #{total_count}"
end

def count_file( file_name )
	# puts "Counting file [ #{file_name} ]"
	file = File.open(file_name)
	count = 0
	file.each do |line|
		real_line = line.delete(" \n\r")
  		unless real_line.empty?
			count += 1
		end
	end
	return count
end

count_files "similar_apps/Openbravo-3.0MP1"
count_files "similar_apps/openerp-server-5.0.16"
