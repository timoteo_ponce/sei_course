/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.ui;

import client.LevelEnum;
import client.LoggerClient;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

/**
 *
 * @author timoteo
 */
@ManagedBean(name="loggerClientView")
@RequestScoped
public class LoggerClientView {
    
    private String host = "localhost";
    
    private String message;
    
    private LevelEnum logLevel;
    
    private final LoggerClient loggerClient = new LoggerClient();

    /** Creates a new instance of LoggerClientView */
    public LoggerClientView() {
    }
    
    public void logMessage(){
        loggerClient.connect("webClient", host, 1055);
        loggerClient.log(message, logLevel.getLevel());
    }
    
    public List<SelectItem> getLogLevels(){
        List<SelectItem> list = new ArrayList<SelectItem>();
        for(LevelEnum item:LevelEnum.values()){
            list.add(new SelectItem(item,item.name()));
        }
        return list;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LevelEnum getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LevelEnum logLevel) {
        this.logLevel = logLevel;
    }
    
    
    
    
}
