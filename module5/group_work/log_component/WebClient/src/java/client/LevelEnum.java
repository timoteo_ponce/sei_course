/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import LogComponent.Level;

/**
 *
 * @author timoteo
 */
public enum LevelEnum {

    TRACE(Level.TRACE), DEBUG(Level.DEBUG), ERROR(Level.ERROR), INFO(Level.INFO), WARN(Level.WARN);
    private final Level level;

    private LevelEnum(Level level) {
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }
}
