/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package definition;

import LogComponent.LoggerPOA;
import LogComponent.LoggerSettings;
import java.util.Enumeration;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 *
 * @author timoteo
 */
public class LoggerImpl extends LoggerPOA {

    private Logger logger = Logger.getLogger(getClass());
    private RollingFileAppender fileAppender;

    public LoggerImpl() {
        org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
        fileAppender = (RollingFileAppender) rootLogger.getAppender("logfile");
    }

    @Override
    public void trace(String identifier, String message) {
        logger.trace(getMessage( identifier ,message));
    }

    @Override
    public void debug(String identifier, String message) {
        logger.debug(getMessage( identifier ,message));
    }

    @Override
    public void info(String identifier, String message) {
        logger.info(getMessage( identifier ,message));
    }

    @Override
    public void error(String identifier, String message) {
        logger.error(getMessage( identifier ,message));
    }

    @Override
    public void warn(String identifier, String message) {
        logger.warn(getMessage( identifier ,message));
    }

    private void setPattern(String pattern) {
        org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
        Enumeration en = rootLogger.getAllAppenders();
        while (en.hasMoreElements()) {
            Appender appender = (Appender) en.nextElement();
            PatternLayout layout = (PatternLayout) appender.getLayout();
            layout.setConversionPattern(pattern);
        }
        logger.info("Pattern changed : " + pattern);
    }

    private String getPattern() {
        PatternLayout layout = (PatternLayout) fileAppender.getLayout();
        return layout.getConversionPattern();
    }

    private void setMaxBackupIndex(short maxBackupIndex) {
        fileAppender.setMaxBackupIndex(maxBackupIndex);
    }

    private void setMaxFileSize(String maxFileSize) {
        fileAppender.setMaxFileSize(maxFileSize);
    }

    private short getMaxBackupIndex() {
        return (short) fileAppender.getMaxBackupIndex();
    }

    private String getMaxFileSize() {
        return Long.toString(fileAppender.getMaximumFileSize());
    }

    @Override
    public LoggerSettings getSettings() {
        return new LoggerSettings(getPattern(), getMaxBackupIndex(), getMaxFileSize());
    }

    @Override
    public void setSettings(LoggerSettings settings) {
        setPattern(settings.pattern);
        setMaxBackupIndex((short) settings.shortMaxBackupIndex);
        setMaxFileSize(settings.maxFileSize);
    }

    private Object getMessage(String identifier, String message) {
        return " [" + identifier + "] " + message;
    }
}
