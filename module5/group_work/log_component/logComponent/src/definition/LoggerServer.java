/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package definition;

import LogComponent.Logger;
import LogComponent.LoggerHelper;
import LogComponent.LoggerSettings;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.timo.gitconfig.Configuration;
import org.timo.gitconfig.GitConfiguration;

/**
 *
 * @author timoteo
 */
public class LoggerServer {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LoggerServer.class);
    private final String[] args = new String[]{"-ORBInitialPort", "1055", "-ORBInitialHost", "localhost"};
    private Configuration configuration = new GitConfiguration();
    private LoggerImpl loggerImpl;

    public void startServer() {
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get reference to rootpoa & activate the POAManager
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            // create servant and register it with the ORB
            loggerImpl = new LoggerImpl();
            loadSettings();
            startConfigMonitor();

            // get object reference from the servant
            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(loggerImpl);
            Logger href = LoggerHelper.narrow(ref);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
            // Use NamingContextExt which is part of the Interoperable
            // Naming Service (INS) specification.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // bind the Object Reference in Naming
            String name = "Logger";
            NameComponent path[] = ncRef.to_name(name);
            ncRef.rebind(path, href);

            logger.info("LoggerServer ready and waiting > " + Arrays.toString(args));
            // wait for invocations from clients
            orb.run();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("Exiting LoggerServer...");
    }

    public static void main(String[] args) {
        new LoggerServer().startServer();
    }

    private void loadSettings() throws Exception {
        configuration.load("conf/service.conf");
        LoggerSettings loggerSettings = new LoggerSettings(configuration.getValue("service.pattern"),
                configuration.getInt("file.maxBackupIndex").shortValue(),
                configuration.getValue("file.maxSize"));
        loggerImpl.setSettings(loggerSettings);
    }

    private void startConfigMonitor() throws Exception {

        FileAlterationObserver fileAlterationObserver = new FileAlterationObserver("conf");
        FileAlterationMonitor monitor = new FileAlterationMonitor(5, fileAlterationObserver);
        fileAlterationObserver.addListener(new FileAlterationListener() {

            @Override
            public void onStart(FileAlterationObserver fao) {
            }

            @Override
            public void onDirectoryCreate(File file) {                
            }

            @Override
            public void onDirectoryChange(File file) {                
            }

            @Override
            public void onDirectoryDelete(File file) {                
            }

            @Override
            public void onFileCreate(File file) {
                try {
                    loadSettings();
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(LoggerServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void onFileChange(File file) {
                try {
                    loadSettings();
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(LoggerServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void onFileDelete(File file) {                
            }

            @Override
            public void onStop(FileAlterationObserver fao) {
            }
        });
        monitor.start();
    }
}
