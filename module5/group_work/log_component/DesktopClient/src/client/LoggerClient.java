/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import LogComponent.Level;
import LogComponent.Logger;
import LogComponent.LoggerHelper;
import LogComponent.LoggerSettings;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

/**
 *
 * @author timoteo
 */
public class LoggerClient {

    private String[] args = new String[]{"-ORBInitialPort", "1055", "-ORBInitialHost", "localhost"};
    private Logger logger;
    private String identifier;

    public void connect(String identifier, String host, int port) {
        this.identifier = identifier;
        try {
            args[1] = Integer.toString(port);
            args[3] = host;
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is 
            // part of the Interoperable naming Service.  
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            String name = "Logger";
            logger = LoggerHelper.narrow(ncRef.resolve_str(name));

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void log(String message, Level level) {
        if (level.equals(Level.DEBUG)) {
            logger.debug(identifier,message);
        } else if (level.equals(Level.ERROR)) {
            logger.error(identifier,message);
        } else if (level.equals(Level.INFO)) {
            logger.info(identifier,message);
        } else if (level.equals(Level.TRACE)) {
            logger.trace(identifier,message);
        } else if (level.equals(Level.WARN)) {
            logger.warn(identifier,message);
        }
    }

    public LoggerSettings getLoggerSettings() {
        return logger.getSettings();
    }

    public void updateSettings(String pattern, int maxBackupFiles, String maxFileSize) {
        LoggerSettings loggerSettings = new LoggerSettings(pattern, (short) maxBackupFiles, maxFileSize);
        logger.setSettings(loggerSettings);
    }
}
