/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import ejb.ChatMessage;
import ejb.ChatRoom;
import java.util.List;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author timoteo
 */
@ManagedBean(name = "chatView")
@SessionScoped
public class ChatView {

    @EJB
    private ChatRoom chatRoom;
    private String nickname;
    private String message;

    /** Creates a new instance of ChatView */
    public ChatView() {
    }

    public List<ChatMessage> getMessagesList() {
        return chatRoom.getMessages();        
    }

    public void addMessage() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessage(message);
        chatMessage.setUsername(nickname);
        this.chatRoom.addMessage(chatMessage);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
