/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author timoteo
 */
@Singleton
@LocalBean
public class ChatRoom {
    
    @EJB
    private LogService logService;
    
    @PersistenceContext(unitName="ChatRoom-ejbPU")
    private EntityManager entityManager;    
    
    public void addMessage(ChatMessage message) {
        entityManager.persist(message);
        logService.log(message.toString());
    }
    
    public List<ChatMessage> getMessages() {
        CriteriaQuery<ChatMessage> query = entityManager.getCriteriaBuilder().createQuery(ChatMessage.class);        
        return entityManager.createQuery(query).getResultList();
    }
}
