/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author timoteo
 */
@Stateless
@LocalBean
public class LogService {

    public void log(final String message) {
        Logger.getLogger(getClass().getName()).info(message);        
    }
}
