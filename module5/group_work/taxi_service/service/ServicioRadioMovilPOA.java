package service;


/**
* service/ServicioRadioMovilPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from servicioRadioMovil.idl
* domingo 11 de diciembre de 2011 12H01' GMT-04:00
*/

public abstract class ServicioRadioMovilPOA extends org.omg.PortableServer.Servant
 implements service.ServicioRadioMovilOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("solicitarCarrera", new java.lang.Integer (0));
    _methods.put ("solicitarEncargoCompra", new java.lang.Integer (1));
    _methods.put ("solicitarEncargoTransporte", new java.lang.Integer (2));
    _methods.put ("solicitarAlquilerMovil", new java.lang.Integer (3));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // service/ServicioRadioMovil/solicitarCarrera
       {
         String cliente = in.read_string ();
         String origen = in.read_string ();
         String destino = in.read_string ();
         this.solicitarCarrera (cliente, origen, destino);
         out = $rh.createReply();
         break;
       }

       case 1:  // service/ServicioRadioMovil/solicitarEncargoCompra
       {
         String cliente = in.read_string ();
         String origen = in.read_string ();
         String destino = in.read_string ();
         String producto = in.read_string ();
         double monto = in.read_double ();
         this.solicitarEncargoCompra (cliente, origen, destino, producto, monto);
         out = $rh.createReply();
         break;
       }

       case 2:  // service/ServicioRadioMovil/solicitarEncargoTransporte
       {
         String cliente = in.read_string ();
         String origen = in.read_string ();
         String destino = in.read_string ();
         String carga = in.read_string ();
         this.solicitarEncargoTransporte (cliente, origen, destino, carga);
         out = $rh.createReply();
         break;
       }

       case 3:  // service/ServicioRadioMovil/solicitarAlquilerMovil
       {
         String cliente = in.read_string ();
         String origen = in.read_string ();
         double periodo = in.read_double ();
         double horaInicio = in.read_double ();
         this.solicitarAlquilerMovil (cliente, origen, periodo, horaInicio);
         out = $rh.createReply();
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:service/ServicioRadioMovil:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public ServicioRadioMovil _this() 
  {
    return ServicioRadioMovilHelper.narrow(
    super._this_object());
  }

  public ServicioRadioMovil _this(org.omg.CORBA.ORB orb) 
  {
    return ServicioRadioMovilHelper.narrow(
    super._this_object(orb));
  }


} // class ServicioRadioMovilPOA
