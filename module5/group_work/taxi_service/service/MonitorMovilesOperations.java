package service;


/**
* service/MonitorMovilesOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from monitorMoviles.idl
* domingo 11 de diciembre de 2011 17H08' GMT-04:00
*/

public interface MonitorMovilesOperations 
{
  service.Movil encontrarMovil (String lugar);
} // interface MonitorMovilesOperations
