package service;


/**
* service/ServicioRadioMovilOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from servicioRadioMovil.idl
* domingo 11 de diciembre de 2011 12H01' GMT-04:00
*/

public interface ServicioRadioMovilOperations 
{
  void solicitarCarrera (String cliente, String origen, String destino);
  void solicitarEncargoCompra (String cliente, String origen, String destino, String producto, double monto);
  void solicitarEncargoTransporte (String cliente, String origen, String destino, String carga);
  void solicitarAlquilerMovil (String cliente, String origen, double periodo, double horaInicio);
} // interface ServicioRadioMovilOperations
