package service;


/**
* service/ServicioCreditoPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from servicioCredito.idl
* domingo 11 de diciembre de 2011 12H01' GMT-04:00
*/

public abstract class ServicioCreditoPOA extends org.omg.PortableServer.Servant
 implements service.ServicioCreditoOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("registrarPlanDePago", new java.lang.Integer (0));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // service/ServicioCredito/registrarPlanDePago
       {
         double codigoPlanPago = in.read_double ();
         this.registrarPlanDePago (codigoPlanPago);
         out = $rh.createReply();
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:service/ServicioCredito:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public ServicioCredito _this() 
  {
    return ServicioCreditoHelper.narrow(
    super._this_object());
  }

  public ServicioCredito _this(org.omg.CORBA.ORB orb) 
  {
    return ServicioCreditoHelper.narrow(
    super._this_object(orb));
  }


} // class ServicioCreditoPOA
