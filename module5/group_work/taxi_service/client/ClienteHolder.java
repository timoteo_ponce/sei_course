package client;

/**
* client/ClienteHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from cliente.idl
* domingo 11 de diciembre de 2011 12H01' GMT-04:00
*/

public final class ClienteHolder implements org.omg.CORBA.portable.Streamable
{
  public client.Cliente value = null;

  public ClienteHolder ()
  {
  }

  public ClienteHolder (client.Cliente initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = client.ClienteHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    client.ClienteHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return client.ClienteHelper.type ();
  }

}
