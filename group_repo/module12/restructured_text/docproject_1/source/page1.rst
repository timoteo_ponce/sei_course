===== 
Titulo 
===== 
Subtitulo 
-------- 
Los titulos son definidos usando 
subrayado con barras horizontales, 
siendo dobles para titulos del nivel 1
y simples para subtitulos.
	
Los parrafos se separan por una linea 
en blanco. Los caracteres mas comunes 
son "``= - ` : ' " ~ ^ _ * + # < >``"


Listas
------

Listas simples:

- Elemento 1
- Elemento 2

- Las listas se generan con "-", "*" o "+".
  El texto adicional debe ser indentado
  de forma correcta para su formato.

Con una linea en blanco terminamos 
la lista.


Listas 2 
--------

- Lista simple
- Lista simple 2

* Lista simple alternativa
* Lista simple alternativa 2

#. Lista numerada
#. Lista numerada 2

Tablas
------

+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+




Enlaces
-------
* Enlaces locales, como :ref:`page2`

* Ejemplo completo, en :ref:`statusPage`

