.. _statusPage:

**********************
Monitoreo de proyectos
**********************

En esta pagina tenemos listados los 
principales proyectos, sus encargados, 
su numero de version y sus 
respectivos estados.

Yabe Forums
###########

Aplicacion web que permite publicar foros de discusion
a traves de una interfaz sencilla.


+------------+--------------------------+
| Encargado  | Timoteo Ponce            |
+------------+--------------------------+
| Version    | 2.2                      |
+------------+--------------------------+
| Estado     | Desarrollo y soporte     |
+------------+--------------------------+


Cucupons
###########

Aplicacion web que permite publicar propagandas
cortas.


+------------+--------------------------+
| Encargado  | Carla Villena            |
+------------+--------------------------+
| Version    | 0.3                      |
+------------+--------------------------+
| Estado     | Desarrollo               |
+------------+--------------------------+


Boringa(AR)
###########

Aplicacion web que permite perder el tiempo
con algo de estilo -> AR.


+------------+--------------------------+
| Encargado  | Roger Unoja              |
+------------+--------------------------+
| Version    | 0.9                      |
+------------+--------------------------+
| Estado     | Abandonado               |
+------------+--------------------------+
| URL        | http://bb.ar.com         |
+------------+--------------------------+


.. [timoteo] Agregar una descripcion con colores a las tablas


Boringa(BO)
###########

Aplicacion web que permite perder el tiempo
con algo de estilo -> BO.


+------------+--------------------------+
| Encargado  | Roger Unoja              |
+------------+--------------------------+
| Version    | 0.9.1                    |
+------------+--------------------------+
| Estado     | Abandonado               |
+------------+--------------------------+
| URL        | http://bb.bo.com         |
+------------+--------------------------+


Boringa(UR)
###########

Aplicacion web que permite perder el tiempo
con algo de estilo -> UR.


+------------+--------------------------+
| Encargado  | Roger Unoja              |
+------------+--------------------------+
| Version    | 0.9                      |
+------------+--------------------------+
| Estado     | Activo                   |
+------------+--------------------------+
| URL        | http://bb.ur.com         |
+------------+--------------------------+




