
Welcome to docproject1's documentation!
***************************************

Contents:

* Titulo
  * Subtitulo
  * Listas
  * Listas 2
  * Tablas
  * Enlaces
* Titulo 2
* Restructured Text (reST) and Sphinx CheatSheet
  * Introduction
  * Inline markup and special characters (e.g., bold, italic,
    verbatim)
  * Headings
  * Internal and External Links
  * List and bullets
  * Comments
  * Directives
  * Tables
  * Include other RST files with the toctree directive
  * Substitutions
  * Colored boxes: note, seealso, todo and warnings
  * Inserting code and Literal blocks
  * Maths and Equations with LaTeX
  * Auto-document your python code
  * Include Images
  * Include a Figure
  * Topic directive
  * Sidebar directive
  * Other directives:: glossary, centered, index, download and field
    list
  * Footnote
  * Citations
  * More about aliases
  * Intersphinx
  * file-wide metadata
  * metainformation
  * contents directives
* Monitoreo de proyectos
  * Yabe Forums
  * Cucupons
  * Boringa(AR)
  * Boringa(BO)
  * Boringa(UR)

Indices and tables
******************

* *Index*

* *Module Index*

* *Search Page*
