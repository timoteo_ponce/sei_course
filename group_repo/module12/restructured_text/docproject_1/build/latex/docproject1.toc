\select@language {english}
\contentsline {chapter}{\numberline {1}Titulo}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Subtitulo}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Listas}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Listas 2}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Tablas}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Enlaces}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Titulo 2}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Restructured Text (reST) and Sphinx CheatSheet}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Inline markup and special characters (e.g., bold, italic, verbatim)}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}Headings}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Internal and External Links}{10}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}External links}{10}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Implicit Links to Titles}{10}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Explicit Links}{10}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}List and bullets}{11}{section.3.5}
\contentsline {section}{\numberline {3.6}Comments}{11}{section.3.6}
\contentsline {section}{\numberline {3.7}Directives}{11}{section.3.7}
\contentsline {section}{\numberline {3.8}Tables}{11}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Simple tables}{12}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Multicells tables, first method}{12}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Multicells table, second method}{12}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}The tabularcolumns directive}{13}{subsection.3.8.4}
\contentsline {subsection}{\numberline {3.8.5}The csv-table directive}{13}{subsection.3.8.5}
\contentsline {section}{\numberline {3.9}Include other RST files with the toctree directive}{13}{section.3.9}
\contentsline {section}{\numberline {3.10}Substitutions}{14}{section.3.10}
\contentsline {section}{\numberline {3.11}Colored boxes: note, seealso, todo and warnings}{14}{section.3.11}
\contentsline {section}{\numberline {3.12}Inserting code and Literal blocks}{15}{section.3.12}
\contentsline {subsection}{\numberline {3.12.1}How to include simple code}{15}{subsection.3.12.1}
\contentsline {subsection}{\numberline {3.12.2}code-block}{15}{subsection.3.12.2}
\contentsline {subsection}{\numberline {3.12.3}Include code with the literalinclude directive}{16}{subsection.3.12.3}
\contentsline {section}{\numberline {3.13}Maths and Equations with LaTeX}{16}{section.3.13}
\contentsline {section}{\numberline {3.14}Auto-document your python code}{16}{section.3.14}
\contentsline {section}{\numberline {3.15}Include Images}{17}{section.3.15}
\contentsline {section}{\numberline {3.16}Include a Figure}{17}{section.3.16}
\contentsline {section}{\numberline {3.17}Topic directive}{17}{section.3.17}
\contentsline {section}{\numberline {3.18}Sidebar directive}{18}{section.3.18}
\contentsline {section}{\numberline {3.19}Other directives:: glossary, centered, index, download and field list}{18}{section.3.19}
\contentsline {subsection}{\numberline {3.19.1}Field list}{18}{subsection.3.19.1}
\contentsline {subsection}{\numberline {3.19.2}glossary}{19}{subsection.3.19.2}
\contentsline {subsection}{\numberline {3.19.3}index}{19}{subsection.3.19.3}
\contentsline {subsection}{\numberline {3.19.4}download}{19}{subsection.3.19.4}
\contentsline {subsection}{\numberline {3.19.5}hlist directive}{19}{subsection.3.19.5}
\contentsline {section}{\numberline {3.20}Footnote}{20}{section.3.20}
\contentsline {section}{\numberline {3.21}Citations}{20}{section.3.21}
\contentsline {section}{\numberline {3.22}More about aliases}{20}{section.3.22}
\contentsline {section}{\numberline {3.23}Intersphinx}{20}{section.3.23}
\contentsline {section}{\numberline {3.24}file-wide metadata}{21}{section.3.24}
\contentsline {subsection}{\numberline {3.24.1}orphan}{21}{subsection.3.24.1}
\contentsline {section}{\numberline {3.25}metainformation}{21}{section.3.25}
\contentsline {section}{\numberline {3.26}contents directives}{21}{section.3.26}
\contentsline {paragraph}{Bibliography}{21}{paragraph*.8}
\contentsline {chapter}{\numberline {4}Monitoreo de proyectos}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Yabe Forums}{23}{section.4.1}
\contentsline {section}{\numberline {4.2}Cucupons}{23}{section.4.2}
\contentsline {section}{\numberline {4.3}Boringa(AR)}{23}{section.4.3}
\contentsline {section}{\numberline {4.4}Boringa(BO)}{23}{section.4.4}
\contentsline {section}{\numberline {4.5}Boringa(UR)}{24}{section.4.5}
\contentsline {chapter}{\numberline {5}Indices and tables}{25}{chapter.5}
\contentsline {chapter}{Bibliography}{27}{chapter*.10}
\contentsline {chapter}{Python Module Index}{29}{section*.13}
