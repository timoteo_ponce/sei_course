﻿[+] type Pedido is record
	[ ] datetime fecha
	[ ] string region 
	[ ] string representante
	[ ] string item
	[ ] int unidades
	[ ] real costoUnit
	[ ] string total
[ ] 
[+] list of Pedido readPedidosCsv()
	[ ] list of Pedido pedidos
	[ ] list of STRING lines
	[ ] int i 
	[ ] listread( lines , CurrentPath + "\..\_Examen1\Sales2009.csv" )
	[-] for ( i = 2 ; i <= listCount( lines ) ; i++) 
		[ ] string line = lines[ i ]
		[ ] Pedido pedido 
		[ ] ParseDateFormat( getfield( line , "," , 1) , "m/d/yy" , pedido.fecha ,20 )
		[ ] pedido.region = getfield( line , "," , 2)
		[ ] pedido.representante = getfield( line , "," , 3)
		[ ] pedido.item = getfield( line , "," , 4)
		[ ] pedido.unidades = val( getfield( line , "," , 5) ) 
		[ ] pedido.costoUnit = val( getfield( line , "," , 6) )
		[ ] pedido.total = getfield( line , "," , 7)
		[ ] listappend( pedidos, pedido )
		[ ] //printPedido (pedido) 
	[ ] return pedidos
	[ ] 
[+] void printPedido( Pedido pedido )
	[ ] print( "fecha={pedido.fecha},region={pedido.region},rep={pedido.representante},item={pedido.item},units={pedido.unidades},costo={pedido.costoUnit},total={pedido.total}" )
	[ ] 
	[ ] 
[ ] //1. escribir una funcion que permita listar los pedidos entre dos fechas determinadas
[+] void printBetween( datetime startDate, datetime endDate )
	[ ] print("------------------ Imprimiendo pedidos entre {startDate} -> {endDate} -------------")
	[ ] int periodDiff = DiffDateTime( endDate, startDate )
	[ ] list of Pedido pedidos = readPedidosCsv()
	[ ] Pedido pedido 
	[-] for each pedido in pedidos
		[-] if DiffDateTime( pedido.fecha , startDate) >= 0 && DiffDateTime( endDate, pedido.fecha) <= periodDiff
			[ ] printPedido( pedido )
[ ] 
[ ] //2 escribir una funcion que imprimar todas las compras por item, y el monto total de su venta
[+] void printItemStats( string item )
	[ ] print("-------------- item stats de {item} ------------- ")
	[ ] list of Pedido pedidos = readPedidosCsv()
	[ ] Pedido pedido 
	[ ] number total = 0
	[-] for each pedido in pedidos
		[-] if trim(pedido.item) == trim(item)
			[ ] printpedido( pedido )
			[ ] total += val( pedido.total ) 
	[ ] print( "Monto total de venta para {item} = {total}" )
[ ] 
[ ] //3 escribir una funcion q guarde en un archivo plano todas las ventas de un determinado representante
[+] void repSalesToFile( string rawrep )
	[ ] string rep = lower( trim( rawrep ) )
	[ ] print( "-------------Salvando ventas del representante : {rep} ----------" )
	[ ] list of Pedido pedidos = readPedidosCsv()
	[ ] Pedido pedido 
	[ ] int count = 0
	[ ] HFILE file
	[-] do	
		[ ] file = FileOpen("sales_{rep}.txt", FM_WRITE)
		[ ] // no incluyo la columna REP por que esa informacion esta en el nombre del archivo
		[ ] fileWriteLine( file , "OrderDate | Region | Item | Units | Unit Cost | Total ")
		[-] for each pedido in pedidos 
			[-] if lower( trim(pedido.representante) ) == rep 
				[ ] fileWriteLine( file,  "{pedido.fecha}, {pedido.region} , {pedido.item}, {pedido.unidades}, {pedido.costoUnit}, {pedido.total}" ) 
				[ ] count++
		[ ] fileclose( file )
		[ ] print( "{count} pedidos guardados en el archivo sales_{rep}.txt" )
	[-] except 
		[ ] raise 1, "Problems while handling files"
[ ] 
[-] main()
	[ ] //readPedidosCsv()
	[ ] //1
	[ ] date startDate = "2010-11-3"
	[ ] date endDate = "2010-12-21"
	[ ] datetime date1 = [datetime] startDate
	[ ] datetime date2 = [datetime] endDate
	[ ] printBetween( date1, date2 )
	[ ] //2
	[ ] printItemStats( "Pen" )
	[ ] printItemStats( "Desk" )
	[ ] printItemStats( "Binder" )
	[ ] //3
	[ ] repSalesToFile( "Jones" )
	[ ] repSalesToFile( "Sorvino" )
