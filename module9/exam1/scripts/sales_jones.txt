OrderDate | Region | Item | Units | Unit Cost | Total 
2009-01-06, Quebec , Pencil, 95, 1.990000,  189.05 
2009-04-01, Quebec , Binder, 60, 4.990000,  299.40 
2009-06-08, Quebec , Binder, 60, 8.990000,  539.40 
2009-08-15, Quebec , Pencil, 35, 4.990000,  174.65 
2009-09-18, Quebec , Pen Set, 16, 15.990000,  255.84 
2009-10-22, Quebec , Pen, 64, 8.990000,  575.36 
2010-02-18, Quebec , Binder, 4, 4.990000,  19.96 
2010-07-04, Quebec , Pen Set, 62, 4.990000,  309.38 
