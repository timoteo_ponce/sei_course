﻿[ ] 
[+] // *** DATA DRIVEN ASSISTANT Section (!! DO NOT REMOVE !!) ***
	[ ] use "datadrivetc.inc"
	[ ] use "testcase1.t"
	[ ] 
	[ ] // *** DSN ***
	[ ] STRING gsDSNConnect = "DSN=Silk DDA Excel;DBQ=C:\Book1.xls;UID=;PWD=;"
	[ ] 
	[ ] // *** Global record for each testcase ***
	[ ] 
	[-] type REC_DATALIST_DD_VerifyReset is record
		[ ] REC_Sheet1_ recSheet1_  //Sheet1$, 
	[ ] 
	[ ] // *** Global record for each Table ***
	[ ] 
	[-] type REC_Sheet1_ is record
		[ ] INTEGER val1  //val1, 
		[ ] REAL val2  //val2, 
		[ ] REAL val3  //val3, 
		[ ] REAL val4  //val4, 
		[ ] REAL val5  //val5, 
		[ ] REAL val6  //val6, 
	[ ] 
	[ ] // *** Global record containing sample data for each table ***
	[ ] // *** Used when running a testcase with 'Use Sample Data from Script' checked ***
	[ ] 
	[-] REC_Sheet1_ grTest_Sheet1_ = {...}
		[ ]  // val1
		[ ] NULL // val2
		[ ] NULL // val3
		[ ] NULL // val4
		[ ] NULL // val5
		[ ] NULL // val6
	[ ] 
	[ ] // *** End of DATA DRIVEN ASSISTANT Section ***
	[ ] 
[-] testcase DD_VerifyReset (REC_DATALIST_DD_VerifyReset rData)
	[ ] string v1,v2,v3,v4,v5,v6
	[ ]  v1 = Str(rData.recSheet1_.val1)
	[ ]  v2 = Str(rData.recSheet1_.val2)
	[ ]  v3 = Str(rData.recSheet1_.val3)
	[ ]  v4 = Str(rData.recSheet1_.val4)
	[ ]  v5 = Str(rData.recSheet1_.val5)
	[ ]  v6 = Str(rData.recSheet1_.val6)
	[ ] sleep(2)
	[-] if OnLineCatalog.verifiReset(v1,v2,v3,v4,v5,v6)
		[ ] print('PASS TEST')
	[-] else
		[ ] print('FAILED TEST')
