﻿[-] window BrowserChild AboutThisSite
	[ ] tag "About This Site"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading AboutThisSite
		[ ] tag "About This Site"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading ThisIsASampleOnlineCommer
		[ ] tag "This is a sample online commerce application. It is not real. Green Mountain Outpost is a fictitious company. Any resemblance *"
	[+] HtmlText ForYourPrivacyAndSecurity1
		[ ] tag "For your privacy and security,DO NOT ENTER REAL BILLING OR SHIPPING INFORMATION"
	[+] HtmlTable ForYourPrivacyAndSecurity2
		[ ] tag "For your privacy and security,DO NOT ENTER REAL BILLING OR SHIPPING INFORMATION"
		[+] HtmlColumn ThisSiteIncorporatesTheFol
			[ ] tag "This site incorporates?the following technologies:"
			[+] HtmlList TheyMayBeAddedInTheFutu
				[ ] tag "?they may be added in the future?:"
		[+] HtmlColumn ItCouldAlsoIncorporateThes
			[ ] tag "It could also incorporate?these technologies??they may be added in the future?:"
			[+] HtmlList ServerSideProcesses
				[ ] tag "Server-side processes"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
