﻿[ ] use 'frame2.inc'
[ ] use 'frame.inc'
[ ] 
[-] testcase VerifyProductDetail() appstate DetalleProducto
	[ ] window title1
	[ ] title1 = GMOProducts.N3PersonDomeTent
	[-] if verifiProductsName( '3 Person Dome Tent','External Frame Backpack','Glacier Sun Glasses','Padded Socks','Hiking Boots','Back Country Shorts')
		[ ] print('PASSED')
	[-] else
		[ ] logerror('FAILED')
		[ ] 
[-]  boolean  verifiProductsName(string v1,string v2, string v3, string v4, string v5,string v6) //appstate DefaultBaseState
		[ ] boolean bFlag = true
		[ ] window vWeb1 = GMOProducts.N3PersonDomeTent
		[ ] window vWeb2 = GMOProducts.ExternalFrameBackpack
		[ ] window vWeb3 = GMOProducts.GlacierSunGlasses
		[ ] window vWeb4 = GMOProducts.PaddedSocks
		[ ] window vWeb5 = GMOProducts.HikingBoots
		[ ] window vWeb6 = GMOProducts.BackCountryShorts
		[-] if (!vWeb1.Exists() || vWeb1.GetText()!=v1)
			[ ] bFlag = false
		[-] if (!vWeb2.Exists() || vWeb2.GetText()!=v2)
			[ ] bFlag = false
		[-] if (!vWeb3.Exists() || vWeb3.GetText()!=v3)
			[ ] bFlag = false
		[-] if (!vWeb4.Exists() || vWeb4.GetText()!=v4)
			[ ] bFlag = false
		[-] if (!vWeb5.Exists() || vWeb5.GetText()!=v5)
			[ ] bFlag = false
		[-] if (!vWeb6.Exists() || vWeb6.GetText()!=v6)
			[ ] bFlag = false
		[ ] return bFlag
[ ] 
[-] testcase main() appstate DefaultBaseState 
	[-] if verifyPageAbout()
		[ ] print('PASSED')
	[-] else
		[ ] logerror('FAILED')
	[ ] 
[ ] 
[-] boolean verifyPageAbout()  
	[ ] WelcomeToGreenMountainOutp.BeSureToVisitUsOnTheInt2.HtmlColumn2.AboutTheGMOSite.Click ()
	[ ] return AboutThisSite.Exists() 
