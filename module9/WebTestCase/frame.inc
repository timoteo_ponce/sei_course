﻿[ ] const wMainWindow = WelcomeToGreenMountainOutp
[ ] 
[+] window BrowserChild WelcomeToGreenMountainOutp
	[ ]  tag "Welcome to Green Mountain Outpost"
	[ ] parent Browser
	[ ]  // The URL of this page
	[ ]  const sLocation = "http://demo.borland.com/gmopost"
	[ ] 
	[ ]  // The login user name
	[ ]  // const sUserName = ?
	[ ] 
	[ ]  // The login password
	[ ]  // const sPassword = ?
	[ ] 
	[ ]  // The size of the browser window
	[ ]  // const POINT BrowserSize = {600, 400}
	[ ] 
	[ ]  // Sets the browser font settings to the default
	[ ]  // const bDefaultFont = TRUE
	[ ] 
	[ ] 
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading GMOOnLine
		[ ] tag "GMO OnLine"
	[+] HtmlText WelcomeToGreenMountainOutp
		[ ] tag "Welcome to Green Mountain Outpost's OnLine Catalog!"
	[+] HtmlText WeAreMovingOur329Page199
		[ ] tag "We are moving our 329 page 1997 catalog to the internet to serve you better. Now you can browse our online database, see produ*"
	[+] HtmlText BeSureToVisitUsOnTheInt1
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*"
	[+] HtmlTable BeSureToVisitUsOnTheInt2
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*[1]"
		[+] HtmlColumn HtmlColumn1
			[ ] tag "#1"
			[+] HtmlPushButton EnterGMOOnLine
				[+] multitag "Enter GMO OnLine"
					[ ] "$bSubmit"
					[ ] "&name='bSubmit'"
		[+] HtmlColumn HtmlColumn2
			[ ] tag "#2"
			[+] HtmlPushButton AboutTheGMOSite
				[+] multitag "About The GMO Site"
					[ ] "$bAbout"
					[ ] "&name='bAbout'"
	[+] HtmlTable BeSureToVisitUsOnTheInt3
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*[2]"
		[+] HtmlColumn HtmlColumn1
			[ ] tag "#1"
			[+] HtmlPushButton BrowserTestPage
				[+] multitag "Browser Test Page"
					[ ] "$bBrowserTest"
					[ ] "&name='bBrowserTest'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild OnLineCatalog
	[ ] tag "OnLine Catalog"
	[ ] parent Browser
	[+] HtmlTable TheseAreTheItemsCurrently
		[ ] tag "These are the items currently available through our online catalog. Select the quantity of each item and then press the ?Plac*"
		[-] HtmlColumn OrderQuantity
			[ ] tag "Order?Quantity"
			[+] HtmlTextField N29999
				[-] multitag "&name='QTY_TENTS'"
					[ ] "? 299.99"
					[ ] "$QTY_TENTS"
			[+] HtmlTextField N17995
				[-] multitag "&name='QTY_BACKPACKS'"
					[ ] "? 179.95"
					[ ] "$QTY_BACKPACKS"
			[+] HtmlTextField N6799
				[-] multitag "&name='QTY_GLASSES'"
					[ ] "? 67.99"
					[ ] "$QTY_GLASSES"
			[+] HtmlTextField N1999
				[-] multitag "&name='QTY_SOCKS'"
					[ ] "? 19.99"
					[ ] "$QTY_SOCKS"
			[-] HtmlTextField N10990
				[-] multitag "&name='QTY_BOOTS'"
					[ ] "? 109.90"
					[ ] "$QTY_BOOTS"
			[-] HtmlTextField N2495
				[-] multitag "&name='QTY_SHORTS'"
					[ ] "? 24.95"
					[ ] "$QTY_SHORTS"
		[+] HtmlColumn ItemName
			[ ] tag "Item Name"
	[-] HtmlPushButton ResetForm
		[+] multitag "&name='bReset'"
			[ ] "Reset Form"
			[ ] "$bReset"
	[-]  boolean  verifiReset(string v1,string v2, string v3, string v4, string v5,string v6) //appstate DefaultBaseState
		[ ] boolean bFlag = true
		[ ] //Taskbar.SetActive ()
		[ ] WelcomeToGreenMountainOutp.SetActive( )
		[ ] WelcomeToGreenMountainOutp.BeSureToVisitUsOnTheInt2.HtmlColumn1.EnterGMOOnLine.Click ()
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N29999.SetText(v1)
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N17995.SetText(v2)
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N6799.SetText(v3)
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N1999.SetText(v4)
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N10990.SetText(v5)
		[ ] TheseAreTheItemsCurrently.OrderQuantity.N2495.SetText(v6)
		[ ] sleep(3)
		[ ] ResetForm.Click ()
		[ ] 
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N29999.GetText() != '0')
			[ ] bFlag = false
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N17995.GetText() != '0')
			[ ] bFlag = false
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N6799.GetText() != '0')
			[ ] bFlag = false
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N1999.GetText() != '0')
			[ ] bFlag = false
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N10990.GetText() != '0')
			[ ] bFlag = false
		[-] if (OnLineCatalog.TheseAreTheItemsCurrently.OrderQuantity.N2495.GetText() != '0')
			[ ] bFlag = false
			[ ] sleep(3)
		[ ] return bFlag
[ ] 
[+] window BrowserChild GMOProducts
	[ ] tag "GMO Products"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading Products
		[ ] tag "Products"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading N3PersonDomeTent
		[ ] tag "3 Person Dome Tent"
	[+] HtmlText OurBest3PersonBackpackDom1
		[ ] tag "Our best 3 person backpack dome tent"
	[+] HtmlImage OurBest3PersonBackpackDom2
		[+] multitag "Our best 3 person backpack dome tent"
			[ ] "$http:??demo.borland.com?gmopost?images?tent.gif"
	[+] HtmlText UnitPrice1
		[ ] tag "Unit Price:[1]"
	[+] HtmlText N29999
		[ ] tag "? 299.99"
	[+] HtmlText InStock1
		[ ] tag "? In Stock:[1]"
	[+] HtmlText N23
		[ ] tag "23"
	[+] HtmlText ItemNumber1
		[ ] tag "Item Number:[1]"
	[+] HtmlText N1000
		[ ] tag "1000"
	[+] HtmlText HereSASuperbThreeSeasonM
		[ ] tag "Here's a superb three-season mountaineering?backpacking tent at a great price! This dome sleeps 3 and weighs in at only 4.5 lb*"
	[+] HtmlHeading ExternalFrameBackpack
		[ ] tag "External Frame Backpack"
	[+] HtmlText OurMostPopularExternalFram1
		[ ] tag "Our most popular external frame backpack"
	[+] HtmlImage OurMostPopularExternalFram2
		[+] multitag "Our most popular external frame backpack"
			[ ] "$http:??demo.borland.com?gmopost?images?backpack.gif"
	[+] HtmlText UnitPrice2
		[ ] tag "Unit Price:[2]"
	[+] HtmlText N17995
		[ ] tag "? 179.95"
	[+] HtmlText InStock2
		[ ] tag "? In Stock:[2]"
	[+] HtmlText N8
		[ ] tag "8"
	[+] HtmlText ItemNumber2
		[ ] tag "Item Number:[2]"
	[+] HtmlText N1001
		[ ] tag "1001"
	[+] HtmlText AnIdealPackForLongTrailT
		[ ] tag "An ideal pack for long trail trips, its strong heli-arc welded 6061-T6 aircraft-quality aluminum frame with V-truss design res*"
	[+] HtmlHeading GlacierSunGlasses
		[ ] tag "Glacier Sun Glasses"
	[+] HtmlText OurBestGlacierSunGlassesW1
		[ ] tag "Our best glacier sun glasses will protect you on your next expedition or when you hit the slopes."
	[+] HtmlImage OurBestGlacierSunGlassesW2
		[+] multitag "Our best glacier sun glasses will protect you on your next expedition or when you hit the slopes."
			[ ] "$http:??demo.borland.com?gmopost?images?glasses.gif"
	[+] HtmlText UnitPrice3
		[ ] tag "Unit Price:[3]"
	[+] HtmlText N6799
		[ ] tag "? 67.99"
	[+] HtmlText InStock3
		[ ] tag "? In Stock:[3]"
	[+] HtmlText N138
		[ ] tag "138"
	[+] HtmlText ItemNumber3
		[ ] tag "Item Number:[3]"
	[+] HtmlText N1002
		[ ] tag "1002"
	[+] HtmlText UseTheseFullCoverageFull
		[ ] tag "Use these full-coverage, full-protection sun glasses for all your outdoor activities. Polycarbonate lenses provide lightweight*"
	[+] HtmlHeading PaddedSocks
		[ ] tag "Padded Socks"
	[+] HtmlText OurFavoritePaddedSocksWill1
		[ ] tag "Our favorite padded socks will make your next back country trek easier on your feet."
	[+] HtmlImage OurFavoritePaddedSocksWill2
		[+] multitag "Our favorite padded socks will make your next back country trek easier on your feet."
			[ ] "$http:??demo.borland.com?gmopost?images?socks.gif"
	[+] HtmlText UnitPrice4
		[ ] tag "Unit Price:[4]"
	[+] HtmlText N1999
		[ ] tag "? 19.99"
	[+] HtmlText InStock4
		[ ] tag "? In Stock:[4]"
	[+] HtmlText N47
		[ ] tag "47"
	[+] HtmlText ItemNumber4
		[ ] tag "Item Number:[4]"
	[+] HtmlText N1003
		[ ] tag "1003"
	[+] HtmlText TheseHeavyweightSocksAreDe
		[ ] tag "These heavyweight socks are designed for long distances. Reinforced heels and flat-seam toes prevent blisters. Made of DuraTe*"
	[+] HtmlHeading HikingBoots
		[ ] tag "Hiking Boots"
	[+] HtmlText ClimbAnyMountainWithOurPe1
		[ ] tag "Climb any mountain with our PermaDry all leather hiking boots."
	[+] HtmlImage ClimbAnyMountainWithOurPe2
		[+] multitag "Climb any mountain with our PermaDry all leather hiking boots."
			[ ] "$http:??demo.borland.com?gmopost?images?boots.gif"
	[+] HtmlText UnitPrice5
		[ ] tag "Unit Price:[5]"
	[+] HtmlText N10990
		[ ] tag "? 109.90"
	[+] HtmlText InStock5
		[ ] tag "? In Stock:[5]"
	[+] HtmlText N32
		[ ] tag "32"
	[+] HtmlText ItemNumber5
		[ ] tag "Item Number:[5]"
	[+] HtmlText N1004
		[ ] tag "1004"
	[+] HtmlText ASolidChoiceForThroughHik
		[ ] tag "A solid choice for through-hikes and other demanding backpacking with heavy loads, our boots are surprisingly light--just 2 lb*"
	[+] HtmlHeading BackCountryShorts
		[ ] tag "Back Country Shorts"
	[+] HtmlText OurQuickDryingShortsAreA1
		[ ] tag "Our quick drying shorts are a customer favorite. Perfect for a day of any outdoor activity or just lounging around the camp."
	[+] HtmlImage OurQuickDryingShortsAreA2
		[+] multitag "Our quick drying shorts are a customer favorite. Perfect for a day of any outdoor activity or just lounging around the camp."
			[ ] "$http:??demo.borland.com?gmopost?images?shorts.gif"
	[+] HtmlText UnitPrice6
		[ ] tag "Unit Price:[6]"
	[+] HtmlText N2495
		[ ] tag "? 24.95"
	[+] HtmlText InStock6
		[ ] tag "? In Stock:[6]"
	[+] HtmlText N59
		[ ] tag "59"
	[+] HtmlText ItemNumber6
		[ ] tag "Item Number:[6]"
	[+] HtmlText N1005
		[ ] tag "1005"
	[+] HtmlText LightweightNylonShortsCanT
		[ ] tag "Lightweight nylon shorts can take on any activity and double as swimwear. Supplex nylon fabric is strong and dries quickly. Ny*"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild BrowserTestPage
	[ ] tag "Browser Test Page"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading AllBrowsersAreNotCreatedE1
		[ ] tag "All Browsers Are Not Created Equal"
	[+] HtmlImage Netscape
		[+] multitag "Netscape"
			[ ] "$http:??demo.borland.com?gmopost?images?nscp-ani.gif"
	[+] HtmlImage InternetExplorer
		[+] multitag "Internet Explorer"
			[ ] "$http:??demo.borland.com?gmopost?images?ie-anim.gif"
	[+] HtmlLink AllBrowsersAreNotCreatedE2
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$http:??demo.borland.com?gmopost?javaapplet.htm"
	[+] HtmlImage AllBrowsersAreNotCreatedE3
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$http:??demo.borland.com?gmopost?images?java-animate2.gif"
	[+] HtmlLink HttpDemoBorlandComGmopos1
		[ ] tag "$http:??demo.borland.com?gmopost?activex.htm"
	[+] HtmlImage HttpDemoBorlandComGmopos2
		[ ] tag "$http:??demo.borland.com?gmopost?images?ax-animate.gif"
	[+] HtmlTable HtmlTable1
		[ ] tag "#1"
		[+] HtmlColumn InternetExplorer30
			[ ] tag "Internet Explorer 3.0"
			[+] HtmlList BlinkingText
				[ ] tag "Blinking Text"
			[+] HtmlList DotBullets
				[ ] tag "dot bullets"
			[+] HtmlList CircleBullets
				[ ] tag "circle bullets"
		[+] HtmlColumn NetscapeNavigator30
			[ ] tag "Netscape Navigator 3.0"
			[+] HtmlText ColoredHorizontalRules
				[ ] tag "Colored Horizontal Rules"
			[+] HtmlText DefinitionTextStyleShouldB
				[ ] tag "Definition Text Style(should be an italic font)"
			[+] HtmlText TrademarksAreImportant
				[ ] tag "Trademarks are important™"
			[+] HtmlText TableProperties
				[ ] tag "Table Properties"
	[+] HtmlPushButton AllBrowsersAreNotCreatedE4
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$I3"
			[ ] "&name='I3'"
	[+] HtmlMarquee SometimesEvenLeftAndRight
		[ ] tag "Sometimes Even Left and Right Doesn't Mean Anything"
	[+] HtmlPushButton LeftOrRight
		[+] multitag "<< Left or Right >>"
			[ ] "$B2"
			[ ] "&name='B2'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] // window BrowserChild PlaceOrder
	[ ] // tag "Place Order"
	[ ] // parent Browser
	[+] // HtmlHeading PlaceOrder
		[ ] // tag "Place Order"
	[+] // HtmlImage Logo
		[+] // multitag "Logo"
			[ ] // "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] // HtmlText YouHaveSelectedTheFollowin1
		[ ] // tag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
	[-] // HtmlTable YouHaveSelectedTheFollowin2
		[ ] // tag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
		[+] // HtmlColumn Qty
			[ ] // tag "Qty"
			[+] // HtmlText N11
				[ ] // tag "1[1]"
			[+] // HtmlText N12
				[ ] // tag "1[2]"
			[+] // HtmlText N13
				[ ] // tag "1[3]"
			[+] // HtmlText N14
				[ ] // tag "1[4]"
			[+] // HtmlText N15
				[ ] // tag "1[5]"
			[+] // HtmlText N16
				[ ] // tag "1[6]"
		[+] // HtmlColumn ProductDescription
			[ ] // tag "Product Description"
			[+] // HtmlLink N3PersonDomeTent
				[+] // multitag "3 Person Dome Tent"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#tents"
			[+] // HtmlLink ExternalFrameBackpack
				[+] // multitag "External Frame Backpack"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#backpacks"
			[+] // HtmlLink GlacierSunGlasses
				[+] // multitag "Glacier Sun Glasses"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#glasses"
			[+] // HtmlLink PaddedSocks
				[+] // multitag "Padded Socks"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#socks"
			[+] // HtmlLink HikingBoots
				[+] // multitag "Hiking Boots"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#boots"
			[+] // HtmlLink BackCountryShorts
				[+] // multitag "Back Country Shorts"
					[ ] // "$http:??demo.borland.com?gmopost?products.htm#shorts"
		[+] // HtmlColumn DeliveryStatus
			[ ] // tag "Delivery Status"
			[+] // HtmlText ToBeShipped1
				[ ] // tag "To Be Shipped[1]"
			[+] // HtmlText ToBeShipped2
				[ ] // tag "To Be Shipped[2]"
			[+] // HtmlText ToBeShipped3
				[ ] // tag "To Be Shipped[3]"
			[+] // HtmlText ToBeShipped4
				[ ] // tag "To Be Shipped[4]"
			[+] // HtmlText ToBeShipped5
				[ ] // tag "To Be Shipped[5]"
			[+] // HtmlText ToBeShipped6
				[ ] // tag "To Be Shipped[6]"
			[+] // HtmlText ProductTotal
				[ ] // tag "Product Total"
			[+] // HtmlText SalesTax
				[ ] // tag "Sales Tax"
			[+] // HtmlText ShippingHandling
				[ ] // tag "Shipping  Handling"
			[+] // HtmlText GrandTotal
				[ ] // tag "Grand Total"
		[+] // HtmlColumn UnitPrice
			[ ] // tag "Unit Price"
			[+] // HtmlText N29999
				[ ] // tag "? 299.99"
			[+] // HtmlText N17995
				[ ] // tag "? 179.95"
			[+] // HtmlText N6799
				[ ] // tag "? 67.99"
			[+] // HtmlText N1999
				[ ] // tag "? 19.99"
			[+] // HtmlText N10990
				[ ] // tag "? 109.90"
			[+] // HtmlText N2495
				[ ] // tag "? 24.95"
		[-] // HtmlColumn TotalPrice
			[ ] // tag "Total Price"
			[-] // HtmlText N29999
				[ ] // tag "? 299.99"
			[+] // HtmlText N17995
				[ ] // tag "? 179.95"
			[+] // HtmlText N6799
				[ ] // tag "? 67.99"
			[+] // HtmlText N1999
				[ ] // tag "? 19.99"
			[+] // HtmlText N10990
				[ ] // tag "? 109.90"
			[+] // HtmlText N2495
				[ ] // tag "? 24.95"
			[+] // HtmlText N70277
				[ ] // tag "? 702.77"
			[+] // HtmlText N3514
				[ ] // tag "? 35.14"
			[+] // HtmlText N500
				[ ] // tag "? 5.00"
			[+] // HtmlText N74291
				[ ] // tag "? 742.91"
	[+] // HtmlPushButton ProceedWithOrder
		[+] // multitag "Proceed With Order"
			[ ] // "$bSubmit"
			[ ] // "&name='bSubmit'"
	[+] // HtmlHidden QTY_TENTS
		[+] // multitag "$QTY_TENTS"
			[ ] // "&name='QTY_TENTS'"
	[+] // HtmlHidden QTY_BACKPACKS
		[+] // multitag "$QTY_BACKPACKS"
			[ ] // "&name='QTY_BACKPACKS'"
	[+] // HtmlHidden QTY_GLASSES
		[+] // multitag "$QTY_GLASSES"
			[ ] // "&name='QTY_GLASSES'"
	[+] // HtmlHidden QTY_SOCKS
		[+] // multitag "$QTY_SOCKS"
			[ ] // "&name='QTY_SOCKS'"
	[+] // HtmlHidden QTY_BOOTS
		[+] // multitag "$QTY_BOOTS"
			[ ] // "&name='QTY_BOOTS'"
	[+] // HtmlHidden QTY_SHORTS
		[+] // multitag "$QTY_SHORTS"
			[ ] // "&name='QTY_SHORTS'"
	[+] // HtmlText ThisIsASampleWebSiteFor
		[ ] // tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] // HtmlText Copyright1997SegueSoftwar1
		[ ] // tag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Oth*"
	[+] // HtmlText InformationInThisDocumentI
		[ ] // tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] // HtmlText Copyright1997SegueSoftwar2
		[ ] // tag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved."
	[+] // HtmlLink PleaseLetUsKnowHowYouLik
		[+] // multitag "Please let us know how you like our site."
			[ ] // "$mailto:gmo-master@segue.com"
[ ] 
[-] window BrowserChild PlaceOrder
	[ ] tag "Place Order"
	[ ] parent Browser
	[+] HtmlHeading PlaceOrder
		[+] multitag "Place Order"
			[ ] "#1"
			[ ] "@(207,52)"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "^Place Order"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
			[ ] "@(865,62)"
	[+] HtmlText YouHaveSelectedTheFollowin1
		[+] multitag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
			[ ] "^Place Order"
			[ ] "#1"
			[ ] "@(501,153)"
	[-] HtmlTable YouHaveSelectedTheFollowin2
		[+] multitag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
			[ ] "^You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button to"
			[ ] "#1"
			[ ] "@(476,351)"
		[+] HtmlColumn Qty
			[+] multitag "Qty"
				[ ] "#1"
				[ ] "@(49,202)"
			[+] HtmlText N11
				[+] multitag "1[1]"
					[ ] "#1"
					[ ] "@(49,230)"
			[+] HtmlText N12
				[+] multitag "1[2]"
					[ ] "^1[1]"
					[ ] "#2"
					[ ] "@(49,260)"
			[+] HtmlText N13
				[+] multitag "1[3]"
					[ ] "^1[2]"
					[ ] "#3"
					[ ] "@(49,290)"
			[+] HtmlText N14
				[+] multitag "1[4]"
					[ ] "^1[3]"
					[ ] "#4"
					[ ] "@(49,320)"
			[+] HtmlText N15
				[+] multitag "1[5]"
					[ ] "^1[4]"
					[ ] "#5"
					[ ] "@(49,350)"
			[+] HtmlText N16
				[+] multitag "1[6]"
					[ ] "^1[5]"
					[ ] "#6"
					[ ] "@(49,380)"
		[+] HtmlColumn ProductDescription
			[+] multitag "Product Description"
				[ ] "#2"
				[ ] "@(259,202)"
			[+] HtmlLink N3PersonDomeTent
				[+] multitag "3 Person Dome Tent"
					[ ] "#1"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#tents"
					[ ] "@(153,230)"
			[+] HtmlLink ExternalFrameBackpack
				[+] multitag "External Frame Backpack"
					[ ] "^3 Person Dome Tent"
					[ ] "#2"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#backpacks"
					[ ] "@(172,260)"
			[+] HtmlLink GlacierSunGlasses
				[+] multitag "Glacier Sun Glasses"
					[ ] "^External Frame Backpack"
					[ ] "#3"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#glasses"
					[ ] "@(152,290)"
			[+] HtmlLink PaddedSocks
				[+] multitag "Padded Socks"
					[ ] "^Glacier Sun Glasses"
					[ ] "#4"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#socks"
					[ ] "@(131,320)"
			[+] HtmlLink HikingBoots
				[+] multitag "Hiking Boots"
					[ ] "^Padded Socks"
					[ ] "#5"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#boots"
					[ ] "@(129,350)"
			[+] HtmlLink BackCountryShorts
				[+] multitag "Back Country Shorts"
					[ ] "^Hiking Boots"
					[ ] "#6"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#shorts"
					[ ] "@(155,380)"
		[+] HtmlColumn DeliveryStatus
			[+] multitag "Delivery Status"
				[ ] "#3"
				[ ] "@(540,202)"
			[+] HtmlText ToBeShipped1
				[+] multitag "To Be Shipped[1]"
					[ ] "#1"
					[ ] "@(540,230)"
			[+] HtmlText ToBeShipped2
				[+] multitag "To Be Shipped[2]"
					[ ] "^To Be Shipped[1]"
					[ ] "#2"
					[ ] "@(540,260)"
			[+] HtmlText ToBeShipped3
				[+] multitag "To Be Shipped[3]"
					[ ] "^To Be Shipped[2]"
					[ ] "#3"
					[ ] "@(540,290)"
			[+] HtmlText ToBeShipped4
				[+] multitag "To Be Shipped[4]"
					[ ] "^To Be Shipped[3]"
					[ ] "#4"
					[ ] "@(540,320)"
			[+] HtmlText ToBeShipped5
				[+] multitag "To Be Shipped[5]"
					[ ] "^To Be Shipped[4]"
					[ ] "#5"
					[ ] "@(540,350)"
			[+] HtmlText ToBeShipped6
				[+] multitag "To Be Shipped[6]"
					[ ] "^To Be Shipped[5]"
					[ ] "#6"
					[ ] "@(540,380)"
			[+] HtmlText ProductTotal
				[+] multitag "Product Total"
					[ ] "^To Be Shipped[6]"
					[ ] "#7"
					[ ] "@(610,410)"
			[+] HtmlText SalesTax
				[+] multitag "Sales Tax"
					[ ] "^Product Total"
					[ ] "#8"
					[ ] "@(610,440)"
			[+] HtmlText ShippingHandling
				[+] multitag "Shipping  Handling"
					[ ] "^Sales Tax"
					[ ] "#9"
					[ ] "@(610,470)"
			[+] HtmlText GrandTotal
				[+] multitag "Grand Total"
					[ ] "^Shipping  Handling"
					[ ] "#10"
					[ ] "@(610,500)"
		[+] HtmlColumn UnitPrice
			[+] multitag "Unit Price"
				[ ] "#4"
				[ ] "@(712,202)"
			[+] HtmlText N29999
				[+] multitag "? 299.99"
					[ ] "#1"
					[ ] "@(712,230)"
			[+] HtmlText N17995
				[+] multitag "? 179.95"
					[ ] "^$ 299.99"
					[ ] "#2"
					[ ] "@(712,260)"
			[+] HtmlText N6799
				[+] multitag "? 67.99"
					[ ] "^$ 179.95"
					[ ] "#3"
					[ ] "@(712,290)"
			[+] HtmlText N1999
				[+] multitag "? 19.99"
					[ ] "^$ 67.99"
					[ ] "#4"
					[ ] "@(712,320)"
			[+] HtmlText N10990
				[+] multitag "? 109.90"
					[ ] "^$ 19.99"
					[ ] "#5"
					[ ] "@(712,350)"
			[+] HtmlText N2495
				[+] multitag "? 24.95"
					[ ] "^$ 109.90"
					[ ] "#6"
					[ ] "@(712,380)"
		[-] HtmlColumn TotalPrice
			[+] multitag "Total Price"
				[ ] "#5"
				[ ] "@(859,202)"
			[-] HtmlText N29999
				[-] multitag "#1"
					[ ] "@(859,230)"
			[-] HtmlText N17995
				[-] multitag "#2"
					[ ] "@(859,260)"
			[-] HtmlText N6799
				[-] multitag "#3"
					[ ] "@(859,290)"
			[-] HtmlText N1999
				[-] multitag "#4"
					[ ] "@(859,320)"
			[-] HtmlText N10990
				[-] multitag "#5"
					[ ] "@(859,350)"
			[-] HtmlText N2495
				[-] multitag "#6"
					[ ] "@(859,380)"
			[-] HtmlText N70277
				[-] multitag "#7"
					[ ] "@(859,410)"
			[-] HtmlText N3514
				[-] multitag "#8"
					[ ] "@(859,440)"
			[-] HtmlText N500
				[-] multitag "#9"
					[ ] "@(859,470)"
			[-] HtmlText N74291
				[-] multitag "#10"
					[ ] "@(859,500)"
	[+] HtmlPushButton ProceedWithOrder
		[+] multitag "Proceed With Order"
			[ ] "^You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button to"
			[ ] "#1"
			[ ] "$bSubmit"
			[ ] "@(848,563)"
			[ ] "&name='bSubmit'"
	[+] HtmlHidden QTY_TENTS
		[+] multitag "#6"
			[ ] "$QTY_TENTS"
			[ ] "@(2,2)"
			[ ] "&name='QTY_TENTS'"
	[+] HtmlHidden QTY_BACKPACKS
		[+] multitag "#2"
			[ ] "$QTY_BACKPACKS"
			[ ] "@(2,2)"
			[ ] "&name='QTY_BACKPACKS'"
	[+] HtmlHidden QTY_GLASSES
		[+] multitag "#1"
			[ ] "$QTY_GLASSES"
			[ ] "@(2,2)"
			[ ] "&name='QTY_GLASSES'"
	[+] HtmlHidden QTY_SOCKS
		[+] multitag "#3"
			[ ] "$QTY_SOCKS"
			[ ] "@(2,2)"
			[ ] "&name='QTY_SOCKS'"
	[+] HtmlHidden QTY_BOOTS
		[+] multitag "#5"
			[ ] "$QTY_BOOTS"
			[ ] "@(2,2)"
			[ ] "&name='QTY_BOOTS'"
	[+] HtmlHidden QTY_SHORTS
		[+] multitag "#4"
			[ ] "$QTY_SHORTS"
			[ ] "@(2,2)"
			[ ] "&name='QTY_SHORTS'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "^You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button to"
			[ ] "#2"
			[ ] "@(140,650)"
	[+] HtmlText Copyright1997SegueSoftwar1
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Oth*"
			[ ] "^This is a sample web site for demonstration purposes only!No products will be sent to you.[2]"
			[ ] "#4"
			[ ] "@(501,715)"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "^This is a sample web site for demonstration purposes only!No products will be sent to you.[3]"
			[ ] "#5"
			[ ] "@(162,726)"
	[+] HtmlText Copyright1997SegueSoftwar2
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved."
			[ ] "^This is a sample web site for demonstration purposes only!No products will be sent to you.[1]"
			[ ] "#3"
			[ ] "@(137,691)"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "^Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Othe"
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
			[ ] "@(122,759)"
[ ] 
[-] appstate DetalleProducto () basedon DefaultBaseState
	[ ] WelcomeToGreenMountainOutp.SetActive( )
	[ ] WelcomeToGreenMountainOutp.BeSureToVisitUsOnTheInt2.HtmlColumn1.EnterGMOOnLine.Click ()
	[ ] OnLineCatalog.TheseAreTheItemsCurrently.ItemName.HtmlLink("3 Person Dome Tent|$http:??demo.borland.com?gmopost?products.htm#tents").Click ()
[ ] 
[-] appstate BrowserTestPage () basedon DefaultBaseState
	[ ] WelcomeToGreenMountainOutp.SetActive( )
	[ ] WelcomeToGreenMountainOutp.BeSureToVisitUsOnTheInt3.HtmlColumn1.BrowserTestPage.Click ()
	[ ] 
[ ] 
