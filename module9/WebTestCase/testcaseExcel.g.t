﻿[ ] 
[+] // *** DATA DRIVEN ASSISTANT Section (!! DO NOT REMOVE !!) ***
	[ ] use "datadrivetc.inc"
	[ ] use "testcase1.t"
	[ ] 
	[ ] // *** DSN ***
	[ ] STRING gsDSNConnect = "DSN=Silk DDA Excel;DBQ=C:\Book1.xls;UID=;PWD=;"
	[ ] 
	[ ] // *** Global record for each testcase ***
	[ ] 
	[-] type REC_DATALIST_DD_VerifyResetForm2 is record
		[ ] REC_Sheet1_ recSheet1_  //Sheet1$, 
	[ ] 
	[ ] // *** Global record for each Table ***
	[ ] 
	[-] type REC_Sheet1_ is record
		[ ] REAL val1  //val1, 
		[ ] REAL val2  //val2, 
		[ ] REAL val3  //val3, 
		[ ] REAL val4  //val4, 
		[ ] REAL val5  //val5, 
		[ ] REAL val6  //val6, 
	[ ] 
	[ ] // *** Global record containing sample data for each table ***
	[ ] // *** Used when running a testcase with 'Use Sample Data from Script' checked ***
	[ ] 
	[-] REC_Sheet1_ grTest_Sheet1_ = {...}
		[ ]  // val1
		[ ] NULL // val2
		[ ] NULL // val3
		[ ] NULL // val4
		[ ] NULL // val5
		[ ] NULL // val6
	[ ] 
	[ ] // *** End of DATA DRIVEN ASSISTANT Section ***
	[ ] 
[-] testcase DD_VerifyResetForm2 (REC_DATALIST_DD_VerifyResetForm2 rData)
	[ ] string v1 = '1'
	[ ] string v2 = '1'
	[ ] string v3 = '1'
	[ ] string v4 = '1'
	[ ] string v5 = '1'
	[ ] string v6 = '1'
	[ ] 
	[-] if verifiReset(v1,v2,v3,v4,v5,v6)
		[ ] print('PASSED TEST')
	[-] else
		[ ] print(Str(rData.recSheet1_.val1, NULL, 2))
