﻿[ ] //c. ATC para FileOpen
[-] testcase openAsSaved()
	[ ] string filename = CurrentPath + "\toread_file{RandInt(2,100)}.txt "
	[+] do
		[ ] HFILE file = fileOpen( filename ,FM_WRITE)
		[ ] string content = "This is a simple test content for a file"
		[ ] fileWriteLine( file , content )
		[ ] fileClose( file )
		[ ] 
		[ ] Notepad.SetActive()
		[ ] Notepad.File.Open.Pick()
		[ ] Open.SetActive()
		[ ] Open.FileName.SetText( filename )
		[ ] Open.Open.Click()
		[ ] Notepad.SetActive()
		[ ] verify( Notepad.TextField1.GetText() , content , "The file could not be properly read ")
	[-] except 
		[ ] logerror( "something bad happened while handling files" )
[ ] 
[ ] // e. ATC para Replace
[-] testcase replaceAll() appstate writtenText
	[ ] string content = Notepad.testContent + ", Timoteo Ponce really wrote it"
	[ ] Notepad.SetActive()
	[ ] Notepad.TextField1.SetText( content )
	[ ] Notepad.Edit.Replace.Pick()
	[ ] Replace.SetActive()
	[ ] Replace.FindWhat.SetText( "Timoteo" )
	[ ] Replace.ReplaceWith.SetText( "Hugo" )
	[ ] Replace.ReplaceAll.Click()
	[ ] Replace.Close()
	[ ] string expected = strtran( content , "Timoteo", "Hugo")
	[ ] verify( Notepad.TextField1.GetText(), expected )
