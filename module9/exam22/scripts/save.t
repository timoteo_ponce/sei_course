﻿[ ] // b. implementar un ATC para SaveAs
[-] testcase saveAs() appstate writtenText
	[ ] Notepad.File.SaveAs.Pick()
	[ ] SaveAs.SetActive()
	[ ] string filename = CurrentPath + "\saved_file1.txt "
	[ ] SaveAs.FileName.SetText( filename )
	[ ] SaveAs.Save.Click()
	[+] if SaveAs2.exists()
		[ ] SaveAs2.Yes.Click()
	[ ] // para evitar la fatiga...
	[ ] sleep( 2 )
	[ ] //
	[+] do
		[ ] HFILE file 
		[ ] file = FileOpen( filename , FM_READ)
		[ ] string line
		[ ] fileReadLine( file , line )
		[ ] fileclose( file ) 
		[ ] print(line)
		[-] if line != Notepad.testContent
			[ ] raise 1, "The application is not storing the file properly"
	[-] except 
		[ ] logerror( "Something went wrong while opening a file" )
[ ] 
[ ] // d. ATC para la ventana About
[-] testcase aboutWindow()
	[ ] Notepad.SetActive()
	[ ] Notepad.Help.AboutNotepad.Pick()
	[+] if AboutNotepad.exists() == false
		[ ] logerror( "About window was not properly opened" ) 
	[ ] AboutNotepad.Close()
