﻿[+] type Customer is record
	[ ] string id
	[ ] string firstname
	[ ] string lastname
	[ ] string address1
	[ ] string address2
	[ ] string city
	[ ] string state
	[ ] string zip
	[ ] string phone
[ ] 
[+] Customer newCustomer()
	[ ] Customer customer 
	[ ] customer.address1 = "address1"
	[ ] customer.address2 = "address2"
	[ ] customer.firstname = "firstname"
	[ ] customer.lastname = "lastname"
	[ ] customer.city = "Yacuiba" 
	[ ] customer.phone = "716875870"
	[ ] customer.state = "Tarija"
	[ ] customer.zip = "33126" 
	[ ] return customer
[ ] 
[ ] const wMainWindow = GMO
[ ] 
[+] window MainWin GMO
	[+]  multitag "Green*"
		[ ]  "$C:\Program Files\Segue\Gmo\green.exe"
	[ ] 
	[ ] // The working directory of the application when it is invoked
	[ ]  const sDir = "C:\Program Files\Segue\Gmo"
	[ ] 
	[ ] // The command line used to invoke the application
	[ ]  const sCmdLine = "C:\Program Files\Segue\Gmo\green.exe "
	[ ] 
	[ ] const Customer NEW_CUSTOMER = newCustomer()
	[ ] // The first window to appear when the application is invoked
	[ ] // const wStartup = ?
	[ ] 
	[ ] // The list of windows the recovery system is to leave open
	[ ] // const lwLeaveOpen = {?}
	[+] Menu File
		[+] multitag "File"
			[ ] "#1"
		[+] MenuItem Exit
			[+] multitag "Exit"
				[ ] "#1"
				[ ] "$57665"
	[+] Menu Order
		[+] multitag "Order"
			[ ] "#2"
		[+] MenuItem CustomerInformation
			[+] multitag "Customer Information"
				[ ] "#1"
				[ ] "$32772"
		[+] MenuItem OrderStatus
			[+] multitag "Order Status"
				[ ] "#2"
				[ ] "$32773"
	[+] Menu Administrator
		[+] multitag "Administrator"
			[ ] "#3"
		[+] MenuItem DBAdmin
			[+] multitag "DB Admin"
				[ ] "#1"
				[ ] "$32774"
	[+] Menu Help
		[+] multitag "Help"
			[ ] "#4"
		[+] MenuItem AboutGreen
			[+] multitag "About Green"
				[ ] "#1"
				[ ] "$57664"
[ ] 
[+] window ChildWin CustomerInformation
	[ ] tag "Customer Information"
	[ ] parent GMO
	[-] DialogBox DialogBox1
		[ ] tag "$59648"
		[+] CustomWin BitMap2
			[+] multitag "[BitMap]#2"
				[ ] "[BitMap]$65535[2]"
		[+] CustomWin BitMap1
			[+] multitag "[BitMap]#1"
				[ ] "[BitMap]$65535[1]"
		[+] StaticText CustomerInformationText
			[+] multitag "Customer Information"
				[ ] "#1"
				[ ] "$65535[1]"
		[+] StaticText CustomerNumberText
			[+] multitag "Customer Number:"
				[ ] "#2"
				[ ] "$65535[2]"
		[+] TextField CustomerNumber
			[+] multitag "Customer Number:"
				[ ] "#1"
				[ ] "$1000"
		[+] StaticText FirstNameText
			[+] multitag "First Name:"
				[ ] "#3"
				[ ] "$65535[3]"
		[+] TextField CustomerInformation
			[+] multitag "Customer Information"
				[ ] "#2"
				[ ] "$1001"
		[+] StaticText LastNameText
			[+] multitag "Last Name:"
				[ ] "#4"
				[ ] "$65535[4]"
		[+] TextField LastName
			[+] multitag "Last Name:"
				[ ] "#3"
				[ ] "$1002"
		[+] StaticText Address1Text
			[+] multitag "Address 1:"
				[ ] "#5"
				[ ] "$65535[5]"
		[+] TextField Address1
			[+] multitag "Address 1:"
				[ ] "#4"
				[ ] "$1003"
		[+] StaticText Address2Text
			[+] multitag "Address 2:"
				[ ] "#6"
				[ ] "$65535[6]"
		[+] TextField Address2
			[+] multitag "Address 2:"
				[ ] "#5"
				[ ] "$1004"
		[+] StaticText CityText
			[+] multitag "City:"
				[ ] "#7"
				[ ] "$65535[7]"
		[+] TextField City
			[+] multitag "City:"
				[ ] "#6"
				[ ] "$1005"
		[+] StaticText StateText
			[+] multitag "State:"
				[ ] "#8"
				[ ] "$65535[8]"
		[+] TextField State
			[+] multitag "State:"
				[ ] "#7"
				[ ] "$1006"
		[+] StaticText ZipText
			[+] multitag "Zip:"
				[ ] "#9"
				[ ] "$65535[9]"
		[+] TextField Zip1
			[+] multitag "Zip:[1]"
				[ ] "#8"
				[ ] "$1007"
		[+] StaticText PhoneText
			[+] multitag "Phone:"
				[ ] "#10"
				[ ] "$65535[10]"
		[+] TextField Zip2
			[+] multitag "Zip:[2]"
				[ ] "#9"
				[ ] "$1008"
		[+] PushButton Search
			[+] multitag "Search"
				[ ] "#1"
				[ ] "$1009"
		[+] PushButton Add
			[+] multitag "Add"
				[ ] "#2"
				[ ] "$1010"
		[+] PushButton Update
			[+] multitag "Update"
				[ ] "#3"
				[ ] "$1011"
		[+] PushButton PlaceOrder
			[+] multitag "Place Order"
				[ ] "#4"
				[ ] "$1012"
		[+] PushButton Clear
			[+] multitag "Clear"
				[ ] "#5"
				[ ] "$1013"
		[+] PushButton Close
			[+] multitag "Close"
				[ ] "#6"
				[ ] "$1014"
[ ] 
[+] window DialogBox CustomerSearch
	[ ] tag "Customer Search"
	[ ] parent GMO
	[+] PushButton Select
		[+] multitag "Select"
			[ ] "#1"
			[ ] "$1"
	[+] PushButton Cancel
		[+] multitag "Cancel"
			[ ] "#2"
			[ ] "$2"
	[+] ListBox Customers
		[+] multitag "Customers:"
			[ ] "#1"
			[ ] "$1036"
	[+] StaticText CustomerNumberText
		[+] multitag "Customer Number:"
			[ ] "#2"
			[ ] "$65535[2]"
	[+] TextField CustomerNumber
		[+] multitag "Customer Number:"
			[ ] "#1"
			[ ] "$1037"
	[+] StaticText AddressText
		[+] multitag "Address:"
			[ ] "#3"
			[ ] "$65535[3]"
	[+] TextField Address
		[+] multitag "Address:"
			[ ] "#2"
			[ ] "$1038"
	[+] StaticText CityText
		[+] multitag "City:"
			[ ] "#4"
			[ ] "$65535[4]"
	[+] TextField City
		[+] multitag "City:"
			[ ] "#3"
			[ ] "$1039"
	[+] StaticText CustomersText
		[+] multitag "Customers:"
			[ ] "#1"
			[ ] "$65535[1]"
[ ] 
[+] window ChildWin PlaceOrder
	[ ] tag "Place Order"
	[ ] parent GMO
	[+] DialogBox DialogBox1
		[ ] tag "$59648"
		[+] StaticText NameText
			[+] multitag "Name:"
				[ ] "#2"
				[ ] "$-1[2]"
		[+] TextField Name
			[+] multitag "Name:"
				[ ] "#1"
				[ ] "$1024"
		[+] StaticText CustomerText
			[+] multitag "Customer"
				[ ] "#1"
				[ ] "$-1[1]"
		[+] StaticText NumberText
			[+] multitag "Number:"
				[ ] "#3"
				[ ] "$-1[3]"
		[+] TextField Customer
			[+] multitag "Customer"
				[ ] "#2"
				[ ] "$1025"
		[+] StaticText ItemDetailText
			[+] multitag "Item Detail"
				[ ] "#4"
				[ ] "$-1[4]"
		[+] StaticText NumberInStockText
			[+] multitag "Number in Stock:"
				[ ] "#5"
				[ ] "$-1[5]"
		[+] TextField NumberInStock
			[+] multitag "Number in Stock:"
				[ ] "#3"
				[ ] "$1026"
		[+] StaticText UnitPriceText
			[+] multitag "Unit Price:"
				[ ] "#7"
				[ ] "$-1[7]"
		[+] TextField UnitPrice
			[+] multitag "Unit Price:"
				[ ] "#5"
				[ ] "$1027"
		[+] StaticText ItemNameText
			[+] multitag "Item Name:"
				[ ] "#10"
				[ ] "$-1[10]"
		[+] TextField ItemName
			[+] multitag "Item Name:"
				[ ] "#7"
				[ ] "$1028"
		[+] StaticText ItemNumberText
			[+] multitag "Item Number:"
				[ ] "#6"
				[ ] "$-1[6]"
		[+] TextField ItemDetail
			[+] multitag "Item Detail"
				[ ] "#4"
				[ ] "$1030"
		[+] StaticText QuantityText
			[+] multitag "Quantity:"
				[ ] "#8"
				[ ] "$-1[8]"
		[+] TextField Quantity
			[+] multitag "Quantity:"
				[ ] "#6"
				[ ] "$1031"
		[+] UpDown Spin1
			[+] multitag "Spin1"
				[ ] "#1"
				[ ] "$1079"
		[+] RadioList Size1
			[+] multitag "Size"
				[ ] "#1"
				[ ] "$1080"
		[+] StaticText DescriptionText
			[+] multitag "Description:"
				[ ] "#11"
				[ ] "$-1[11]"
		[+] TextField Description
			[+] multitag "Description:"
				[ ] "#8"
				[ ] "$1029"
		[+] CustomWin Size2
			[+] multitag "[BitMap]Size"
				[ ] "[BitMap]#1"
				[ ] "[BitMap]$1047"
		[+] PushButton Search
			[+] multitag "Search"
				[ ] "#1"
				[ ] "$1032"
		[+] PushButton OrderItem
			[+] multitag "Order Item"
				[ ] "#2"
				[ ] "$1033"
		[+] PushButton CompleteOrder
			[+] multitag "Complete Order"
				[ ] "#3"
				[ ] "$1034"
		[+] PushButton Close
			[+] multitag "Close"
				[ ] "#4"
				[ ] "$1035"
		[+] StaticText SizeText
			[+] multitag "Size"
				[ ] "#9"
				[ ] "$-1[9]"
[ ] 
[+] window DialogBox ItemSearch
	[ ] tag "Item Search"
	[ ] parent GMO
	[+] PushButton Select
		[+] multitag "Select"
			[ ] "#1"
			[ ] "$1"
	[+] PushButton Cancel
		[+] multitag "Cancel"
			[ ] "#2"
			[ ] "$2"
	[+] ListBox Items
		[+] multitag "Items:"
			[ ] "#1"
			[ ] "$1040"
	[+] StaticText ItemNumberText
		[+] multitag "Item Number:"
			[ ] "#2"
			[ ] "$65535[2]"
	[+] TextField ItemNumber
		[+] multitag "Item Number:"
			[ ] "#1"
			[ ] "$1041"
	[+] StaticText QuantityText
		[+] multitag "Quantity:"
			[ ] "#3"
			[ ] "$65535[3]"
	[+] TextField Quantity
		[+] multitag "Quantity:"
			[ ] "#2"
			[ ] "$1042"
	[+] StaticText PriceText
		[+] multitag "Price:"
			[ ] "#4"
			[ ] "$65535[4]"
	[+] TextField Price
		[+] multitag "Price:"
			[ ] "#3"
			[ ] "$1043"
	[+] StaticText ItemsText
		[+] multitag "Items:"
			[ ] "#1"
			[ ] "$65535[1]"
[ ] 
[+] window ChildWin CompleteOrder
	[ ] tag "Complete Order"
	[ ] parent GMO
	[-] DialogBox DialogBox1
		[ ] tag "$59648"
		[+] StaticText NameText
			[+] multitag "Name:"
				[ ] "#2"
				[ ] "$-1[2]"
		[+] TextField Name
			[+] multitag "Name:"
				[ ] "#1"
				[ ] "$1064"
		[+] StaticText CustomerNumberText
			[+] multitag "Customer Number:"
				[ ] "#1"
				[ ] "$-1[1]"
		[+] TextField CustomerNumber
			[+] multitag "Customer Number:"
				[ ] "#2"
				[ ] "$1065"
		[+] StaticText OrderNumberText
			[+] multitag "Order Number:"
				[ ] "#3"
				[ ] "$-1[3]"
		[+] TextField OrderNumber
			[+] multitag "Order Number:"
				[ ] "#3"
				[ ] "$1066"
		[+] CheckBox SendNewProductCatalog
			[+] multitag "Send New Product Catalog"
				[ ] "#1"
				[ ] "$1052"
		[+] StaticText PaymentTypeText
			[+] multitag "Payment Type"
				[ ] "#4"
				[ ] "$-1[4]"
		[+] RadioList PaymentType
			[+] multitag "Payment Type"
				[ ] "#1"
				[ ] "$1049"
		[+] StaticText CardNumberText
			[+] multitag "Card Number:"
				[ ] "#5"
				[ ] "$-1[5]"
		[+] TextField CardNumber
			[+] multitag "Card Number:"
				[ ] "#5"
				[ ] "$1063"
		[+] StaticText ExpirationDateText
			[+] multitag "Expiration Date:"
				[ ] "#8"
				[ ] "$-1[8]"
		[+] TextField ExpirationDate
			[+] multitag "Expiration Date:"
				[ ] "#7"
				[ ] "$1062"
		[+] StaticText ConfirmationText
			[+] multitag "Confirmation:"
				[ ] "#10"
				[ ] "$-1[10]"
		[+] TextField Confirmation
			[+] multitag "Confirmation:"
				[ ] "#9"
				[ ] "$1061"
		[+] StaticText ProductTotalText
			[+] multitag "Product Total:"
				[ ] "#6"
				[ ] "$-1[6]"
		[+] TextField ProductTotal
			[+] multitag "Product Total:"
				[ ] "#4"
				[ ] "$1056"
		[+] StaticText TaxText
			[+] multitag "Tax:"
				[ ] "#7"
				[ ] "$-1[7]"
		[+] TextField Tax
			[+] multitag "Tax:"
				[ ] "#6"
				[ ] "$1057"
		[+] StaticText ShippingText
			[+] multitag "Shipping:"
				[ ] "#9"
				[ ] "$-1[9]"
		[+] TextField Shipping
			[+] multitag "Shipping:"
				[ ] "#8"
				[ ] "$1058"
		[+] StaticText TotalCostText
			[+] multitag "Total Cost:"
				[ ] "#11"
				[ ] "$-1[11]"
		[+] TextField TotalCost1
			[+] multitag "Total Cost:"
				[ ] "#10"
				[ ] "$1059"
		[+] PushButton ProcessOrder
			[+] multitag "Process Order"
				[ ] "#1"
				[ ] "$1053"
		[+] PushButton NextOrder
			[+] multitag "Next Order"
				[ ] "#2"
				[ ] "$1054"
		[+] PushButton Cancel
			[+] multitag "Cancel"
				[ ] "#3"
				[ ] "$1055"
		[+] CustomWin TotalCost2
			[-] multitag "[AfxWnd40]Total Cost:"
				[ ] "[AfxWnd40]#1"
				[ ] "[AfxWnd40]$0"
[ ] 
[+] window DialogBox DBAdmin
	[ ] tag "DB Admin"
	[ ] parent GMO
	[+] PushButton DeleteSelectedCustomer
		[+] multitag "Delete Selected Customer"
			[ ] "#1"
			[ ] "$1083"
	[+] PushButton Close
		[+] multitag "Close"
			[ ] "#2"
			[ ] "$2"
	[+] ListBox Customers
		[+] multitag "Customers:"
			[ ] "#1"
			[ ] "$1086"
	[+] StaticText CustomerNumberText
		[+] multitag "Customer Number:"
			[ ] "#2"
			[ ] "$65535[2]"
	[+] TextField CustomerNumber
		[+] multitag "Customer Number:"
			[ ] "#1"
			[ ] "$1087"
	[+] StaticText HasPlacedOrderText
		[+] multitag "Has Placed Order:"
			[ ] "#3"
			[ ] "$65535[3]"
	[+] TextField HasPlacedOrder
		[+] multitag "Has Placed Order:"
			[ ] "#2"
			[ ] "$1088"
	[+] StaticText CustomersText
		[+] multitag "Customers:"
			[ ] "#1"
			[ ] "$65535[1]"
[ ] 
[+] window DialogBox AboutGMO
	[ ] tag "About Green Mountain Outpost"
	[ ] parent GMO
	[+] StaticText GreenMountainOutpostText
		[+] multitag "Green Mountain Outpost"
			[ ] "#1"
			[ ] "$65535[1]"
	[+] StaticText Copyright1999Text
		[+] multitag "Copyright © 1999"
			[ ] "#3"
			[ ] "$65535[3]"
	[+] StaticText N32BitVersion10Text
		[+] multitag "32-bit Version 1.0"
			[ ] "#2"
			[ ] "$65535[2]"
	[+] StaticText SegueSoftwareIncText
		[+] multitag "Segue Software, Inc."
			[ ] "#4"
			[ ] "$65535[4]"
	[+] CustomWin BitMap1
		[+] multitag "[BitMap]#1"
			[ ] "[BitMap]$65535"
	[+] PushButton OK
		[+] multitag "OK"
			[ ] "#1"
			[ ] "$1"
[ ] 
[+] appstate newCustomerState () basedon DefaultBaseState
	[ ] GMO.SetActive()
	[ ] GMO.Order.CustomerInformation.Pick ()
	[ ] CustomerInformation.SetActive()
	[ ] CustomerInformation.DialogBox1.Address1.SetText( GMO.NEW_CUSTOMER.address1 )
	[ ] CustomerInformation.DialogBox1.Address2.SetText( GMO.NEW_CUSTOMER.address2 )
	[ ] CustomerInformation.DialogBox1.City.SetText( GMO.NEW_CUSTOMER.city )
	[ ] CustomerInformation.DialogBox1.CustomerInformation.SetText( GMO.NEW_CUSTOMER.firstname )
	[ ] CustomerInformation.DialogBox1.LastName.SetText( GMO.NEW_CUSTOMER.lastname )
	[ ] CustomerInformation.DialogBox1.Zip2.SetText( GMO.NEW_CUSTOMER.phone )
	[ ] CustomerInformation.DialogBox1.State.SetText( GMO.NEW_CUSTOMER.state )
	[ ] CustomerInformation.DialogBox1.Zip1.SetText( GMO.NEW_CUSTOMER.zip )
[ ] 
[-] appstate newOrderPlaced() basedon DefaultBaseState
	[ ] GMO.SetActive ()
	[ ] GMO.Order.CustomerInformation.Pick ()
	[ ] CustomerInformation.SetActive ()
	[ ] CustomerInformation.DialogBox1.Search.Click ()
	[ ] CustomerSearch.SetActive ()
	[ ] CustomerSearch.Customers.Select ("lastname, firstname")
	[ ] CustomerSearch.Select.Click ()
	[ ] CustomerInformation.SetActive ()
	[ ] CustomerInformation.DialogBox1.PlaceOrder.Click ()
	[ ] PlaceOrder.SetActive ()
	[ ] PlaceOrder.DialogBox1.Search.Click ()
	[ ] ItemSearch.SetActive ()
	[ ] ItemSearch.Items.Select ("3 PERSON DOME TENT")
	[ ] ItemSearch.Select.Click ()
	[ ] PlaceOrder.SetActive ()
	[ ] PlaceOrder.DialogBox1.Spin1.Increment ()
	[ ] PlaceOrder.DialogBox1.OrderItem.Click ()
	[ ] PlaceOrder.DialogBox1.Search.Click ()
	[ ] ItemSearch.SetActive ()
	[ ] ItemSearch.Items.Select ("PADDED SOCKS")
	[ ] ItemSearch.Select.Click ()
	[ ] PlaceOrder.SetActive ()
	[ ] PlaceOrder.DialogBox1.Spin1.Increment ()
	[ ] PlaceOrder.DialogBox1.Spin1.Increment ()
	[ ] PlaceOrder.DialogBox1.Size1.Select ("L")
	[ ] PlaceOrder.DialogBox1.OrderItem.Click ()
	[ ] sleep( 10 )
	[ ] PlaceOrder.DialogBox1.CompleteOrder.Click ()
	[ ] 
[ ] 
[+] appstate newOrderSaved() basedon newOrderPlaced
	[ ] CompleteOrder.SetActive()
	[ ] CompleteOrder.DialogBox1.CardNumber.SetText( "111-222-333" )
	[ ] CompleteOrder.DialogBox1.ExpirationDate.SetText( "12/13" )
	[ ] CompleteOrder.DialogBox1.ProcessOrder.Click()
	[ ] sleep( 10 )
	[ ] CompleteOrder.Close()
