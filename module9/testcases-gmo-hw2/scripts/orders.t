﻿[ ] 
[ ] // Descripcion: verificar el contenido de una preorden antes de ser procesada
[+] testcase verifyPreOrderForm() appstate newOrderPlaced
	[ ] CompleteOrder.SetActive()
	[ ] verify( CompleteOrder.DialogBox1.ProductTotal.GetText(),  "$359.96")
	[ ] verify( CompleteOrder.DialogBox1.Tax.GetText(),  "$18.00")
	[ ] verify( CompleteOrder.DialogBox1.Shipping.GetText(),  "$12.20")
	[ ] verify( CompleteOrder.DialogBox1.TotalCost1.GetText(),  "$390.16")
	[ ] CompleteOrder.Close()
	[ ] CustomerInformation.Close()
	[ ] 
[ ] // Descripcion: verificar una order procesada en la base de datos
[+] testcase verifySavedOrder() appstate newOrderSaved
	[ ] string customer_number = CustomerInformation.DialogBox1.CustomerNumber.GetText()
	[ ] verify( isOrderPresentFor( customer_number ) , true)
	[ ] CustomerInformation.Close()
	[ ] 
[+] boolean isOrderPresentFor( string customer_number )
	[ ] boolean found = false
	[ ] string order_nr, status, prod_total, tax, shipping, card, expiration, payment, confirmation, c_number
	[ ] HDATABASE hdbc
	[ ] HSQL statement
	[ ] hdbc = DB_Connect( "dsn=Green2" )
	[ ] statement = DB_ExecuteSql( hdbc , "SELECT * FROM ordb")
	[-] while( DB_FetchNext( statement , order_nr, status, prod_total, tax, shipping, card, expiration, payment, confirmation, c_number) )
		[-] if trim( c_number ) == customer_number
			[ ] found = true
			[ ] break
	[ ] 
	[ ] DB_FinishSql( statement )
	[ ] DB_Disconnect( hdbc ) 
	[ ] return found
	[ ] 
