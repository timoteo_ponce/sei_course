﻿[+] type ItemNotas is record
	[ ] string pp1
	[ ] string pp2
	[ ] string pp3
	[ ] string pp4
	[ ] string pp5
	[ ] string pa1
	[ ] string pa2
	[ ] string p1
	[ ] string p2
	[ ] string p3 
	[ ] string p4
	[ ] string p5
	[ ] string asistencia
	[ ] 
[+] type EvalItem is record
	[ ] int no
	[ ] string nombre 
	[ ] string nit
	[ ] string ci
	[ ] ItemNotas notas
	[ ] real total
	[ ] string rawLine
	[ ] 
[+] list of EvalItem readEvaluations()
	[ ] list of EvalItem evalItems
	[ ] list of string lines
	[ ] int i 
	[ ] listread( lines , CurrentPath + "\..\data\2\FullEval.csv" )
	[-] for (i = 2 ; i <= listCount( lines ) ; i++)
		[ ] string line = lines[i]
		[ ] EvalItem item
		[ ] item.no = val( getfield( line , "," , 1 ) )
		[ ] item.nombre = getfield( line , "," , 2 )
		[ ] item.nit = getfield( line , "," , 3 )
		[ ] item.ci = getfield( line , "," , 4 )
		[ ] item.notas = parseNotas ( line , 5 )
		[ ] item.total = val( getfield( line , "," , 18 ) )
		[ ] item.rawLine = line
		[ ] listappend( evalItems, item)
	[ ] return evalItems
[ ] 
[+] ItemNotas parseNotas ( string line, int index )
	[ ] ItemNotas notas 
	[ ] notas.pp1 = getfield( line , "," , index )
	[ ] notas.pp2 = getfield( line , "," , index + 1 )
	[ ] notas.pp3 = getfield( line , "," , index + 2 )
	[ ] notas.pp4 = getfield( line , "," , index + 3 )
	[ ] notas.pp5 = getfield( line , "," , index + 4 )
	[ ] notas.pa1 = getfield( line , "," , index + 5 )
	[ ] notas.pa2 = getfield( line , "," , index + 6 )
	[ ] notas.p1 = getfield( line , "," , index + 7 )
	[ ] notas.p2 = getfield( line , "," , index + 8 )
	[ ] notas.p3 = getfield( line , "," , index + 9 )
	[ ] notas.p4 = getfield( line , "," , index + 10 )
	[ ] notas.p5 = getfield( line , "," , index + 11 )
	[ ] notas.asistencia = getfield( line , "," , index + 12 )
	[ ] return notas
	[ ] 
[ ] 
[ ] //===============1==============
[ ] // 1.	El archivo FullEval.csv contiene la evaluación semestral para el curso de programación. 
[ ] // (PP = Prueba Periódica, PA = Prueba Anunciada, P = Practica, NSP = No se Presento).
[ ] //
[ ] // Implementar una función que verifique que la suma de las evaluaciones efectivamente es el valor que aparece en la columna TOTAL
[+] void verifyTotalInNotas()
	[ ] print("=========== verifyTotalInNotas ==========")
	[ ] list of EvalItem evalItems = readEvaluations()
	[ ] EvalItem item 
	[-] for each item in evalItems
		[ ] number total = getTotal( item.notas )
		[-] if item.total != total
			[ ] print ( " Nota invalida el el item : {item.no} = {item.total}, el esperado es : {total}")
[+] real getTotal( ItemNotas notas)
	[ ] return getRealValue( notas.pp1, notas.pp2, notas.pp3, notas.pp4, notas.pp5, notas.pa1, notas.pa2, notas.p1, notas.p2, notas.p3, notas.p4, notas.p5, notas.asistencia )
	[ ] 
[+] real getRealValue( varargs of string values)
	[ ] string value
	[ ] number sum = 0
	[-] for each value in values
		[-] if value != "NSP" && value != "NP"
			[ ] sum += val ( value )
	[ ]  return sum
[ ] 
[ ] // Implementar una función que dado un apellido imprima las evaluaciones que obtuvo
[+] void printNotas( string apellido )
	[ ] print("=========== printNotas ==========")
	[ ] list of EvalItem evalItems = readEvaluations()
	[ ] boolean found = false
	[ ] int i
	[ ] EvalItem item 
	[-] for ( i = 1 ; i <= listcount( evalItems ) && found == false; i++ )
		[ ] item = evalItems[ i ]
		[-] if item.nombre == apellido
			[ ] found = true
	[-] if found
		[ ] printNotasOf( item )
		[ ] 
[+] void printNotasOf( EvalItem item)
	[ ] ItemNotas notas = item.notas
	[ ] print( "Notas de {item.nombre} [pp1={notas.pp1},pp2={notas.pp2},pp3={notas.pp3},pp4={notas.pp4},pp5={notas.pp5},pa1={notas.pa1},pa2={notas.pa2},p1={notas.p1},p2={notas.p2},p3={notas.p3},p4={notas.p4},p5={notas.p5},asistencia={notas.asistencia},total={item.total}]" )
	[ ] 
[ ] 
[ ] // Implementar una función que imprima la lista de puntaje mayor a menor
[+] void printSortedNotas( )
	[ ] print("=========== printSortedNotas ==========")
	[ ] list of EvalItem evalItems = readEvaluations()
	[ ] list of real scores 
	[ ] list of real sortedScores 
	[ ] EvalItem item 
	[-] for each item in evalItems
		[ ] listappend( scores , item.total )
	[ ] listsort( scores ) 
	[ ] int i = listcount( scores )
	[-] while i-- > 1
		[ ] listappend( sortedScores, scores[ i ] )
	[ ] listprint( sortedScores )
[ ] 
[ ] // Implementar una función que imprima los alumnos habilitados (TOTAL >=30) 
[+] void printHabilitados()
	[ ] print("=========== printHabilitados ==========")
	[ ] list of EvalItem evalItems = readEvaluations()
	[ ] EvalItem item 
	[-] for each item in evalItems
		[-] if item.total >= 30
			[ ] printNotasOf( item )
[ ] 
[ ] //2. Los archivos notas.txt y Alumnos.txt contienen las notas y apellidos de la evaluación semestral
[ ] //
[+] list of EvalItem readNotaAlumnos()
	[ ] list of EvalItem evalItems
	[ ] list of string alumnos
	[ ] list of string notas
	[ ] int i 
	[ ] listread( alumnos , CurrentPath + "\..\data\2\Alumnos.txt" )
	[ ] listread( notas , CurrentPath + "\..\data\2\notas.txt" )
	[-] for (i = 2 ; i <= listCount( alumnos ) ; i++)
		[ ] string nota = notas[i]
		[ ] EvalItem item
		[ ] item.no = i - 1
		[ ] item.nombre = alumnos[i]
		[ ] item.notas = parseNotas ( nota , 1 )
		[ ] item.total = val( getfield( nota , "," , 13 ) )
		[ ] listappend( evalItems, item)
	[ ] return evalItems
[ ] 
[ ] // Implementar una función que guarde en un archivo de texto la lista final de alumnos y su nota final. 
[+] void saveNotasAlumnos()
	[ ] print("=========== saveNotasAlumnos ==========")
	[ ] list of EvalItem evalItems = readNotaAlumnos()
	[ ] EvalItem item
	[ ] HFILE file
	[ ] file = FileOpen("notas_alumnos.txt", FM_WRITE)
	[ ] filewriteline( file , "NOMBRE | NOTA FINAL")
	[-] for each item in evalItems
		[ ] filewriteline( file , "{item.nombre} | {getTotal( item.notas )}" )
	[ ] fileclose( file ) 
[ ] // Implementar una función que guarde en un archivo de texto el nombre y su promedio de notas.
[+] void savePromedioNotasAlumnos()
	[ ] print("=========== savePromedioNotasAlumnos ==========")
	[ ] list of EvalItem evalItems = readNotaAlumnos()
	[ ] EvalItem item
	[ ] HFILE file
	[ ] file = FileOpen("promedio_notas_alumnos.txt", FM_WRITE)
	[ ] filewriteline( file , "NOMBRE | PROMEDIO")
	[-] for each item in evalItems
		[ ] filewriteline( file , "{item.nombre} | {getTotal( item.notas )/13}" )
	[ ] fileclose( file ) 
	[ ] 
[ ] // Implementar una función guarde en un archivo de texto lista de puntaje menor a mayor
[+] void saveAscSortedNotas()
	[ ] print("=========== saveAscSortedNotas ==========")
	[ ] list of EvalItem evalItems = readNotaAlumnos()
	[ ] EvalItem item
	[ ] list of real scores 
	[-] for each item in evalItems
		[ ] listappend( scores , getTotal( item.notas ) )
	[ ] listsort( scores )
	[ ] HFILE file
	[ ] file = FileOpen("sorted_notas.txt", FM_WRITE)
	[ ] filewriteline( file , " NOTA")
	[ ] real score
	[-] for each score in scores
		[ ] filewriteline( file , str( score ) )
	[ ] fileclose( file ) 
	[ ] 
[ ] // Implementar una función que imprima los alumnos que están sobre el promedio de notas del curso
[-] void printNotasAlumnoSobrePromedio( number promedio )
	[ ] print("=========== printNotasAlumnoSobrePromedio ({promedio}) ==========")
	[ ] list of EvalItem evalItems = readNotaAlumnos()
	[ ] EvalItem item
	[-] for each item in evalItems
		[ ] number avg = getTotal( item.notas )/13
		[-] if avg >= promedio
			[ ] print( " {item.nombre} - {avg}" )
	[ ] 
[ ] 
[-] main()
	[ ] // PART 1
	[ ] verifyTotalInNotas()
	[ ] //
	[ ] printNotas( "CASTELLON" )
	[ ] printNotas( "TOLA" )
	[ ] //
	[ ] printSortedNotas()
	[ ] //
	[ ] printHabilitados()
	[ ] // PART 2
	[ ] saveNotasAlumnos()
	[ ] //
	[ ] savePromedioNotasAlumnos()
	[ ] //
	[ ] saveAscSortedNotas()
	[ ] // 
	[ ] printNotasAlumnoSobrePromedio( 1 )
