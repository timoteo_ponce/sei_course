﻿[+] type CsvEvent is record
	[ ] string severityCategory
	[ ] int severity
	[ ] string unknown1
	[ ] string status
	[ ] int eventId
	[ ] int parentEventId
	[ ] string hasComments
	[ ] int eventCount
	[ ] string serverName
	[ ] datetime lastOccurredConsole
	[ ] datetime lastOcurredAgent
	[ ] string knowledgeScript
	[ ] int jobId
	[ ] string repository
	[ ] string knowledgeScriptGroupName
	[ ] string knowledgeScriptName
	[ ] string categoryName
	[ ] string comment
	[ ] string message
	[ ] string rawLine
[ ] 
[-] list of CsvEvent readCsvEvents()
	[ ] list of CsvEvent events
	[ ] list of string lines
	[ ] int i 
	[ ] listread( lines , CurrentPath + "\..\data\1\SampleGrid.csv" )
	[+] for (i = 2 ; i < listCount( lines ) ; i++)
		[ ] string line = lines[i]
		[ ] CsvEvent event
		[ ] event.rawLine = line
		[ ] event.severityCategory = getfield ( line , "|" , 2 )
		[ ] event.severity = val( getfield ( line , "|" , 3 ) ) 
		[ ] event.unknown1 = getfield ( line , "|" , 4 )
		[ ] event.status = getfield ( line , "|" , 5 )
		[ ] event.eventId = val( getfield ( line , "|" , 6 ) )
		[ ] event.parentEventId = val( getfield ( line , "|" , 7 ) ) 
		[ ] event.hasComments = getfield ( line , "|" , 8 )
		[ ] event.eventCount = val( getfield ( line , "|" , 9 ) )
		[ ] event.serverName = getfield ( line , "|" , 10 )  
		[ ] event.lastOccurredConsole = parseDateTime( getfield ( line , "|" ,11 ) )
		[ ] event.lastOcurredAgent = parseDateTime( getfield ( line , "|" , 12 ) ) 
		[ ] event.knowledgeScript = getfield ( line , "|" , 13 )
		[ ] event.jobId = val( getfield ( line , "|" , 14 ) )
		[ ] event.repository = getfield ( line , "|" , 15 )
		[ ] event.knowledgeScriptGroupName = getfield ( line , "|" , 16 )
		[ ] event.knowledgeScriptName = getfield ( line , "|" , 17 )
		[ ] event.categoryName = getfield ( line , "|" , 18 )
		[ ] event.comment =  getfield ( line , "|" , 19 )
		[ ] event.message = getfield ( line , "|" , 20 )
		[ ] listappend( events, event )
	[ ] return events
[ ] 
[-] datetime parseDateTime(string dateString)
	[ ] string datePart = getField( dateString , " " , 1 )
	[ ] string timePart = getField( dateString , " " , 2 )
	[ ] int year = val ( getfield( datePart, "/", 3 ) )
	[ ] int month = val ( getfield( datePart, "/", 1 ) )
	[ ] int day = val ( getfield( datePart, "/", 2 ) ) 
	[ ] int hour = val ( getfield( timePart, ":" , 1 ) ) 
	[ ] int minute = val ( getfield( timePart, ":" , 2 ) ) 
	[ ] int seconds = val ( getfield( timePart, ":" , 3 ) )
	[ ] //print( "makedatetime(  {year}, {month} ,{day} ,{hour} , {minute} ,{seconds} )" ) 
	[ ] return makedatetime(  year , month , day , hour , minute , seconds )
	[ ] 
[-] void printEvents( string severity )
	[ ] print( "--------- Printing events with severity {severity} ---------" )
	[ ] list of CsvEvent events = readCsvEvents()
	[ ] CsvEvent event
	[-] for each event in events
		[-] if event.severityCategory == severity 
			[ ] print( event.rawLine )
			[ ] 
[-] void printEventsUntil(  datetime untilDate ) 
	[ ] datetime startDate = "2010-01-01 00:00:00"
	[ ] print( "--------- Printing events between {startDate} < {untilDate} ---------" )
	[ ] int periodDifference = DiffDateTime ( untilDate, startDate)
	[ ] list of CsvEvent events = readCsvEvents()
	[ ] CsvEvent event
	[-] for each event in events
		[-] if DiffDateTime ( event.lastOcurredAgent , startDate ) <= periodDifference
			[ ] print ( event.rawLine )
	[ ] 
[-] main()
	[ ] printEvents( "Warning")
	[ ] datetime date1 = "2010-06-11 00:00:00"
	[ ] printEventsUntil( date1 )
