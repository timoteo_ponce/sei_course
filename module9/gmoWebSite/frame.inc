﻿[ ] const wMainWindow = GMO
[ ] 
[+] window BrowserChild GMO
	[ ]  tag "Welcome to Green Mountain Outpost"
	[ ] 
	[ ]  // The URL of this page
	[ ]  const sLocation = "http://demo.borland.com/gmopost"
	[ ] 
	[ ]  // The login user name
	[ ]  // const sUserName = ?
	[ ] 
	[ ]  // The login password
	[ ]  // const sPassword = ?
	[ ] 
	[ ]  // The size of the browser window
	[ ]  // const POINT BrowserSize = {600, 400}
	[ ] 
	[ ]  // Sets the browser font settings to the default
	[ ]  // const bDefaultFont = TRUE
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading GMOOnLine
		[ ] tag "GMO OnLine"
	[+] HtmlText WelcomeToGreenMountainOutp
		[ ] tag "Welcome to Green Mountain Outpost's OnLine Catalog!"
	[+] HtmlText WeAreMovingOur329Page199
		[ ] tag "We are moving our 329 page 1997 catalog to the internet to serve you better. Now you can browse our online database, see produ*"
	[+] HtmlText BeSureToVisitUsOnTheInt1
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*"
	[+] HtmlTable BeSureToVisitUsOnTheInt2
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*[1]"
		[+] HtmlColumn HtmlColumn1
			[ ] tag "#1"
			[+] HtmlPushButton EnterGMOOnLine
				[+] multitag "Enter GMO OnLine"
					[ ] "$bSubmit"
					[ ] "&name='bSubmit'"
		[+] HtmlColumn HtmlColumn2
			[ ] tag "#2"
			[+] HtmlPushButton AboutTheGMOSite
				[+] multitag "About The GMO Site"
					[ ] "$bAbout"
					[ ] "&name='bAbout'"
	[+] HtmlTable BeSureToVisitUsOnTheInt3
		[ ] tag "Be sure to visit us on the internet often. We're always adding new items and you always get the very best prices by shopping o*[2]"
		[+] HtmlColumn HtmlColumn1
			[ ] tag "#1"
			[+] HtmlPushButton BrowserTestPage
				[+] multitag "Browser Test Page"
					[ ] "$bBrowserTest"
					[ ] "&name='bBrowserTest'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild BrowserTestPage
	[ ] tag "Browser Test Page"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading AllBrowsersAreNotCreatedE1
		[ ] tag "All Browsers Are Not Created Equal"
	[+] HtmlImage Netscape
		[+] multitag "Netscape"
			[ ] "$http:??demo.borland.com?gmopost?images?nscp-ani.gif"
	[+] HtmlImage InternetExplorer
		[+] multitag "Internet Explorer"
			[ ] "$http:??demo.borland.com?gmopost?images?ie-anim.gif"
	[+] HtmlLink AllBrowsersAreNotCreatedE2
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$http:??demo.borland.com?gmopost?javaapplet.htm"
	[+] HtmlImage AllBrowsersAreNotCreatedE3
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$http:??demo.borland.com?gmopost?images?java-animate2.gif"
	[+] HtmlLink HttpDemoBorlandComGmopos1
		[ ] tag "$http:??demo.borland.com?gmopost?activex.htm"
	[+] HtmlImage HttpDemoBorlandComGmopos2
		[ ] tag "$http:??demo.borland.com?gmopost?images?ax-animate.gif"
	[+] HtmlTable HtmlTable1
		[ ] tag "#1"
		[+] HtmlColumn InternetExplorer30
			[ ] tag "Internet Explorer 3.0"
			[+] HtmlList BlinkingText
				[ ] tag "Blinking Text"
			[+] HtmlList DotBullets
				[ ] tag "dot bullets"
			[+] HtmlList CircleBullets
				[ ] tag "circle bullets"
		[+] HtmlColumn NetscapeNavigator30
			[ ] tag "Netscape Navigator 3.0"
			[+] HtmlText ColoredHorizontalRules
				[ ] tag "Colored Horizontal Rules"
			[+] HtmlText DefinitionTextStyleShouldB
				[ ] tag "Definition Text Style(should be an italic font)"
			[+] HtmlText TrademarksAreImportant
				[ ] tag "Trademarks are important™"
			[+] HtmlText TableProperties
				[ ] tag "Table Properties"
	[+] HtmlPushButton AllBrowsersAreNotCreatedE4
		[+] multitag "All Browsers Are Not Created Equal"
			[ ] "$I3"
			[ ] "&name='I3'"
	[+] HtmlMarquee SometimesEvenLeftAndRight
		[ ] tag "Sometimes Even Left and Right Doesn't Mean Anything"
	[+] HtmlPushButton LeftOrRight
		[+] multitag "<< Left or Right >>"
			[ ] "$B2"
			[ ] "&name='B2'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[ ] tag "This is a sample web site for demonstration purposes only!No products will be sent to you."
	[+] HtmlText InformationInThisDocumentI
		[ ] tag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
	[+] HtmlText Copyright1997SegueSoftwar
		[ ] tag "Copyright © 1997 Segue Software, Inc. All rights reserved."
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild OnLineCatalog
	[ ] tag "OnLine Catalog"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "#3"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "#1"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "#2"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading OnLineCatalog
		[+] multitag "OnLine Catalog"
			[ ] "#1"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlText TheseAreTheItemsCurrently1
		[+] multitag "These are the items currently available through our online catalog. Select the quantity of each item and then press the ?Plac*"
			[ ] "#1"
	[-] HtmlTable TheseAreTheItemsCurrently2
		[+] multitag "These are the items currently available through our online catalog. Select the quantity of each item and then press the ?Plac*"
			[ ] "#1"
		[+] HtmlColumn ItemNumber
			[+] multitag "Item?Number"
				[ ] "#1"
			[+] HtmlText N1000
				[+] multitag "1000"
					[ ] "#1"
			[+] HtmlText N1001
				[+] multitag "1001"
					[ ] "#2"
			[+] HtmlText N1002
				[+] multitag "1002"
					[ ] "#3"
			[+] HtmlText N1003
				[+] multitag "1003"
					[ ] "#4"
			[+] HtmlText N1004
				[+] multitag "1004"
					[ ] "#5"
			[+] HtmlText N1005
				[+] multitag "1005"
					[ ] "#6"
		[+] HtmlColumn ItemName
			[+] multitag "Item Name"
				[ ] "#2"
			[+] HtmlLink N3PersonDomeTent
				[+] multitag "3 Person Dome Tent"
					[ ] "#1"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#tents"
			[+] HtmlLink ExternalFrameBackpack
				[+] multitag "External Frame Backpack"
					[ ] "#2"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#backpacks"
			[+] HtmlLink GlacierSunGlasses
				[+] multitag "Glacier Sun Glasses"
					[ ] "#3"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#glasses"
			[+] HtmlLink PaddedSocks
				[+] multitag "Padded Socks"
					[ ] "#4"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#socks"
			[+] HtmlLink HikingBoots
				[+] multitag "Hiking Boots"
					[ ] "#5"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#boots"
			[+] HtmlLink BackCountryShorts
				[+] multitag "Back Country Shorts"
					[ ] "#6"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#shorts"
		[-] HtmlColumn UnitPrice
			[+] multitag "Unit?Price"
				[ ] "#3"
			[-] HtmlText N29999
				[ ] multitag "? #1"
			[-] HtmlText N17995
				[ ] multitag "? #2"
			[-] HtmlText N6799
				[ ] multitag "? #3"
			[-] HtmlText N1999
				[ ] multitag "? #4"
			[-] HtmlText N10990
				[ ] multitag "? #5"
			[-] HtmlText N2495
				[ ] multitag "? #6"
		[+] HtmlColumn OrderQuantity
			[+] multitag "Order?Quantity"
				[ ] "#4"
			[+] HtmlTextField N29999
				[+] multitag "? 299.99"
					[ ] "#1"
					[ ] "$QTY_TENTS"
					[ ] "&name='QTY_TENTS'"
			[+] HtmlTextField N17995
				[+] multitag "? 179.95"
					[ ] "#2"
					[ ] "$QTY_BACKPACKS"
					[ ] "&name='QTY_BACKPACKS'"
			[+] HtmlTextField N6799
				[+] multitag "? 67.99"
					[ ] "#3"
					[ ] "$QTY_GLASSES"
					[ ] "&name='QTY_GLASSES'"
			[+] HtmlTextField N1999
				[+] multitag "? 19.99"
					[ ] "#4"
					[ ] "$QTY_SOCKS"
					[ ] "&name='QTY_SOCKS'"
			[+] HtmlTextField N10990
				[+] multitag "? 109.90"
					[ ] "#5"
					[ ] "$QTY_BOOTS"
					[ ] "&name='QTY_BOOTS'"
			[+] HtmlTextField N2495
				[+] multitag "? 24.95"
					[ ] "#6"
					[ ] "$QTY_SHORTS"
					[ ] "&name='QTY_SHORTS'"
	[+] HtmlPushButton ResetForm
		[+] multitag "Reset Form"
			[ ] "#1"
			[ ] "$bReset"
			[ ] "&name='bReset'"
	[+] HtmlPushButton PlaceAnOrder
		[+] multitag "Place An Order"
			[ ] "#2"
			[ ] "$bSubmit"
			[ ] "&name='bSubmit'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "#2"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "#4"
	[+] HtmlText Copyright1997SegueSoftwar
		[+] multitag "Copyright © 1997 Segue Software, Inc. All rights reserved."
			[ ] "#3"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild GMOProducts
	[ ] tag "GMO Products"
	[ ] parent Browser
	[+] HtmlMeta ContentType
		[+] multitag "Content-Type"
			[ ] "#3"
			[ ] "$text?html;?charset=iso-8859-1"
	[+] HtmlMeta GENERATOR
		[+] multitag "GENERATOR"
			[ ] "#2"
			[ ] "$Microsoft?FrontPage?2.0[2]"
			[ ] "&name='GENERATOR'"
	[+] HtmlMeta FORMATTER
		[+] multitag "FORMATTER"
			[ ] "#1"
			[ ] "$Microsoft?FrontPage?2.0[1]"
			[ ] "&name='FORMATTER'"
	[+] HtmlHeading Products
		[+] multitag "Products"
			[ ] "#1"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading N3PersonDomeTent
		[+] multitag "3 Person Dome Tent"
			[ ] "#2"
	[+] HtmlText OurBest3PersonBackpackDom1
		[+] multitag "Our best 3 person backpack dome tent"
			[ ] "#1"
	[+] HtmlImage OurBest3PersonBackpackDom2
		[+] multitag "Our best 3 person backpack dome tent"
			[ ] "#2"
			[ ] "$http:??demo.borland.com?gmopost?images?tent.gif"
	[+] HtmlText UnitPrice1
		[+] multitag "Unit Price:[1]"
			[ ] "#2"
	[+] HtmlText N29999
		[+] multitag "? 299.99"
			[ ] "#3"
	[+] HtmlText InStock1
		[+] multitag "? In Stock:[1]"
			[ ] "#4"
	[+] HtmlText N23
		[+] multitag "23"
			[ ] "#5"
	[+] HtmlText ItemNumber1
		[+] multitag "Item Number:[1]"
			[ ] "#6"
	[+] HtmlText N1000
		[+] multitag "1000"
			[ ] "#7"
	[+] HtmlText HereSASuperbThreeSeasonM
		[+] multitag "Here's a superb three-season mountaineering?backpacking tent at a great price! This dome sleeps 3 and weighs in at only 4.5 lb*"
			[ ] "#8"
	[+] HtmlHeading ExternalFrameBackpack
		[+] multitag "External Frame Backpack"
			[ ] "#3"
	[+] HtmlText OurMostPopularExternalFram1
		[+] multitag "Our most popular external frame backpack"
			[ ] "#9"
	[+] HtmlImage OurMostPopularExternalFram2
		[+] multitag "Our most popular external frame backpack"
			[ ] "#3"
			[ ] "$http:??demo.borland.com?gmopost?images?backpack.gif"
	[+] HtmlText UnitPrice2
		[+] multitag "Unit Price:[2]"
			[ ] "#10"
	[+] HtmlText N17995
		[+] multitag "? 179.95"
			[ ] "#11"
	[+] HtmlText InStock2
		[+] multitag "? In Stock:[2]"
			[ ] "#12"
	[+] HtmlText N8
		[+] multitag "8"
			[ ] "#13"
	[+] HtmlText ItemNumber2
		[+] multitag "Item Number:[2]"
			[ ] "#14"
	[+] HtmlText N1001
		[+] multitag "1001"
			[ ] "#15"
	[+] HtmlText AnIdealPackForLongTrailT
		[+] multitag "An ideal pack for long trail trips, its strong heli-arc welded 6061-T6 aircraft-quality aluminum frame with V-truss design res*"
			[ ] "#16"
	[+] HtmlHeading GlacierSunGlasses
		[+] multitag "Glacier Sun Glasses"
			[ ] "#4"
	[+] HtmlText OurBestGlacierSunGlassesW1
		[+] multitag "Our best glacier sun glasses will protect you on your next expedition or when you hit the slopes."
			[ ] "#17"
	[+] HtmlImage OurBestGlacierSunGlassesW2
		[+] multitag "Our best glacier sun glasses will protect you on your next expedition or when you hit the slopes."
			[ ] "#4"
			[ ] "$http:??demo.borland.com?gmopost?images?glasses.gif"
	[+] HtmlText UnitPrice3
		[+] multitag "Unit Price:[3]"
			[ ] "#18"
	[+] HtmlText N6799
		[+] multitag "? 67.99"
			[ ] "#19"
	[+] HtmlText InStock3
		[+] multitag "? In Stock:[3]"
			[ ] "#20"
	[+] HtmlText N138
		[+] multitag "138"
			[ ] "#21"
	[+] HtmlText ItemNumber3
		[+] multitag "Item Number:[3]"
			[ ] "#22"
	[+] HtmlText N1002
		[+] multitag "1002"
			[ ] "#23"
	[+] HtmlText UseTheseFullCoverageFull
		[+] multitag "Use these full-coverage, full-protection sun glasses for all your outdoor activities. Polycarbonate lenses provide lightweight*"
			[ ] "#24"
	[+] HtmlHeading PaddedSocks
		[+] multitag "Padded Socks"
			[ ] "#5"
	[+] HtmlText OurFavoritePaddedSocksWill1
		[+] multitag "Our favorite padded socks will make your next back country trek easier on your feet."
			[ ] "#25"
	[+] HtmlImage OurFavoritePaddedSocksWill2
		[+] multitag "Our favorite padded socks will make your next back country trek easier on your feet."
			[ ] "#5"
			[ ] "$http:??demo.borland.com?gmopost?images?socks.gif"
	[+] HtmlText UnitPrice4
		[+] multitag "Unit Price:[4]"
			[ ] "#26"
	[+] HtmlText N1999
		[+] multitag "? 19.99"
			[ ] "#27"
	[+] HtmlText InStock4
		[+] multitag "? In Stock:[4]"
			[ ] "#28"
	[+] HtmlText N47
		[+] multitag "47"
			[ ] "#29"
	[+] HtmlText ItemNumber4
		[+] multitag "Item Number:[4]"
			[ ] "#30"
	[+] HtmlText N1003
		[+] multitag "1003"
			[ ] "#31"
	[+] HtmlText TheseHeavyweightSocksAreDe
		[+] multitag "These heavyweight socks are designed for long distances. Reinforced heels and flat-seam toes prevent blisters. Made of DuraTe*"
			[ ] "#32"
	[+] HtmlHeading HikingBoots
		[+] multitag "Hiking Boots"
			[ ] "#6"
	[+] HtmlText ClimbAnyMountainWithOurPe1
		[+] multitag "Climb any mountain with our PermaDry all leather hiking boots."
			[ ] "#33"
	[+] HtmlImage ClimbAnyMountainWithOurPe2
		[+] multitag "Climb any mountain with our PermaDry all leather hiking boots."
			[ ] "#6"
			[ ] "$http:??demo.borland.com?gmopost?images?boots.gif"
	[+] HtmlText UnitPrice5
		[+] multitag "Unit Price:[5]"
			[ ] "#34"
	[+] HtmlText N10990
		[+] multitag "? 109.90"
			[ ] "#35"
	[+] HtmlText InStock5
		[+] multitag "? In Stock:[5]"
			[ ] "#36"
	[+] HtmlText N32
		[+] multitag "32"
			[ ] "#37"
	[+] HtmlText ItemNumber5
		[+] multitag "Item Number:[5]"
			[ ] "#38"
	[+] HtmlText N1004
		[+] multitag "1004"
			[ ] "#39"
	[+] HtmlText ASolidChoiceForThroughHik
		[+] multitag "A solid choice for through-hikes and other demanding backpacking with heavy loads, our boots are surprisingly light--just 2 lb*"
			[ ] "#40"
	[+] HtmlHeading BackCountryShorts
		[+] multitag "Back Country Shorts"
			[ ] "#7"
	[+] HtmlText OurQuickDryingShortsAreA1
		[+] multitag "Our quick drying shorts are a customer favorite. Perfect for a day of any outdoor activity or just lounging around the camp."
			[ ] "#41"
	[+] HtmlImage OurQuickDryingShortsAreA2
		[+] multitag "Our quick drying shorts are a customer favorite. Perfect for a day of any outdoor activity or just lounging around the camp."
			[ ] "#7"
			[ ] "$http:??demo.borland.com?gmopost?images?shorts.gif"
	[+] HtmlText UnitPrice6
		[+] multitag "Unit Price:[6]"
			[ ] "#42"
	[+] HtmlText N2495
		[+] multitag "? 24.95"
			[ ] "#43"
	[+] HtmlText InStock6
		[+] multitag "? In Stock:[6]"
			[ ] "#44"
	[+] HtmlText N59
		[+] multitag "59"
			[ ] "#45"
	[+] HtmlText ItemNumber6
		[+] multitag "Item Number:[6]"
			[ ] "#46"
	[+] HtmlText N1005
		[+] multitag "1005"
			[ ] "#47"
	[+] HtmlText LightweightNylonShortsCanT
		[+] multitag "Lightweight nylon shorts can take on any activity and double as swimwear. Supplex nylon fabric is strong and dries quickly. Ny*"
			[ ] "#48"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "#49"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "#51"
	[+] HtmlText Copyright1997SegueSoftwar
		[+] multitag "Copyright © 1997 Segue Software, Inc. All rights reserved."
			[ ] "#50"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild PlaceOrder
	[ ] tag "Place Order"
	[ ] parent Browser
	[+] HtmlHeading PlaceOrder
		[+] multitag "Place Order"
			[ ] "#1"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlText YouHaveSelectedTheFollowin1
		[-] multitag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
			[ ] "#1"
	[-] HtmlTable YouHaveSelectedTheFollowin2
		[+] multitag "You have selected the following items. Click the ?Proceed With Order? button to continue, or use the browser's ?back? button t*"
			[ ] "#1"
		[+] HtmlColumn Qty
			[+] multitag "Qty"
				[ ] "#1"
			[-] HtmlText N1
				[ ] multitag "#1"
			[-] HtmlText N2
				[ ] multitag "#2"
			[-] HtmlText N3
				[ ] multitag "#3"
			[-] HtmlText N4
				[ ] multitag "#4"
			[-] HtmlText N5
				[ ] multitag "#5"
			[-] HtmlText N6
				[ ] multitag "#6"
		[+] HtmlColumn ProductDescription
			[+] multitag "Product Description"
				[ ] "#2"
			[+] HtmlLink N3PersonDomeTent
				[+] multitag "3 Person Dome Tent"
					[ ] "#1"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#tents"
			[+] HtmlLink ExternalFrameBackpack
				[+] multitag "External Frame Backpack"
					[ ] "#2"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#backpacks"
			[+] HtmlLink GlacierSunGlasses
				[+] multitag "Glacier Sun Glasses"
					[ ] "#3"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#glasses"
			[+] HtmlLink PaddedSocks
				[+] multitag "Padded Socks"
					[ ] "#4"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#socks"
			[+] HtmlLink HikingBoots
				[+] multitag "Hiking Boots"
					[ ] "#5"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#boots"
			[+] HtmlLink BackCountryShorts
				[+] multitag "Back Country Shorts"
					[ ] "#6"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#shorts"
		[+] HtmlColumn DeliveryStatus
			[+] multitag "Delivery Status"
				[ ] "#3"
			[-] HtmlText ToBeShipped1
				[-] multitag "To Be Shipped[1]"
					[ ] "#1"
			[-] HtmlText ToBeShipped2
				[-] multitag "To Be Shipped[2]"
					[ ] "#2"
			[+] HtmlText ToBeShipped3
				[+] multitag "To Be Shipped[3]"
					[ ] "#3"
			[+] HtmlText ToBeShipped4
				[+] multitag "To Be Shipped[4]"
					[ ] "#4"
			[+] HtmlText ToBeShipped5
				[+] multitag "To Be Shipped[5]"
					[ ] "#5"
			[+] HtmlText ToBeShipped6
				[+] multitag "To Be Shipped[6]"
					[ ] "#6"
			[-] HtmlText ProductTotal
				[+] multitag "Product Total"
					[ ] "#7"
			[+] HtmlText SalesTax
				[+] multitag "Sales Tax"
					[ ] "#8"
			[+] HtmlText ShippingHandling
				[+] multitag "Shipping  Handling"
					[ ] "#9"
			[+] HtmlText GrandTotal
				[+] multitag "Grand Total"
					[ ] "#10"
		[+] HtmlColumn UnitPrice
			[-] multitag "Unit Price"
				[ ] "#4"
			[-] HtmlText UP1
				[ ] multitag "#1"
			[-] HtmlText UP2
				[ ] multitag "#2"
			[-] HtmlText UP3
				[ ] multitag "#3"
			[-] HtmlText UP4
				[ ] multitag "#4"
			[-] HtmlText UP5
				[ ] multitag "#5"
			[-] HtmlText UP6
				[ ] multitag "#6"
		[-] HtmlColumn TotalPrice
			[+] multitag "Total Price"
				[ ] "#5"
			[-] HtmlText TP1
				[ ] multitag "#1"
			[-] HtmlText TP2
				[ ] multitag "#2"
			[-] HtmlText TP3
				[ ] multitag "#3"
			[-] HtmlText TP4
				[ ] multitag "#4"
			[-] HtmlText TP5
				[ ] multitag "#5"
			[-] HtmlText TP6
				[ ] multitag "#6"
			[-] HtmlText TP7
				[ ] multitag "#7"
			[-] HtmlText TP8
				[ ] multitag "#8"
			[-] HtmlText TP9
				[ ] multitag "#9"
			[-] HtmlText TP10
				[ ] multitag "#10"
	[+] HtmlPushButton ProceedWithOrder
		[+] multitag "Proceed With Order"
			[ ] "#1"
			[ ] "$bSubmit"
			[ ] "&name='bSubmit'"
	[+] HtmlHidden QTY_TENTS
		[+] multitag "#6"
			[ ] "$QTY_TENTS"
			[ ] "&name='QTY_TENTS'"
	[+] HtmlHidden QTY_BACKPACKS
		[+] multitag "#2"
			[ ] "$QTY_BACKPACKS"
			[ ] "&name='QTY_BACKPACKS'"
	[+] HtmlHidden QTY_GLASSES
		[+] multitag "#1"
			[ ] "$QTY_GLASSES"
			[ ] "&name='QTY_GLASSES'"
	[+] HtmlHidden QTY_SOCKS
		[+] multitag "#3"
			[ ] "$QTY_SOCKS"
			[ ] "&name='QTY_SOCKS'"
	[+] HtmlHidden QTY_BOOTS
		[+] multitag "#5"
			[ ] "$QTY_BOOTS"
			[ ] "&name='QTY_BOOTS'"
	[+] HtmlHidden QTY_SHORTS
		[+] multitag "#4"
			[ ] "$QTY_SHORTS"
			[ ] "&name='QTY_SHORTS'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "#2"
	[+] HtmlText Copyright1997SegueSoftwar1
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Oth*"
			[ ] "#4"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "#5"
	[+] HtmlText Copyright1997SegueSoftwar2
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved."
			[ ] "#3"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild BillingInformation
	[ ] tag "Billing Information"
	[ ] parent Browser
	[+] HtmlHeading BillingInformation
		[-] multitag "Billing Information"
			[ ] "#1"
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlText PleaseFillInThisFormYour
		[+] multitag "Please fill in this form. Your credit card will not be billed until your order is shipped."
			[ ] "#1"
	[+] HtmlText BillTo1
		[+] multitag "Bill To:"
			[ ] "#2"
	[+] HtmlText Name11
		[+] multitag "Name[1]"
			[ ] "#7"
	[+] HtmlTextField Name12
		[+] multitag "Name[1]"
			[ ] "#1"
			[ ] "$billName"
			[ ] "&name='billName'"
	[+] HtmlText Address11
		[+] multitag "Address[1]"
			[ ] "#9"
	[+] HtmlTextField Address12
		[+] multitag "Address[1]"
			[ ] "#3"
			[ ] "$billAddress"
			[ ] "&name='billAddress'"
	[+] HtmlText City11
		[+] multitag "City[1]"
			[ ] "#11"
	[+] HtmlTextField City12
		[+] multitag "City[1]"
			[ ] "#5"
			[ ] "$billCity"
			[ ] "&name='billCity'"
	[+] HtmlText State11
		[+] multitag "State[1]"
			[ ] "#13"
	[+] HtmlTextField State12
		[+] multitag "State[1]"
			[ ] "#7"
			[ ] "$billState"
			[ ] "&name='billState'"
	[+] HtmlText Zip11
		[+] multitag "Zip[1]"
			[ ] "#15"
	[+] HtmlTextField Zip12
		[+] multitag "Zip[1]"
			[ ] "#9"
			[ ] "$billZipCode"
			[ ] "&name='billZipCode'"
	[+] HtmlText Phone11
		[+] multitag "Phone[1]"
			[ ] "#17"
	[+] HtmlTextField Phone12
		[+] multitag "Phone[1]"
			[ ] "#11"
			[ ] "$billPhone"
			[ ] "&name='billPhone'"
	[+] HtmlText EMail1
		[+] multitag "E-mail"
			[ ] "#19"
	[+] HtmlTextField EMail2
		[+] multitag "E-mail"
			[ ] "#13"
			[ ] "$billEmail"
			[ ] "&name='billEmail'"
	[+] HtmlText CreditCard1
		[+] multitag "Credit Card"
			[ ] "#20"
	[-] HtmlPopupList CreditCard2
		[-] multitag "Credit Card"
			[ ] "#1"
			[ ] "$CardType"
			[ ] "&name='CardType'"
	[+] HtmlText CardNumber1
		[+] multitag "Card Number"
			[ ] "#21"
	[+] HtmlTextField CardNumber2
		[+] multitag "Card Number"
			[ ] "#14"
			[ ] "$CardNumber"
			[ ] "&name='CardNumber'"
	[+] HtmlText Expiration1
		[+] multitag "Expiration"
			[ ] "#22"
	[+] HtmlTextField Expiration2
		[+] multitag "Expiration"
			[ ] "#15"
			[ ] "$CardDate"
			[ ] "&name='CardDate'"
	[+] HtmlText ShipTo
		[+] multitag "Ship To:"
			[ ] "#3"
	[+] HtmlCheckBox SameAs
		[+] multitag "Same as"
			[ ] "#1"
			[ ] "$shipSameAsBill"
			[ ] "&name='shipSameAsBill'"
	[+] HtmlText N1
		[+] multitag "?[1]"
			[ ] "#4"
	[+] HtmlText BillTo2
		[+] multitag "Bill To"
			[ ] "#5"
	[+] HtmlText N2
		[+] multitag "?[2]"
			[ ] "#6"
	[+] HtmlText Name21
		[+] multitag "Name[2]"
			[ ] "#8"
	[+] HtmlTextField Name22
		[+] multitag "Name[2]"
			[ ] "#2"
			[ ] "$shipName"
			[ ] "&name='shipName'"
	[+] HtmlText Address21
		[+] multitag "Address[2]"
			[ ] "#10"
	[+] HtmlTextField Address22
		[+] multitag "Address[2]"
			[ ] "#4"
			[ ] "$shipAddress"
			[ ] "&name='shipAddress'"
	[+] HtmlText City21
		[+] multitag "City[2]"
			[ ] "#12"
	[+] HtmlTextField City22
		[+] multitag "City[2]"
			[ ] "#6"
			[ ] "$shipCity"
			[ ] "&name='shipCity'"
	[+] HtmlText State21
		[+] multitag "State[2]"
			[ ] "#14"
	[+] HtmlTextField State22
		[+] multitag "State[2]"
			[ ] "#8"
			[ ] "$shipState"
			[ ] "&name='shipState'"
	[+] HtmlText Zip21
		[+] multitag "Zip[2]"
			[ ] "#16"
	[+] HtmlTextField Zip22
		[+] multitag "Zip[2]"
			[ ] "#10"
			[ ] "$shipZipCode"
			[ ] "&name='shipZipCode'"
	[+] HtmlText Phone21
		[+] multitag "Phone[2]"
			[ ] "#18"
	[+] HtmlTextField Phone22
		[+] multitag "Phone[2]"
			[ ] "#12"
			[ ] "$shipPhone"
			[ ] "&name='shipPhone'"
	[+] HtmlPushButton PlaceTheOrder
		[+] multitag "Place The Order"
			[ ] "#1"
			[ ] "$bSubmit"
			[ ] "&name='bSubmit'"
	[+] HtmlHidden QTY_TENTS
		[+] multitag "#3"
			[ ] "$QTY_TENTS"
			[ ] "&name='QTY_TENTS'"
	[+] HtmlHidden QTY_BACKPACKS
		[+] multitag "#4"
			[ ] "$QTY_BACKPACKS"
			[ ] "&name='QTY_BACKPACKS'"
	[+] HtmlHidden QTY_GLASSES
		[+] multitag "#1"
			[ ] "$QTY_GLASSES"
			[ ] "&name='QTY_GLASSES'"
	[+] HtmlHidden QTY_SOCKS
		[+] multitag "#2"
			[ ] "$QTY_SOCKS"
			[ ] "&name='QTY_SOCKS'"
	[+] HtmlHidden QTY_BOOTS
		[+] multitag "#5"
			[ ] "$QTY_BOOTS"
			[ ] "&name='QTY_BOOTS'"
	[+] HtmlHidden QTY_SHORTS
		[+] multitag "#6"
			[ ] "$QTY_SHORTS"
			[ ] "&name='QTY_SHORTS'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "#23"
	[+] HtmlText Copyright1997SegueSoftwar1
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Oth*"
			[ ] "#24"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "#26"
	[+] HtmlText Copyright1997SegueSoftwar2
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved."
			[ ] "#25"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[+] window BrowserChild OnLineStoreReceipt
	[ ] tag "OnLine Store Receipt"
	[ ] parent Browser
	[+] HtmlImage Logo
		[+] multitag "Logo"
			[ ] "#1"
			[ ] "$http:??demo.borland.com?gmopost?images?gmo-animate.gif"
	[+] HtmlHeading OnLineStoreReceipt
		[-] multitag "OnLine Store Receipt"
			[ ] "#1"
	[+] HtmlHeading ThankYouForShoppingWithGr1
		[+] multitag "Thank you for shopping with Green Mountain Outpost"
			[ ] "#2"
	[+] HtmlTable ThankYouForShoppingWithGr2
		[+] multitag "Thank you for shopping with Green Mountain Outpost[1]"
			[ ] "#1"
		[+] HtmlColumn BillTo
			[+] multitag "Bill to:"
				[ ] "#1"
		[+] HtmlColumn Timo1
			[+] multitag "timo[1]"
				[ ] "#2"
			[+] HtmlText Address
				[+] multitag "address"
					[ ] "#1"
			[+] HtmlText SantaCruzSantaCruz33126
				[+] multitag "santa cruz, santa cruz 33126"
					[ ] "#2"
		[+] HtmlColumn HtmlColumn3
			[ ] tag "#3"
		[+] HtmlColumn ShipTo
			[+] multitag "Ship to:"
				[ ] "#4"
		[+] HtmlColumn Timo2
			[+] multitag "timo[2]"
				[ ] "#5"
			[+] HtmlText Address
				[+] multitag "address"
					[ ] "#1"
			[+] HtmlText SantaCruzSantaCruz33126
				[+] multitag "santa cruz, santa cruz 33126"
					[ ] "#2"
			[+] HtmlText N1231231234
				[+] multitag "123-123-1234"
					[ ] "#3"
	[-] HtmlTable ThankYouForShoppingWithGr3
		[+] multitag "Thank you for shopping with Green Mountain Outpost[2]"
			[ ] "#2"
		[-] HtmlColumn Qty
			[ ] multitag "#1"
			[-] HtmlText N1
				[ ] multitag "#1"
			[-] HtmlText N2
				[ ] multitag "#2"
			[-] HtmlText N3
				[ ] multitag "#3"
			[-] HtmlText N4
				[ ] multitag "#4"
			[-] HtmlText N5
				[ ] multitag "#5"
			[-] HtmlText N6
				[ ] multitag "#6"
			[-] HtmlText MethodOfPaymentAMEXYourCr
				[-] multitag "Method of Payment: AMEXYour credit card will not be charged until your order ships."
					[ ] "#7"
		[+] HtmlColumn ProductDescription
			[+] multitag "Product Description"
				[ ] "#2"
			[+] HtmlLink N3PersonDomeTent
				[+] multitag "3 Person Dome Tent"
					[ ] "#1"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#tents"
			[+] HtmlLink ExternalFrameBackpack
				[+] multitag "External Frame Backpack"
					[ ] "#2"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#backpacks"
			[+] HtmlLink GlacierSunGlasses
				[+] multitag "Glacier Sun Glasses"
					[ ] "#3"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#glasses"
			[+] HtmlLink PaddedSocks
				[+] multitag "Padded Socks"
					[ ] "#4"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#socks"
			[+] HtmlLink HikingBoots
				[+] multitag "Hiking Boots"
					[ ] "#5"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#boots"
			[+] HtmlLink BackCountryShorts
				[+] multitag "Back Country Shorts"
					[ ] "#6"
					[ ] "$http:??demo.borland.com?gmopost?products.htm#shorts"
		[+] HtmlColumn DeliveryStatus
			[+] multitag "Delivery Status"
				[ ] "#3"
			[+] HtmlText ToBeShipped1
				[+] multitag "To Be Shipped[1]"
					[ ] "#1"
			[+] HtmlText ToBeShipped2
				[+] multitag "To Be Shipped[2]"
					[ ] "#2"
			[+] HtmlText ToBeShipped3
				[+] multitag "To Be Shipped[3]"
					[ ] "#3"
			[+] HtmlText ToBeShipped4
				[+] multitag "To Be Shipped[4]"
					[ ] "#4"
			[+] HtmlText ToBeShipped5
				[+] multitag "To Be Shipped[5]"
					[ ] "#5"
			[+] HtmlText ToBeShipped6
				[+] multitag "To Be Shipped[6]"
					[ ] "#6"
			[+] HtmlText ProductTotal
				[+] multitag "Product Total"
					[ ] "#7"
			[+] HtmlText SalesTax
				[+] multitag "Sales Tax"
					[ ] "#8"
			[+] HtmlText ShippingHandling
				[+] multitag "Shipping  Handling"
					[ ] "#9"
			[+] HtmlText GrandTotal
				[+] multitag "Grand Total"
					[ ] "#10"
		[+] HtmlColumn UnitPrice
			[+] multitag "Unit Price"
				[ ] "#4"
			[+] HtmlText N29999
				[+] multitag "? 299.99"
					[ ] "#1"
			[+] HtmlText N17995
				[+] multitag "? 179.95"
					[ ] "#2"
			[+] HtmlText N6799
				[+] multitag "? 67.99"
					[ ] "#3"
			[+] HtmlText N1999
				[+] multitag "? 19.99"
					[ ] "#4"
			[+] HtmlText N10990
				[+] multitag "? 109.90"
					[ ] "#5"
			[+] HtmlText N2495
				[+] multitag "? 24.95"
					[ ] "#6"
		[-] HtmlColumn TotalPrice
			[ ] multitag "#5"
			[-] HtmlText TP1
				[ ] multitag "#1"
			[-] HtmlText TP2
				[ ] multitag "#2"
			[-] HtmlText TP3
				[ ] multitag "#3"
			[-] HtmlText TP4
				[ ] multitag "#4"
			[-] HtmlText TP5
				[ ] multitag "#5"
			[-] HtmlText TP6
				[ ] multitag "#6"
			[-] HtmlText TP7
				[ ] multitag "#7"
			[-] HtmlText TP8
				[ ] multitag "#8"
			[-] HtmlText TP9
				[ ] multitag "#9"
			[-] HtmlText TP10
				[ ] multitag "#10"
	[+] HtmlPushButton ReturnToHomePage
		[+] multitag "Return to Home Page"
			[ ] "#1"
			[ ] "$bSubmit"
			[ ] "&name='bSubmit'"
	[+] HtmlText ThisIsASampleWebSiteFor
		[+] multitag "This is a sample web site for demonstration purposes only!No products will be sent to you."
			[ ] "#1"
	[+] HtmlText Copyright1997SegueSoftwar1
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved.Information in this document is subject to change without notice.Oth*"
			[ ] "#3"
	[+] HtmlText InformationInThisDocumentI
		[+] multitag "Information in this document is subject to change without notice.Other products and companies referred to herein are trademark*"
			[ ] "#4"
	[+] HtmlText Copyright1997SegueSoftwar2
		[+] multitag "Copyright ¨ 1997 Segue Software, Inc. All rights reserved."
			[ ] "#2"
	[+] HtmlLink PleaseLetUsKnowHowYouLik
		[+] multitag "Please let us know how you like our site."
			[ ] "#1"
			[ ] "$mailto:gmo-master@segue.com"
[ ] 
[ ] 
[-] appstate catalogState () basedon DefaultBaseState
	[ ] GMO.SetActive ()
	[ ] GMO.BeSureToVisitUsOnTheInt2.HtmlColumn1.EnterGMOOnLine.Click ()
	[ ] 
[-] appstate billingInformationState () basedon catalogState
	[ ] OnLineCatalog.SetActive()
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N10990.SetText("10")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N17995.SetText("5")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.SetText("2")
	[ ] OnLineCatalog.PlaceAnOrder.Click()
	[ ] PlaceOrder.SetActive()
	[ ] PlaceOrder.ProceedWithOrder.Click()
	[ ] BillingInformation.Name12.SetText("timoteo ponce")
	[ ] BillingInformation.Address12.SetText("Buenos Aires 1")
	[ ] BillingInformation.City12.SetText("Santa Cruz")
	[ ] BillingInformation.State12.SetText("Santa Cruz")
	[ ] BillingInformation.Zip12.SetText("33126")
	[ ] BillingInformation.Phone12.setText("333-123-4444")
	[ ] BillingInformation.EMail2.SetText("timo@mail.ch")
	[ ] BillingInformation.CardNumber2.SetText("3333-123456-55555")
	[ ] BillingInformation.Expiration2.SetText("11/15")
[ ] 
