﻿[+] testcase browseTestPage() 
	[ ] GMO.SetActive()
	[ ] GMO.BeSureToVisitUsOnTheInt3.HtmlColumn1.BrowserTestPage.Click()
	[ ] Window browsePage = BrowserTestPage
	[+] if browsePage.exists()  == true
		[ ] print( "all good" )
	[+] else
		[ ] logerror( "something went wrong" )
	[ ] 
[ ] 
[ ] // Descripcion: Probar el reset button > omitido
[ ] 
[ ] // Descripcion: Ingresa a la pagina de catalogo e intenta colocar una cantidad 
[ ] // negativa en los campos de entrada.
[ ] // Resultado esperado: la pagina no deberia cambiar y el foco deberia estar en el campo invalido
[ ] // Tipo: Falla grave, ya que la siguiente pagina asume valores positivos de entrada
[+] testcase onlineCatalogValidationOnNegativeQuantity() appstate catalogState
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N10990.SetText("10")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N17995.SetText("5")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.SetText("-1")
	[ ] OnLineCatalog.PlaceAnOrder.Click()
	[ ] 
	[-] if OnLineCatalog.exists() && OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.HasFocus()
		[ ] print("The redirection was not performed, all gut ")
	[-] else
		[ ] RaiseError (1 , "Negative values aren't allowed") 
[ ] 
[ ] // Descripcion: Ingresa a la pagina de catalogo e intenta colocar un valor no numerico
[ ] // en los campos de entrada.
[ ] // Resultado esperado: la pagina no deberia cambiar y el foco deberia estar en el campo invalido
[ ] // Tipo: Falla aceptable, ya que el calculo del total en la siguiente pagina queda en cero
[+] testcase onlineCatalogValidationOnNonNumericValues() appstate catalogState
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N10990.SetText("10")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N17995.SetText("5")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.SetText("a")
	[ ] OnLineCatalog.PlaceAnOrder.Click()
	[ ] 
	[-] if OnLineCatalog.exists() && OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.HasFocus()
		[ ] print("The redirection was not performed, all gut ")
	[-] else
		[ ] logerror ( "Non numeric values aren't allowed") 
[ ] 
[ ] // Descripcion: Ingresa a la pagina de catalogo e intenta colocar un valor no numerico
[ ] // en los campos de entrada.
[ ] // Resultado esperado: la pagina no deberia cambiar y el foco deberia estar en el campo invalido
[ ] // Tipo: Falla aceptable, ya que el calculo del total en la siguiente pagina queda en cero
[+] testcase placeOrderCostCalculation() appstate catalogState
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N10990.SetText("10")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N17995.SetText("5")
	[ ] OnLineCatalog.TheseAreTheItemsCurrently2.OrderQuantity.N29999.SetText("2")
	[ ] OnLineCatalog.PlaceAnOrder.Click()
	[ ] PlaceOrder.SetActive()
	[ ] 
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.Qty.N1.GetText() , "2")
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.Qty.N2.GetText() , "5")
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.Qty.N3.GetText() , "10")
	[ ] 
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.UnitPrice.UP1.GetText() , "$ 299.99" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.UnitPrice.UP2.GetText() , "$ 179.95" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.UnitPrice.UP3.GetText() , "$ 109.90" )
	[ ] 
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP1.GetText() , "$ 599.98" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP2.GetText() , "$ 899.75" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP3.GetText() , "$ 1099.00" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP4.GetText() , "$ 2598.73" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP5.GetText() , "$ 129.94" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP6.GetText() , "$ 5.00" )
	[ ] verify( PlaceOrder.YouHaveSelectedTheFollowin2.TotalPrice.TP7.GetText() , "$ 2733.67" )
