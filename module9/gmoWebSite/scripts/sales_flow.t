﻿[ ] // Descripcion: recorre el ciclo de ventas completo.
[ ] // Resultado esperado: terminar el ciclo sin problemas
[ ] // Tipo: Critica
[+] testcase completeSalesFlow() appstate billingInformationState
	[ ] BillingInformation.SameAs.Check()
	[ ] BillingInformation.PlaceTheOrder.Click()
	[ ] OnLineStoreReceipt.SetActive()
	[ ] verify( OnLineStoreReceipt.exists(), true ) 
	[ ] assertReceiptValues()
	[ ] 
[+] void assertReceiptValues()
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP1.GetText(), "$ 599.98")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP2.GetText(), "$ 899.75")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP3.GetText(), "$ 1099.00")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP4.GetText(), "$ 2598.73")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP5.GetText(), "$ 129.94")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP6.GetText(), "$ 5.00")
	[ ] verify( OnLineStoreReceipt.ThankYouForShoppingWithGr3.TotalPrice.TP7.GetText(), "$ 2733.67")
[ ] 
[ ] // Descripcion: Prueba la validacion de zip-code validos. 
[ ] // Resultado esperado: validar los formatos y valores ingresados, no permitiendo 
[ ] // proseguir a la siguiente etapa 
[ ] // Tipo: falla grave
[+] testcase zipCodeValidation() appstate billingInformationState
	[ ] BillingInformation.Zip12.SetText("333")
	[ ] BillingInformation.SameAs.Check()
	[ ] BillingInformation.PlaceTheOrder.Click()
	[ ] verify( BillingInformation.exists(), true ) 
	[ ] 
	[ ] 
[ ] // Descripcion: verifica que el check de copia de campos funciona correctamente
[ ] // Resultado esperado: los campos deben ser copiados con su valor actual
[ ] // Tipo: falla aceptable
[+] testcase copyFieldsValidation() appstate billingInformationState
	[ ] BillingInformation.SameAs.Check()
	[ ] 
	[ ] verify( BillingInformation.Name12.GetText(), BillingInformation.Name22.GetText() ) 
	[ ] verify( BillingInformation.Address12.GetText(), BillingInformation.Address22.GetText() ) 
	[ ] verify( BillingInformation.City12.GetText(), BillingInformation.City22.GetText() ) 
	[ ] verify( BillingInformation.State12.GetText(), BillingInformation.State22.GetText() ) 
	[ ] verify( BillingInformation.Zip12.GetText(), BillingInformation.Zip22.GetText() ) 
	[ ] verify( BillingInformation.Phone12.GetText(), BillingInformation.Phone22.GetText() ) 
[ ] 
[ ] 
