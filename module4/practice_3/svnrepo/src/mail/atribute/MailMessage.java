/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.atribute;

import java.io.PrintStream;
import mail.atribute.adjunto.AttachFiles;

/**
 *
 * @author Roberto
 */
public class MailMessage {
    // The text of the message.
    private Message message;
    private AttachFiles attachs;

    public MailMessage(String message) {
        this.message = new Message(message);
        attachs = new AttachFiles();
    }
    
    public void attachFile(String file) {
        attachs.attachAFile(file);
    }
    /**
     * Print this mail message to the text terminal.
     */
    public void print(){
        message.print();
        attachs.print();
    }

    public AttachFiles getAttachs() {
        return attachs;
    }

    public Message getMessage() {
        return message;
    }
    
    
}
