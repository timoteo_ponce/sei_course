package mail.atribute;

import java.util.Date;
import mail.account.UserAccount;
import mail.account.UserAccounts;

/**
 * A class to model a simple mail item. The item has sender and recipient
 * addresses and a message string.
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class MailItem {
    // The sender and intended recipient of the item.

    private MailFromTo fromTo;
    // The text of the message.
    private MailMessage message;
    private String subject;
    
    private Date date;
    private boolean readed;

    public boolean isReaded()
    {
        return readed;
    }
    public void markAsReaded()
    {
        readed = true;
    }
    public void setDate(Date date)
    {
        this.date = date;
    }
    public Date getDate()
    {
        return date;
    }
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MailMessage getMessage() {
        return message;
    }
    public UserAccount getTo()
    {
        return fromTo.getTo();
    }
    public UserAccount getFrom()
    {
        return fromTo.getFrom();
    }
    
    public void setFrom(UserAccount from) {
        fromTo.setFrom(from);
    }
    
    public void setTo(UserAccount to) {
        fromTo.setTo(to);
    }

    /**
     * Create a mail item from sender to the given recipient,
     * containing the given message.
     * @param from The sender of this item.
     * @param to The intended recipient of this item.
     * @param message The text of the message to be sent.
     */
    public MailItem(String from, String to, String message) {
        fromTo = new MailFromTo(new UserAccount(from),
                new UserAccount(to));
        this.message = new MailMessage(message);
        readed = false;
    }

    public boolean equalsTo(UserAccount count) {
        return fromTo.equalsTo(count);
    }

    public boolean existAccounts(UserAccounts accountsList) {
        return fromTo.existAccounts(accountsList);
    }

    public MailFromTo getFromTo() {
        return fromTo;
    }



    /**
     * Print this mail message to the text terminal.
     */
    public void print() {
        fromTo.print();
        message.print();
    }


    public void copyFrom(MailItem newItem) {
        this.fromTo = newItem.fromTo;
        this.message = newItem.message;
        this.subject = newItem.subject;
    }
}
