/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.atribute;

/**
 *
 * @author timoteo
 */
public class NullMailItem extends MailItem {

    public static final NullMailItem NULL_ITEM = new NullMailItem();

    private NullMailItem() {
        super("", "", "");
    }
}
