package mail.account;

/**
 *
 * @author Roberto
 */
public class UserAccount {
    private String user;

    public UserAccount(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return user;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserAccount other = (UserAccount) obj;        
        if (this.user == null) {
            return false;
        }
        String theOtherUser = other.user;        
        if (!user.equals(theOtherUser)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        if (user != null) {
            hash = 59 * hash + user.hashCode();
            return hash;
        }
        hash = 59 * hash;
        return hash;
    }
}
