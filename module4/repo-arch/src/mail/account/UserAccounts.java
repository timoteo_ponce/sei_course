/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mail.ui.ListaUser;

/**
 *
 * @author Roberto
 */
public class UserAccounts {
    private  List<UserAccount> accounts;
    ListaUser listaUser = new ListaUser();

    public UserAccounts(String ruta) {
        accounts = listaUser.cargarListaDelArchivo(ruta);
    }
    
    public void addAccount(String user) {
        
        UserAccount userAccount = new UserAccount(user);
        if (existsAccount(userAccount))
            return;
        
        accounts.add(userAccount);
    }
    
    public  boolean existsAccount(UserAccount userAccount) {
        int count = 0;
        Iterator<UserAccount> accountIterator = accounts.iterator();        
        while (accountIterator.hasNext() && count == 0) {
            UserAccount account = accountIterator.next();            
            count = account.equals(userAccount) ? count + 1 : 0;
        }        
        if (count > 0)
            return true;
        return false;
    }
    
}
