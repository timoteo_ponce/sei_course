/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import mail.account.UserAccount;
import mail.atribute.MailItem;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Roberto
 */
public class Mails {

    //private List<MailItem> items;
    private int position;

    public Mails() 
    {
        //items = new ArrayList<MailItem>();
        position = 0;
    }

    public MailItem getNextMail(UserAccount count, String path) 
    {
        if (position >= howManyMailItems(count, path))
            return null;
        
        try
        {
            File directory = new File(path + "\\" + count.toString());
            File []files = directory.listFiles();
            
            BufferedReader output = new BufferedReader(new FileReader(files[position]));
            
            String message = "";
            String line = output.readLine();
            
            while (line != null)
            {
                message += line;    
                line = output.readLine();
            }
            output.close();
            
            String info[] = message.split("[$]");
            
            MailItem item = new MailItem(info[1], count.toString(), info[4]);
            
            String fecha[] = info[2].split("[/]");
            
            Date dt = new Date(Integer.parseInt(fecha[0]),Integer.parseInt(fecha[1]),Integer.parseInt(fecha[2]),Integer.parseInt(fecha[3]),Integer.parseInt(fecha[4]),Integer.parseInt(fecha[5]));
            
            item.setDate(dt);
            item.setSubject(info[3]);
            if (info[0].equals("1"))
                item.markAsReaded();
            
            position++;
            return item;
        }
        catch (IOException exception)
        {
            return null;
        }
        
        /*
        Iterator<MailItem> iteratorItems = items.iterator();
        MailItem item = null;
        while (iteratorItems.hasNext() && item == null) {
            item = verifyAndRemove(count, iteratorItems);

        }
        return item;
        */
    }
    /*
    private MailItem verifyAndRemove(UserAccount count, Iterator<MailItem> iteratorItems) 
    {
        MailItem item = iteratorItems.next();

        if (item.equalsTo(count)) 
        {
            iteratorItems.remove();
            return item;
        }
        return null;
    }
     */
    public void remove(UserAccount account, MailItem item, String directory) 
    {
        File file = new File(directory + "\\" + item.getTo().toString() + "\\" + item.getDate().getTime()+"\\.txt");
        file.delete();
    }

    /**
     * Add the given mail item to the message list.
     * @param item The mail item to be stored on the server.
     */
    public void post(MailItem item, String directory) {
        try {
            
            File dir = new File(directory + "\\" + item.getTo().toString());
            if (!dir.exists())
                dir.mkdir();
            
            Writer output = null;
            String text = item.getMessage().getMessage().toString();

            File file = new File(directory + "\\" + item.getTo().toString() + "\\" + item.getDate().getTime()+".txt");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            output.write("0");
            output.write("$");
            output.write(item.getFrom().toString());
            output.write("$");
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd/hh/mm/ss");
            output.write(df.format(item.getDate()));
            //output.write(item.getDate().getTime()+"");
            output.write("$");
            output.write(item.getSubject());            
            output.write("$");
            output.write(text);
            output.close();
        } catch (IOException exception) 
        {
        }
        //items.add(item);
    }

    public int howManyMailItems(UserAccount who, String directory) 
    {
        File file = new File(directory + "\\" + who.toString());
        if (!file.exists() || !file.isDirectory())
            return -1;
        
        return file.listFiles().length;
        
        
    }

}
