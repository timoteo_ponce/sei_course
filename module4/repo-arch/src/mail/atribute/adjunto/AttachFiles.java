/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.atribute.adjunto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Roberto
 */
public class AttachFiles {
    private  List<AttachFile> files;

    public AttachFiles() {
        files = new ArrayList<AttachFile>();
    }
    
    public void attachAFile(String fileName) {
        
        AttachFile attach = new AttachFile(fileName);
        if (existsAttach(attach))
            return;
        
        files.add(attach);
    }
    
    public  boolean existsAttach(AttachFile attach) {
        int count = 0;
        Iterator<AttachFile> attachIterator = files.iterator();        
        while (attachIterator.hasNext() && count != 0) {
            AttachFile file = attachIterator.next();            
            count = file.equals(attach) ? count + 1 : 0;
        }        
        if (count > 0)
            return true;
        return false;
    }
    
    public void print(){
        for (AttachFile attach : files) {
            attach.print();
        }
    }
    
    public int size(){
        return files.size();
    }
    
    public AttachFile getAttachFile(int index){
        return files.get(index);
    }
    public  List<AttachFile> getListFile(){
        return this.files;
    } 
}