/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import mail.atribute.MailItem;
import mail.atribute.NullMailItem;
import mail.ui.MailIterator;

/**
 *
 * @author timoteo
 */
public class MailIteratorMock implements MailIterator {

    private final List<MailItem> itemList = new ArrayList<MailItem>();
    private int currentIndex = 0;

    @Override
    public MailItem getCurrent() {
        init();
        return getItem(currentIndex);
    }

    @Override
    public void goNext() {
        init();
        increaseIterator();
    }

    @Override
    public void goPrevious() {
        init();
        decreaseIterator();
    }

    private void init() {
        if (itemList.isEmpty()) {
            for (int i = 0; i < 10; i++) {
                MailItem mailItem = new MailItem("userFrom-" + i, "userTo-" + i, "message-" + i);
                mailItem.setDate(new Date());
                mailItem.setSubject("subject-"+i);
                itemList.add(mailItem);
            }
        }
    }

    private void increaseIterator() {
        if (currentIndex < (itemList.size() - 1)) {
            currentIndex++;
        }        
    }

    private void decreaseIterator() {
        if (currentIndex > 0) {
            currentIndex--;
        }
    }

    private MailItem getItem(int currentIndex) {
        if (itemList.isEmpty()) {
            return NullMailItem.NULL_ITEM;
        }
        return itemList.get(currentIndex);
    }

    @Override
    public String toString() {
        return "MailIteratorMock{" + "itemList=" + itemList + ", currentIndex=" + currentIndex + '}';
    }
    
    
    public static void main(String args[]){
        MailIteratorMock mail = new MailIteratorMock();        
        mail.init();
        mail.getItem(1).print();
        mail.getItem(2).print();
      
    }
    
}
