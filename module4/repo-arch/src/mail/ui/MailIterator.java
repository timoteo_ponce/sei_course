/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import mail.atribute.MailItem;

/**
 *
 * @author timoteo
 */
public interface MailIterator {

    MailItem getCurrent();

    void goNext();

    void goPrevious();
//
//    void refresh();
}
