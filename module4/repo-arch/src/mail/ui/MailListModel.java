/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import mail.atribute.MailItem;
import mail.atribute.NullMailItem;
import mail.client.MailClient;

/**
 *
 * @author Roberto
 */
public class MailListModel extends AbstractTableModel implements MailIterator {

    private String columnNames[] = {"No.", "Subject", "Time", "Status"};
    private MailClient client;
    private int currentMail;
    private List<MailItem> mails;

    public MailListModel(MailClient client) {
        this.client = client;
        mails = client.recuperarTodosLosCorreos();                
        if (mails.isEmpty()) {
            currentMail = -1;
            return;
        }
        currentMail = 0;
    }

    @Override
    public int getRowCount() {
        return mails.isEmpty() ? 0 : mails.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return (Object) String.valueOf(rowIndex + 1);
        }
        MailItem mail = mails.get(rowIndex);
        switch (columnIndex) {
            case 1:
                return mail.getSubject();
            case 2: 
                return mail.getDate();
            case 3:
                return mail.isReaded()? "R" : "U";
        }
        return null;
    }

    @Override
    public MailItem getCurrent() {
        return mails.isEmpty() ? NullMailItem.NULL_ITEM : mails.get(currentMail);
    }

    @Override
    public void goNext() {
        if (currentMail < mails.size() - 1) {
            currentMail++;
        }
    }

    @Override
    public void goPrevious() {
        if (currentMail > 0) {
            currentMail--;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    public void deleteAllMails() {
        client.deleteAllMails();
        mails = new ArrayList<MailItem>();
        currentMail = -1;
    }
    
    public MailClient getMailClient()
    {
      return client;
    }
    
}
