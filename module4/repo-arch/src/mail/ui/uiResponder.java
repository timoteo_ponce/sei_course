/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * uiResponder.java
 *
 * Created on 19-11-2011, 03:13:32 PM
 */
package mail.ui;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mail.atribute.MailItem;
import mail.client.MailClient;
import mail.server.MailServer;

/**
 *
 * @author Otro
 */
public class uiResponder extends javax.swing.JFrame implements MailSendForm{
    /** Creates new form uiResponder */
    private MailItem mailItem;
    private MailClient cliente=null;
    public uiResponder() {
        try {
            initComponents();
            setLocationRelativeTo(null);
            cliente=new MailClient(MailServer.CreateServer("D:/Diplomado Modulo 5/svnDavor/src/mail/mail.config"), "Alex");
            mailItem=new MailItem("from", "to", "message");
            mailItem.setDate(new Date());
            //mailViewComponent1.setModo(mailViewComponent1.Modo_WRITE);
            mailViewComponent1.setMailItem(mailItem,mailViewComponent1.Modo_WRITE);
        } catch (Exception ex) {
            Logger.getLogger(uiResponder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public uiResponder(MailItem mailItem_, MailClient cliente_) {
        initComponents();
        cliente=cliente_;
        mailItem=mailItem_;
            //mailViewComponent1.setModo(mailViewComponent1.Modo_WRITE);
            mailViewComponent1.setMailItem(mailItem,mailViewComponent1.Modo_WRITE);
    }        

    public MailItem getMailItem() {
        return mailItem;
    }

    public void setMailItem(MailItem mailItem) {
        this.mailItem = mailItem;
            //mailViewComponent1.setModo(mailViewComponent1.Modo_WRITE);
            mailViewComponent1.setMailItem(mailItem,mailViewComponent1.Modo_WRITE);
        //mailViewer1.setSender(mailItem.getFrom().toString());
        //mailViewer1.setReceiver(mailItem.getTo().toString());
       // mailViewer1.
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBtEnviar = new javax.swing.JButton();
        jBtCancelar = new javax.swing.JButton();
        mailViewComponent1 = new mail.ui.MailViewComponent();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jBtEnviar.setText("Enviar");
        jBtEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtEnviarActionPerformed(evt);
            }
        });

        jBtCancelar.setText("Cancelar");
        jBtCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addComponent(jBtEnviar)
                        .addGap(144, 144, 144)
                        .addComponent(jBtCancelar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(mailViewComponent1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mailViewComponent1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtEnviar)
                    .addComponent(jBtCancelar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtEnviarActionPerformed

        // TODO add your handling code here:
      /*  mailItem.setSubject(mailViewComponent1.getSubjectText().getText());
        String date=mailViewComponent1.getSentDateText().getText();
        Date d=new Date(date);
        mailItem.setDate(d);
        mailItem=new MailItem(date, date, date)
        */
        mailViewComponent1.updateData();
        mailItem.print(); 
        send();
        
        
    }//GEN-LAST:event_jBtEnviarActionPerformed

    private void jBtCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtCancelarActionPerformed
        // TODO add your handling code here:
        cancel();
    }//GEN-LAST:event_jBtCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new uiResponder().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtCancelar;
    private javax.swing.JButton jBtEnviar;
    private mail.ui.MailViewComponent mailViewComponent1;
    // End of variables declaration//GEN-END:variables

    @Override
    public boolean send() {
        if(validateForm()){
            cliente.sendMailItem(mailItem);  
            System.out.println("mensaje enviado");
            return true;
        }
            System.out.println("no enviado");
        
        //throw new UnsupportedOperationException("Not supported yet.");
        return false;
    }

    @Override
    public boolean validateForm() {
        if(cliente!=null && mailItem!=null){
            if(!mailItem.getFrom().toString().equals("")){
                if(!mailItem.getTo().toString().equals("")){
                    if(!mailItem.getDate().toString().equals("")){
                        return true;
                    }                 
                    System.out.println("fecha vacia");
                }
                System.out.println("to vacio");
            }
            System.out.println("from vacio");
        }
        System.out.println("nulls");
        // throw new UnsupportedOperationException("Not supported yet.");
        return false;
    }

    @Override
    public boolean cancel() {
        return JOptionPane.showConfirmDialog(this, "¿Desea cancelar el mensaje?", "", JOptionPane.OK_OPTION,JOptionPane.OK_CANCEL_OPTION) == 0;
    }
}
