/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.general;

import java.util.logging.Level;

/**
 * Esta clase permite regitrar los mensajes de Información, Advertencia, mensajes Criticos que se desea reportar en cada clase en la bitacora.
 * 
 * Para poder usarlo:
 * Simplemente debe agregar en el encabezado de cada clase la siguiente Instancia:
 * 
 * private static final Logger LOG = Bitacora.getLogger();
 * 
 * y usar en los métodos de la siguiente forma
 * 
 * LOG.info("depurar...");
 * 
 * que registrar la bitacora segun el nivel de  seleccionado 
 * tal el caso de LOG.info("depurar...");
 * donde info corresponde al Nivel INFO que son sucesos esperados.
 * 
 * INFO: debug...
 * 
 * @author milca 
 * 
 */

public class Bitacora {
  
                    
     public static Level level = Level.ALL;
     
     public static java.util.logging.Logger getLogger() {
        final Throwable t = new Throwable();
        final StackTraceElement methodCaller = t.getStackTrace()[1];
        final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(methodCaller.getClassName());
        logger.setLevel(level);

        return logger;
    }
    
}
