/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import junit.framework.TestCase;
import mail.atribute.MailItem;

/**
 *
 * @author timoteo
 */
public class MailItemTest extends TestCase{
    
    public void testEqualsOnSame(){
        MailItem item1 = new MailItem("FROM", "TO", "message", "subject");
        MailItem item2 = new MailItem("FROM", "TO", "message", "subject");
        MailItem item3 = new MailItem("FROM2", "TO", "message", "subject");
        assertEquals(item1, item2);
        assertNotSame(item1, item3);
    }
    
    public void testEqualsOnNotSame(){
        MailItem item1 = new MailItem("FROM", "TO", "message", "subject");
        MailItem item2 = new MailItem("FROM2", "TO", "message", "subject");
        assertNotSame(item1, item2);
    }
    
}
