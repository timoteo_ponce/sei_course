/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.util.List;
import junit.framework.TestCase;
import mail.atribute.MailItem;

/**
 *
 * @author timoteo
 */
public class MailServerTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MailsTest.cleanup();
    }

    public void testPost() {
        MailsTest.cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "hello", "subject");        
        server.post(item);
        assertEquals(1, server.howManyMailItems(MailsTest.receiverAccount));
        //add a new one
        MailItem item2 = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "hello2", "subject2");
        server.post(item2);
        assertEquals(2, server.howManyMailItems(MailsTest.receiverAccount));
        MailsTest.cleanup();
    }

    public void testExistAccountInServer() {
        MailServer server = createMailServer();
        assertTrue(server.existsAccountInServer(MailsTest.TEST_RECEIVER));
        assertFalse(server.existsAccountInServer("trololo"));
    }

    public void testGetNextMail() {
        MailsTest.cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "Subject4","hello4");
        server.post(item);
        MailItem nextMailItem = server.getNextMailItem(MailsTest.receiverAccount);
        assertNotNull(nextMailItem);
        MailsTest.cleanup();
    }

    public void testFetchMailsFrom() {
        MailsTest.cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "subject4","hello4");
        MailItem item2 = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "subject4","hello4");
        server.post(item);        
        server.post(item2);
        List<MailItem> mails = server.fetchMailsFromUser(MailsTest.receiverAccount);
        assertEquals(2, mails.size());
        MailsTest.cleanup();
    }

    private MailServer createMailServer() {
        MailServer server = MailServer.CreateServer(MailsTest.getUserHome());
        server.addAccountsServer(MailsTest.TEST_RECEIVER);
        server.addAccountsServer(MailsTest.TEST_SENDER);
        return server;
    }
}
