/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.io.File;
import junit.framework.TestCase;
import mail.account.UserAccount;
import mail.atribute.MailItem;

/**
 *
 * @author timoteo
 */
public class MailsTest extends TestCase {

    public static final String TEST_RECEIVER = "test_receiver";
    public static final String TEST_SENDER = "test_sender";
    public static UserAccount receiverAccount = new UserAccount(TEST_RECEIVER);

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cleanup();
    }

    public void testPost() {
        MailsTest.cleanup();
        Mails mails = new Mails();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        mails.post(item, getUserHome());
        assertEquals(mails.getNextMail(receiverAccount, getUserHome()), item);
        assertNull(mails.getNextMail(receiverAccount, getUserHome()));
        cleanup();
    }

    public void testRemove() {
        MailsTest.cleanup();
        Mails mails = new Mails();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        mails.post(item, getUserHome());
        assertEquals(mails.howManyMailItems(receiverAccount, getUserHome()), 1);
        mails.remove(receiverAccount, item, getUserHome());
        assertEquals(mails.howManyMailItems(receiverAccount, getUserHome()), 0);
        cleanup();
    }

    public void testHowManyItems() {
        MailsTest.cleanup();
        Mails mails = new Mails();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");        
        mails.post(item, getUserHome());
        assertEquals(mails.howManyMailItems(receiverAccount, getUserHome()), 1);
        
        MailItem item2 = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject2", "hello2");
        mails.post(item2, getUserHome());
        assertEquals(mails.howManyMailItems(receiverAccount, getUserHome()), 2);
        cleanup();
    }

    public static String getUserHome() {
        return System.getProperty("user.home") + "/myconf";
    }

    public static void cleanup() {
        File userFolder = new File(getUserHome() + "/" + TEST_RECEIVER + "/");
        if (userFolder.exists()) {
            for (File file : userFolder.listFiles()) {
                file.delete();
            }
        }
    }
}
