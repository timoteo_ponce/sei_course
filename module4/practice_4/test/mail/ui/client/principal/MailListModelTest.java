/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui.client.principal;

import junit.framework.TestCase;
import mail.atribute.MailItem;
import mail.atribute.NullMailItem;
import mail.client.MailClient;
import mail.server.MailServer;
import mail.server.MailsTest;
import mail.ui.MailIterator;
import mail.ui.MailListModel;

/**
 *
 * @author timoteo
 */
public class MailListModelTest extends TestCase {

    MailListModel mailListModel;
    MailServer server = MailServer.CreateServer(MailsTest.getUserHome());
    MailItem mail1;
    MailItem mail2;
    MailItem mail3;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MailClient client = new MailClient(server, MailsTest.TEST_SENDER);
        MailsTest.cleanup();
        mail1 = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "helloit1", "testSubject");

        client.sendMailItem(mail1);
        mail2 = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "helloit2", "testSubject");
        client.sendMailItem(mail2);
        mail3 = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "helloit3", "testSubject");
        client.sendMailItem(mail3);
    }

    public void testGetCurrentAtTheBeginning() throws Exception {
        MailIterator iterator = new MailListModel(new MailClient(server, MailsTest.TEST_RECEIVER));
        assertNotSame(iterator.getCurrent(), NullMailItem.NULL_ITEM);
    }

    public void testGetAfterANext() throws Exception {
        MailListModel iterator = new MailListModel(new MailClient(server, MailsTest.TEST_RECEIVER));
        iterator.refresh();
        iterator.goNext();
        System.out.println(iterator.getCurrent().getMessage().getMessage());
//        assertEquals(iterator.getCurrent(), mail3);
    }
}
