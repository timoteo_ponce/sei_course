/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import javax.swing.JFrame;
import junit.framework.TestCase;
import mail.atribute.MailItem;
import mail.client.MailClient;
import mail.server.MailServer;
import mail.server.MailsTest;

/**
 *
 * @author julio
 */
public class UINewViewTest extends TestCase {

    public void testValidateFormOnInvalidValues() {
        UINewView uINewView = new UINewView(new JFrame(), false, null);
        assertFalse(uINewView.validateForm());
    }

    public void testValidateFormOnValidValues() {
        UINewView uINewView = new UINewView(new JFrame(), false, null);
        MailItem item = new MailItem("julio", "julio", "Important","Hi men!!. $$");
        uINewView.setItem(item);
        assertTrue(uINewView.validateForm());
    }

    public void testSendMail() throws Exception {
        MailClientMock client = new MailClientMock();
        UINewView uINewView = new UINewView(new JFrame(), false, client);
        //fill
        MailItem item = new MailItem("julio", "julio", "Important","Hi men!!. $$");
        uINewView.setItem(item);
        assertTrue(uINewView.validateForm());
        client.sendMailItem(item);
        assertTrue(true);
    }

    private static class MailClientMock extends MailClient {
        private boolean mailSent;

        public MailClientMock() throws Exception {
            super(MailServer.CreateServer(MailsTest.getUserHome()), MailsTest.TEST_SENDER);
        }

        @Override
        public void sendMailItem(MailItem item) {
            this.mailSent = true;
        }
        
        
    }
}
