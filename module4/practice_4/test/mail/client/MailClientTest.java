/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.client;

import junit.framework.TestCase;
import mail.atribute.MailItem;
import mail.server.MailServer;
import mail.server.MailsTest;

/**
 *
 * @author PORTATIL
 */
public class MailClientTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        MailsTest.cleanup();
    }

    public void testPost() throws Exception {
        MailItem item = new MailItem(MailsTest.TEST_SENDER, MailsTest.TEST_RECEIVER, "hello", "subject");
        MailServer server = MailServer.CreateServer(MailsTest.getUserHome());
        MailClient cliente = new MailClient(server, MailsTest.TEST_RECEIVER);
        cliente.sendMailItem(item);
        MailItem nextMail = cliente.getNextMailItem();
        assertEquals(nextMail, item);
        assertNull(cliente.getNextMailItem());
        MailsTest.cleanup();
    }
}
