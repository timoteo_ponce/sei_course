/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import java.io.*;
import mail.account.*;
import java.util.*;

/**
 *
 * @author Ernesto Soto Roca
 */
public class ListaUser {

    public Boolean userExists(String user, String rutaArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(rutaArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            //linea = br.readLine();
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
                if (linea.equals(user)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public List<UserAccount> cargarListaDelArchivo(String rutaArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        List<UserAccount> ListAccountAux = new ArrayList<UserAccount>();

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(rutaArchivo);
            if (archivo.exists()) {
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);
            } else {
                InputStreamReader inputStreamReader = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream("mail.config"));
                br = new BufferedReader(inputStreamReader);
            }


            // Lectura del fichero
            String linea;
            linea = br.readLine();
            while ((linea = br.readLine()) != null) {
                UserAccount ObjUserA = new UserAccount(linea);
                ListAccountAux.add(ObjUserA);
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return ListAccountAux;

    }
}
