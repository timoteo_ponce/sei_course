/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

/**
 *
 * @author Administrador
 */
public interface MailSendForm {

    boolean send();

    boolean validateForm();

    boolean cancel();
}
