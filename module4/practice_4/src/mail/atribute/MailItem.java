package mail.atribute;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import mail.account.UserAccount;
import mail.account.UserAccounts;

/**
 * A class to model a simple mail item. The item has sender and recipient
 * addresses and a message string.
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class MailItem {
    // The sender and intended recipient of the item.

    private MailFromTo fromTo;
    // The text of the message.
    private MailMessage message;
    private String subject;
    private Date date;
    private boolean read;

    public MailItem(String from, String to, String subject, String message) {
        fromTo = new MailFromTo(new UserAccount(from),
                new UserAccount(to));
        this.message = new MailMessage(message);
        this.subject = subject;
    }

    public boolean isRead() {
        return read;
    }

    public void markAsRead() {
        read = true;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MailMessage getMessage() {
        return message;
    }

    public UserAccount getTo() {
        return fromTo.getTo();
    }

    public UserAccount getFrom() {
        return fromTo.getFrom();
    }

    public void setFrom(UserAccount from) {
        fromTo.setFrom(from);
    }

    public void setTo(UserAccount to) {
        fromTo.setTo(to);
    }

    public boolean equalsTo(UserAccount count) {
        return fromTo.equalsTo(count);
    }

    public boolean existAccounts(UserAccounts accountsList) {
        return fromTo.existAccounts(accountsList);
    }

    public MailFromTo getFromTo() {
        return fromTo;
    }

    /**
     * Print this mail message to the text terminal.
     */
    public void print() {
        fromTo.print();
        message.print();
    }

    public void copyFrom(MailItem newItem) {
        this.fromTo = newItem.fromTo;
        this.message = newItem.message;
        this.subject = newItem.subject;
        this.date = newItem.date;
        this.read = newItem.read;
    }

    @Override
    public boolean equals(Object obj) {
        final MailItem other = (MailItem) obj;
        if (this.fromTo != other.fromTo && (this.fromTo == null || !this.fromTo.equals(other.fromTo))) {
            return false;
        }
        if (this.message != other.message && (this.message == null || !this.message.equals(other.message))) {
            return false;
        }
        if ((this.subject == null) ? (other.subject != null) : !this.subject.equals(other.subject)) {
            return false;
        }
//        String dateString1 = dateFormat.format(date);
//        String dateString2 = dateFormat.format(other.date);        
//        return dateString1.equals(dateString2);
        return true;
    }
}
