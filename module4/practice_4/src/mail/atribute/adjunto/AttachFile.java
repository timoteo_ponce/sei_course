/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.atribute.adjunto;

import java.io.PrintStream;

/**
 *
 * @author Roberto
 */
public class AttachFile {
    private String nameFile;

    public AttachFile(String nameFile) {
        this.nameFile = nameFile;
    }

    @Override
    public String toString() {
        return nameFile ;
    }
    
    public void print() {
        PrintStream outFile  = System.out;
        outFile.println("Attach: " + nameFile);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachFile other = (AttachFile) obj;        
        if (nameFile == null) {
            return false;
        }
        String theOtherFile = other.nameFile;        
        if (!nameFile.equals(theOtherFile)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        
        int hash = 5;
        if (nameFile != null) {
            hash = 11 * hash + nameFile.hashCode();
            return hash;
        }
        hash = 11 * hash;
        return hash;
    }
    
    
    
}
