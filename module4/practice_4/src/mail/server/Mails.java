/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mail.account.UserAccount;
import mail.atribute.MailItem;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Roberto
 */
public class Mails {

    DateFormat df = new SimpleDateFormat("yyyy/MM/dd/hh/mm/ss/SSSS");
    private int position;

    public Mails() {
        position = 0;
    }

    public MailItem getNextMail(UserAccount account, String path) {
        int itemsCount = howManyMailItems(account, path);
        if (position >= itemsCount) {
            return null;
        }

        try {
            File directory = new File(path + "/" + account.toString());
            File[] files = directory.listFiles();

            MailItem item = parseItem(account, files[position]);
            position++;
            return item;
        } catch (Exception exception) {
            return null;
        }
    }

    public void remove(UserAccount account, MailItem item, String directory) {
        File file = new File(directory + "/" + item.getTo().toString() + "/");
        for (File tempItem : file.listFiles()) {
            try {
                MailItem mailItem = parseItem(account, tempItem);
                if (item.equals(mailItem)) {
                    tempItem.delete();
                }
            } catch (Exception ex) {
                Logger.getLogger(Mails.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Add the given mail item to the message list.
     * @param item The mail item to be stored on the server.
     */
    public void post(MailItem item, String directory) {
        Writer output = null;
        File file = null;
        try {
            File dir = new File(directory + "/" + item.getTo().toString());
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String text = item.getMessage().getMessage().toString();
            Date sentDate = new Date();
            file = new File(directory + "/" + item.getTo().toString() + "/" + (sentDate.getTime()) + ".txt");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            output.write("0");
            output.write("$");
            output.write(item.getFrom().toString());
            output.write("$");
            output.write(df.format(sentDate));
            output.write("$");
            output.write(item.getSubject());
            output.write("$");
            output.write(text);
            output.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(Mails.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int howManyMailItems(UserAccount who, String directory) {
        File file = new File(directory + "/" + who.toString());
        if (!file.exists() || !file.isDirectory()) {
            return -1;
        }
        return file.listFiles().length;

    }

    private MailItem parseItem(UserAccount account, File file) throws ParseException, IOException {
        BufferedReader output = new BufferedReader(new FileReader(file));
        String message = "";
        String line = output.readLine();

        while (line != null) {
            message += line;
            line = output.readLine();
        }
        output.close();

        String info[] = message.split("[$]");
        MailItem item = new MailItem(info[1], account.toString(), info[3], info[4]);
        Date dt = df.parse(info[2]);
        item.setDate(dt);
        if (info[0].equals("1")) {
            item.markAsRead();
        }
        return item;
    }
}
