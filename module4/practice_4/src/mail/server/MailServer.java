package mail.server;

import mail.account.UserAccount;
import mail.account.UserAccounts;
import mail.atribute.MailItem;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple model of a mail server. The server is able to receive
 * mail items for storage, and deliver them to clients on demand.
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class MailServer {
    // Storage for the arbitrary number of mail items to be stored
    // on the server.    

    private Mails mails;
    private String localDirectory;
    private UserAccounts serverAccountsList;

    public static MailServer CreateServer(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            return null;
        }
        return new MailServer(path);
    }

    /**
     * Construct a mail server.
     */
    private MailServer(String directory) {
        this.localDirectory = directory;
        mails = new Mails();
    }

    /**
     * Return how many mail items are waiting for a user.
     * @param who The user to check for.
     * @return How many items are waiting.
     */
    public int howManyMailItems(UserAccount who) {
        return mails.howManyMailItems(who, localDirectory);
    }

    /**
     * Return the next mail item for a user or null if there
     * are none.
     * @param who The user requesting their next item.
     * @return The user's next item.
     */
    public MailItem getNextMailItem(UserAccount who) {
        return mails.getNextMail(who, localDirectory);
    }

    /**
     * Add the given mail item to the message list.
     * @param item The mail item to be stored on the server.
     */
    public void post(MailItem item) {
        if (item.existAccounts(serverAccountsList)) {
            mails.post(item, localDirectory);
        }
    }

    public void addAccountsServer(String user) {
        serverAccountsList = new UserAccounts(localDirectory + "/mail.config");
        serverAccountsList.addAccount(user);
    }

    public boolean existsAccountInServer(String user) {
        serverAccountsList = new UserAccounts(localDirectory + "/mail.config");
        return serverAccountsList.existsAccount(new UserAccount(user));
    }

    public List<MailItem> fetchMailsFromUser(UserAccount user) {
        List lt = new ArrayList();
        MailItem item = mails.getNextMail(user, localDirectory);
        while (item != null) {
            lt.add(item);
            item = mails.getNextMail(user, localDirectory);
        }
        return lt;
    }

    public void deleteMail(UserAccount user, MailItem item) {
        mails.remove(user, item, localDirectory);
    }

    public void deleteAllMailFromUser(UserAccount user) {
        List lt = new ArrayList();
        MailItem item = mails.getNextMail(user, localDirectory);
        while (item != null) {
            lt.add(item);
            item = mails.getNextMail(user, localDirectory);
            mails.remove(user, item, localDirectory);
        }

    }
}