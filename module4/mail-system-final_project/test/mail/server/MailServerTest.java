/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.util.List;
import mail.MailTest;
import mail.attribute.MailItem;

/**
 *
 * @author timoteo
 */
public class MailServerTest extends MailTest {


    public void testPost() throws InterruptedException {
        cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "hello", "subject");        
        server.post(item);
        assertEquals(1, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        //add a new one
        Thread.sleep(500);
        MailItem item2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "hello2", "subject2");
        server.post(item2);
        assertEquals(2, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        cleanup();
    }

    public void testExistAccountInServer() {
        MailServer server = createMailServer();
        assertTrue(server.existsAccountInServer(PersistentMailBoxTest.TEST_RECEIVER));
        assertFalse(server.existsAccountInServer("trololo"));
    }

    public void testGetNextMail() {
        cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "Subject4","hello4");
        server.post(item);
        MailItem nextMailItem = server.getNextMailItem(PersistentMailBoxTest.receiverAccount);
        assertNotNull(nextMailItem);
        cleanup();
    }

    public void testFetchMailsFrom() {
        cleanup();
        MailServer server = createMailServer();
        MailItem item = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject4","hello4");
        server.post(item);        
        List<MailItem> mails = server.fetchMailsFromUser(PersistentMailBoxTest.receiverAccount);
        assertEquals(1, mails.size());
        cleanup();
    }

    private MailServer createMailServer() {
        MailServer server = MailServer.createServer();
        server.addAccountsServer(PersistentMailBoxTest.TEST_RECEIVER);
        server.addAccountsServer(PersistentMailBoxTest.TEST_SENDER);
        return server;
    }
    
    public void testHowManyMailItems() throws InterruptedException {
        cleanup();
        MailServer server = createMailServer();
        MailItem item1 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject5", "hello5");
        server.post(item1);
        assertEquals(1, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        Thread.sleep(500);
        MailItem item2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject6", "hello6");
        server.post(item2);
        assertEquals(2, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        cleanup();
    }
    
    public void testDeleteAllMailFromUser() throws InterruptedException {
        cleanup();
        MailServer server = createMailServer();
        MailItem item1 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject7", "hello7");
        server.post(item1);
        Thread.sleep(500);
        assertEquals(1, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        MailItem item2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject6", "hello6");
        server.post(item2);
        assertEquals(2, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        
        server.deleteAllMailFromUser(PersistentMailBoxTest.receiverAccount);
        assertEquals(0, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        cleanup();
    }
    
    public void testDeleteMail() throws InterruptedException {
        cleanup();
        MailServer server = createMailServer();
        MailItem item1 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject7", "hello7");
        server.post(item1);
        Thread.sleep(500);
        assertEquals(1, server.howManyMailItems(PersistentMailBoxTest.receiverAccount));
        MailItem item2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject6", "hello6");
        server.post(item2);
        Thread.sleep(500);
        int cantPrev = server.howManyMailItems(PersistentMailBoxTest.receiverAccount);
        server.deleteMail(PersistentMailBoxTest.receiverAccount, item1);
        Thread.sleep(500);
        int cantPost = server.howManyMailItems(PersistentMailBoxTest.receiverAccount);
        assertEquals(cantPrev, cantPost+1);
    }
    
}
