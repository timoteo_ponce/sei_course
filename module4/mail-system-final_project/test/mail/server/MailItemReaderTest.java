/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.io.File;
import mail.MailTest;
import mail.account.UserAccount;
import mail.attribute.MailItem;

/**
 *
 * @author Roger
 */
public class MailItemReaderTest extends MailTest {

    /**
     * Test of read method, of class MailItemReader.
     */
    public void testRead() throws Exception {
        cleanup();
        System.out.println("read");
        MailItemReader mailIteReader = new MailItemReader();
        MailServer server = MailServer.createServer();
        server.addAccountsServer(PersistentMailBoxTest.TEST_RECEIVER);
        server.addAccountsServer(PersistentMailBoxTest.TEST_SENDER);
        MailItem item1 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject1", "hola1");
        server.post(item1);
        MailItem item2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject2", "hola2");
        server.post(item2);
        UserAccount account = new UserAccount(PersistentMailBoxTest.TEST_RECEIVER);
        File directory = new File(MailServer.DEFAULT_FOLDER + "/" + account.toString() + "/");
        File[] files = directory.listFiles();
        MailItem expResult = mailIteReader.read(account, files[0]);
        assertTrue(expResult.isRead());
    }
}
