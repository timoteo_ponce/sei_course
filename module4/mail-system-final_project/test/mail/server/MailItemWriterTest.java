/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import mail.MailTest;
import mail.attribute.MailItem;

/**
 *
 * @author Roger
 */
public class MailItemWriterTest extends MailTest {

    /**
     * Test of save method, of class MailItemWriter.
     */
    public void testSave() {
        cleanup();
        PersistentMailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "subject4", "hello4");
        MailItemWriter writer = new MailItemWriter(MailServer.DEFAULT_FOLDER);
        writer.save(item);
        int count = mailBox.howManyMailItems(PersistentMailBoxTest.receiverAccount);
        assertEquals(1, count);
    }
}
