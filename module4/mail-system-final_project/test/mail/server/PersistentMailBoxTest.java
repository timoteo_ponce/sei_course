/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.util.ArrayList;
import java.util.List;
import mail.MailTest;
import mail.account.UserAccount;
import mail.attribute.MailItem;

/**
 *
 * @author timoteo
 */
public class PersistentMailBoxTest extends MailTest {

    public void testPost() {
        cleanup();
        MailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        mailBox.post(item);
        assertEquals(mailBox.getNextMail(receiverAccount), item);
        assertNull(mailBox.getNextMail(receiverAccount));
        cleanup();
    }

    public void testRemove() {
        cleanup();
        MailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        mailBox.post(item);
        assertEquals(mailBox.howManyMailItems(receiverAccount), 1);
        mailBox.remove(receiverAccount, item);
        assertEquals(mailBox.howManyMailItems(receiverAccount), 0);
        cleanup();
    }

    public void testHowManyItems() {
        cleanup();
        MailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");        
        mailBox.post(item);
        assertEquals(mailBox.howManyMailItems(receiverAccount), 1);
        
        MailItem item2 = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject2", "hello2");
        mailBox.post(item2);
        assertEquals(mailBox.howManyMailItems(receiverAccount), 2);
        cleanup();
    }

    /**
     * Test of getNextMail method, of class PersistentMailBox.
     */
    public void testGetNextMail() {
        cleanup();        
        UserAccount account = new UserAccount(TEST_RECEIVER);
        PersistentMailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        mailBox.post(item);
        MailItem result = mailBox.getNextMail(account);        
        assertEquals(item, result);
        cleanup();
    }

    /**
     * Test of getAllMails method, of class PersistentMailBox.
     */
    public void testGetAllMails() throws InterruptedException {
        cleanup();
        System.out.println("");
        System.out.println("");
        System.out.println("getAllMails");
        UserAccount account = new UserAccount(TEST_RECEIVER);
        PersistentMailBox mailBox = new PersistentMailBox();
        MailItem item = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject", "hello");
        MailItem item2 = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject2", "hello2");
        MailItem item3 = new MailItem(TEST_SENDER, TEST_RECEIVER, "subject3", "hello3");
        mailBox.post(item);
        Thread.sleep(1000);
        mailBox.post(item2);        
        Thread.sleep(1000);
        mailBox.post(item3);        
        Thread.sleep(1000);
        List<MailItem> mailsListResult = new ArrayList<MailItem>();
        mailsListResult.add(item);
        mailsListResult.add(item2);
        mailsListResult.add(item3);
        List result = mailBox.getAllMails(account);
        cleanup();
        assertEquals(3, result.size());
    }

}
