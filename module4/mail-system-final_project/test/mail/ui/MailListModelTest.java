/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import javax.swing.JTable;
import junit.framework.TestCase;
import mail.MailTest;
import mail.attribute.MailItem;
import mail.attribute.NullMailItem;
import mail.client.MailClient;
import mail.server.MailServer;
import mail.server.PersistentMailBoxTest;

/**
 *
 * @author timoteo
 */
public class MailListModelTest extends MailTest {

    MailListModel mailListModel;
    MailServer server = MailServer.createServer();
    MailItem mail1;
    MailItem mail2;
    MailItem mail3;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MailClient client = new MailClient(server, PersistentMailBoxTest.TEST_SENDER);
        mail1 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "helloit1", "testSubject");

        client.sendMailItem(mail1);
        mail2 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "helloit2", "testSubject");
        client.sendMailItem(mail2);
        mail3 = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "helloit3", "testSubject");
        client.sendMailItem(mail3);
    }

    public void testGetCurrentAtTheBeginning() throws Exception {
        JTable table = new JTable();
        MailIterator iterator = new MailListModel(table, new MailClient(server, PersistentMailBoxTest.TEST_RECEIVER));
        assertNotSame(iterator.getCurrent(), NullMailItem.NULL_ITEM);
    }

    public void testGetAfterANext() throws Exception {
        JTable table = new JTable();
        MailListModel iterator = new MailListModel(table, new MailClient(server, PersistentMailBoxTest.TEST_RECEIVER));
        iterator.refresh();
        iterator.goNext();
    }
}
