/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import junit.framework.TestCase;

/**
 *
 * @author Marines L S
 */
public class MessageTest extends TestCase {
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of equals method, of class Message.
     */
    public void testEquals() {
        System.out.println("equals");
        Object o = new Message("my message");
        Message message = new Message("my message");
        boolean result = message.equals(o);
        System.out.println(result);
        assertEquals(true, result);
        
        message = new Message("other message");
        result = message.equals(o);
        System.out.println(result);
        assertEquals(false, result);
    }
}
