/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute.attach;

import java.util.Arrays;
import junit.framework.TestCase;
import mail.general.FileUtils;

/**
 *
 * @author timoteo
 */
public class AttachmentTest extends TestCase {
    
    public void testEquals() {
        String fileName = FileUtils.getResource("mail.config").getPath();
        System.out.println(fileName);
        Attachment attachment = new Attachment(fileName);
        Object attachment2 = new Attachment(fileName);
        boolean result = attachment.equals(attachment2);
        assertEquals(true,result);
        
        attachment2 = new Attachment("hola");
        result = attachment.equals(attachment2);
        assertEquals(false,result);
        
    }

    public void testValidFileAttachment() {
        String fileName = FileUtils.getResource("mail.config").getPath();
        Attachment attachment = new Attachment(fileName);
        assertTrue(Arrays.equals(attachment.getData(), FileUtils.fileToByteArray(fileName)));
    }
    
}
