/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute.attach;

import java.util.List;
import junit.framework.TestCase;
import mail.general.FileUtils;

/**
 *
 * @author timoteo
 */
public class AttachmentsListTest extends TestCase {

    public void testAttachADuplicateFile() {
        String fileName = FileUtils.getResource("mail.config").getPath();
        Attachment attachment = new Attachment(fileName);
        AttachmentsList list = new AttachmentsList();
        list.attachAFile(fileName);
        list.attachAFile(fileName);//try twice to make it fail
        assertTrue(list.attachmentExists(attachment));
        assertEquals(list.size(), 1);
    }

    /**
     * Test of attachAFile method, of class AttachmentsList.
     */
    public void testAttachAFile() {
        System.out.println("attachAFile");
        String fileName = "uno";
        AttachmentsList instance = new AttachmentsList();
        instance.attachAFile(fileName);
        boolean result = instance.attachmentExists(new Attachment(fileName));
        assertEquals(true, result);
        result = instance.attachmentExists(new Attachment("other"));
        assertEquals(false, result);
    }

    /**
     * Test of attachmentExists method, of class AttachmentsList.
     */
    public void testAttachmentExists() {
        System.out.println("attachmentExists");
        Attachment attach = new Attachment("uno");
        AttachmentsList instance = new AttachmentsList();
        instance.attachAFile("uno");
        instance.attachAFile("dos");
        instance.attachAFile("tres");
        boolean result = instance.attachmentExists(attach);
        assertEquals(true, result);
        
        result = instance.attachmentExists(new Attachment("hola"));
        assertEquals(false, result);
    }

    /**
     * Test of size method, of class AttachmentsList.
     */
    public void testSize() {
        System.out.println("size");
        AttachmentsList instance = new AttachmentsList();
        int result = instance.size();
        assertEquals(0, result);
        
        instance.attachAFile("uno");
        instance.attachAFile("dos");
        instance.attachAFile("tres");
        result = instance.size();
        assertEquals(3, result);
    }

    /**
     * Test of getAttachFile method, of class AttachmentsList.
     */
    public void testGetAttachFile() {
        System.out.println("getAttachFile");
        int index = 1;
        AttachmentsList instance = new AttachmentsList();
        instance.attachAFile("uno");
        instance.attachAFile("dos");
        instance.attachAFile("tres");
        Attachment result = instance.getAttachFile(index);
        assertEquals(new Attachment("dos"), result);
    }
    
}
