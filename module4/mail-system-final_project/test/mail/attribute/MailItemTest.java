/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import java.util.Date;
import junit.framework.TestCase;
import mail.account.UserAccount;
import mail.account.UserAccountList;
import mail.server.PersistentMailBoxTest;

/**
 *
 * @author Marines L S
 */
public class MailItemTest extends TestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of equalsTo method, of class MailItem.
     */
    public void testEqualsTo() {
        System.out.println("equalsTo");
        UserAccount count = new UserAccount("myTo");
        MailItem mail = new MailItem("myFrom", "myTo", "subject", "hola a todos");
        boolean result = mail.equalsTo(count);
        System.out.println(result);
        assertEquals(true, result);
        
        mail.setTo(new UserAccount("otherTo"));
        result = mail.equalsTo(count);
        System.out.println(result);
        assertEquals(false, result);
    }

    /**
     * Test of accountsExist method, of class MailItem.
     */
    public void testAccountsExist() {
        System.out.println("accountsExist");
        UserAccountList accountsList = new UserAccountList();
        accountsList.addAccount("liseth");
        accountsList.addAccount("yenny");   
        MailItem mail = new MailItem("myFrom", "myTo", "subject", "hola a todos");
        boolean result = mail.accountsExist(accountsList);
        System.out.println(result);
        assertEquals(false, result);
        
        mail.setFrom(new UserAccount("yenny"));
        mail.setTo(new UserAccount("liseth"));
        result = mail.accountsExist(accountsList);
        System.out.println(result);
        assertEquals(true, result);
        
        mail.setFrom(new UserAccount("myFrom"));
        mail.setTo(new UserAccount("yenny"));
        mail.print();
        result = mail.accountsExist(accountsList);
        System.out.println(result);
        assertEquals(false, result);
    }
    
    /**
     * Test of copyFrom method, of class MailItem.
     */
    public void testCopyFrom() {
        System.out.println("copyFrom");
        MailItem newItem = new MailItem("myFrom", "myTo", "subject", "hola a todos");
        MailItem mail = new MailItem("otherFrom", "otherTo", "Subject2", "hola a todos jejeje");
        boolean result=mail.equals(newItem);
        System.out.println(result);
        
        mail.copyFrom(newItem);
        result=mail.equals(newItem);
        System.out.println(result);
        assertEquals(true, result);
    }

    /**
     * Test of equals method, of class MailItem.
     */
    public void testEquals() {
        System.out.println("equals");
        Object obj = new MailItem("myFrom", "myTo", "subject", "hola a todos");
        MailItem mail = new MailItem("myFrom", "myTo", "subject", "hola a todos");
        boolean result = mail.equals(obj);
        System.out.println(result);
        assertEquals(true, result);
        
        mail.setFrom(new UserAccount("otherFrom"));
        result = mail.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
    }
}
