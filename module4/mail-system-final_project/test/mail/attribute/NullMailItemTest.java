/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import junit.framework.TestCase;
import mail.account.UserAccount;

/**
 *
 * @author Marines L S
 */
public class NullMailItemTest extends TestCase {
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of equals method, of class NullMailItem.
     */
    public void testEquals() {
        System.out.println("equals");
        Object obj = NullMailItem.NULL_ITEM ;
        NullMailItem mailNull = NullMailItem.NULL_ITEM ;
        boolean result = mailNull.equals(obj);
        System.out.println(result);
        assertEquals(true, result);
        
        obj = null;
        result = mailNull.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
        
        obj = new MailFromTo(new UserAccount("from"), new UserAccount("to"));
        result = mailNull.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
    }
}
