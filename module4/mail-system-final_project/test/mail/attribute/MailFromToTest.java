/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import junit.framework.TestCase;
import mail.account.UserAccount;
import mail.account.UserAccountList;
import mail.server.PersistentMailBoxTest;

/**
 *
 * @author Marines L S
 */
public class MailFromToTest extends TestCase {
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of equalsTo method, of class MailFromTo.
     */
    public void testEqualsTo() {
        System.out.println("equalsTo");
        UserAccount count = new UserAccount("myto");
        MailFromTo fromTo = new MailFromTo(new UserAccount("myfrom"), new UserAccount("myto"));
        boolean result = fromTo.equalsTo(count);
        System.out.println(result);
        assertEquals(true, result);
        
        fromTo.setTo(new UserAccount("otherTo"));
        result = fromTo.equalsTo(count);
        System.out.println(result);
        assertEquals(false, result);
    }

    /**
     * Test of equalsFrom method, of class MailFromTo.
     */
    public void testEqualsFrom() {
        System.out.println("equalsFrom");
        UserAccount count = new UserAccount("myfrom");
        MailFromTo fromTo = new MailFromTo(new UserAccount("myfrom"), new UserAccount("myto"));
        boolean result = fromTo.equalsFrom(count);
        System.out.println(result);
        assertEquals(true, result);
        
        fromTo.setFrom(new UserAccount("otherFrom"));
        result = fromTo.equalsFrom(count);
        System.out.println(result);
        assertEquals(false, result);
    }

    /**
     * Test of accountExists method, of class MailFromTo.
     */
    public void testAccountExists() {
        System.out.println("accountExists");
        UserAccountList accountsList = new UserAccountList();
        accountsList.addAccount("liseth");
        accountsList.addAccount("yenny");        
        MailFromTo fromTo = new MailFromTo(new UserAccount("yenny"), new UserAccount("myto"));
        boolean result = fromTo.accountExists(accountsList);
        System.out.println(result);
        assertEquals(false, result);
        
        fromTo.setFrom(new UserAccount("myfrom"));
        fromTo.setTo(new UserAccount("liseth"));
        result = fromTo.accountExists(accountsList);
        System.out.println(result);
        assertEquals(false, result);
        
        fromTo.setFrom(new UserAccount("yenny"));
        fromTo.setTo(new UserAccount("liseth"));
        result = fromTo.accountExists(accountsList);
        System.out.println(result);
        assertEquals(true, result);
    }

    /**
     * Test of equals method, of class MailFromTo.
     */
    public void testEquals() {
        System.out.println("equals");
        Object obj = new MailFromTo(new UserAccount("myfrom"), new UserAccount("myto"));
        MailFromTo fromTo = new MailFromTo(new UserAccount("myfrom"), new UserAccount("myto"));
        boolean result = fromTo.equals(obj);
        System.out.println(result);
        assertEquals(true, result);
        
        fromTo.setFrom(new UserAccount("yenny"));
        result = fromTo.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
        
        fromTo.setFrom(new UserAccount("myfrom"));
        fromTo.setTo(new UserAccount("yenny"));
        result = fromTo.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
    }
}
