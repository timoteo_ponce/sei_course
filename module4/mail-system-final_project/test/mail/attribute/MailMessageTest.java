/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import junit.framework.TestCase;
import mail.attribute.attach.AttachmentsList;

/**
 *
 * @author Marines L S
 */
public class MailMessageTest extends TestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of equals method, of class MailMessage.
     */
    public void testEquals() {
        System.out.println("equals");
        Object obj = new MailMessage("hola todos");
        MailMessage mailMessage = new MailMessage("hola todos");
        boolean result = mailMessage.equals(obj);
        System.out.println(result);
        assertEquals(true, result);
        
        mailMessage.setContent("hola y chau");
        result = mailMessage.equals(obj);
        System.out.println(result);
        assertEquals(false, result);
    }
}
