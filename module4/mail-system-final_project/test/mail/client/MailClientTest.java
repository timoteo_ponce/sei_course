/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.client;

import junit.framework.TestCase;
import mail.MailTest;
import mail.attribute.MailItem;
import mail.server.MailServer;
import mail.server.PersistentMailBoxTest;

/**
 *
 * @author PORTATIL
 */
public class MailClientTest extends MailTest {

    public void testPost() throws Exception {
        MailItem item = new MailItem(PersistentMailBoxTest.TEST_SENDER, PersistentMailBoxTest.TEST_RECEIVER, "hello", "subject");
        MailServer server = MailServer.createServer();
        MailClient cliente = new MailClient(server, PersistentMailBoxTest.TEST_RECEIVER);
        cliente.sendMailItem(item);
        MailItem nextMail = cliente.getNextMailItem();
        assertEquals(nextMail, item);
        assertNull(cliente.getNextMailItem());
        cleanup();
    }
}
