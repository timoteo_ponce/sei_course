/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import junit.framework.TestCase;
import mail.server.PersistentMailBoxTest;

/**
 *
 * @author usuario
 */
public class UserAccountListTest extends TestCase {
    
    public UserAccountListTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of addAccount method, of class UserAccountList.
     */
    public void testAddAccount() {
        System.out.println("addAccount");
        String user = "mary";
        UserAccountList lista = new UserAccountList();
        lista.addAccount(user);
        boolean result=lista.contains(new UserAccount(user));
        System.out.println(result);
        assertEquals(true, result);
        
        result=lista.contains(new UserAccount("juanita"));
        System.out.println(result);
        assertEquals(false, result);
    }

    /**
     * Test of contains method, of class UserAccountList.
     */
    public void testContains() {
        System.out.println("contains");
        String user = "ana";
        UserAccountList list = new UserAccountList();
        list.addAccount("ana");
        list.addAccount("maria");
        list.addAccount("sole");
        boolean result=list.contains(new UserAccount(user));
        System.out.println(result);
        assertEquals(true, result);
      
        result=list.contains(new UserAccount("wily"));
        System.out.println(result);
        assertEquals(false, result);
    
      
    }
}
