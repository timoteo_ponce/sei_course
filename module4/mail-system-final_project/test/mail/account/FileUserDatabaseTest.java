/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import mail.server.*;
import junit.framework.TestCase;

/**
 *
 * @author admin
 * @author Timoteo Ponce
 */
public class FileUserDatabaseTest extends TestCase {

    MailBox mails = new PersistentMailBox();

    public void testUserExists() {
        String user = "angel";
        FileUserDatabase userDatabase = new FileUserDatabase("mail.config");
        Boolean exists = userDatabase.userExists(user);
        assertTrue(exists);
    }

    public void testAddUser() {
        String newUser = "guest2" + System.currentTimeMillis();
        FileUserDatabase userDatabase = new FileUserDatabase("mail.config");
        assertFalse(userDatabase.userExists(newUser));
        userDatabase.add(new UserAccount(newUser));
        assertTrue(userDatabase.userExists(newUser));
    }
}
