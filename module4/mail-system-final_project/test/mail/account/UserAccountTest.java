/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import junit.framework.TestCase;

/**
 *
 * @author timoteo
 */
public class UserAccountTest extends TestCase {
    
    /**
     * Test of toString method, of class UserAccount.
     */
    public void testEquals() {
        UserAccount account = new UserAccount("test");
        assertTrue(account.equals(new UserAccount("test")));
        assertFalse(account.equals(new UserAccount("test2")));
    }

}
