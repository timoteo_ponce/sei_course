/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import java.io.File;
import junit.framework.TestCase;
import mail.account.UserAccount;
import mail.account.UserAccountList;
import mail.attribute.NullMailItem;
import mail.server.MailServer;

/**
 *
 * @author timoteo
 */
public class MailTest extends TestCase {

    public static final String TEST_RECEIVER = "test_receiver";
    public static final String TEST_SENDER = "test_sender";
    public static UserAccount receiverAccount = new UserAccount(TEST_RECEIVER);

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cleanup();
        UserAccountList accountList = new UserAccountList();
        accountList.addAccount(TEST_SENDER);
        accountList.addAccount(TEST_RECEIVER);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        cleanup();
    }
    
    public void testMock(){
        assertEquals(NullMailItem.NULL_ITEM, NullMailItem.NULL_ITEM);
    }

    public void cleanup() {
        File userFolder = new File(MailServer.DEFAULT_FOLDER + "/" + TEST_RECEIVER + "/");
        if (userFolder.exists()) {
            for (File file : userFolder.listFiles()) {
                file.delete();
            }
        }
    }
}
