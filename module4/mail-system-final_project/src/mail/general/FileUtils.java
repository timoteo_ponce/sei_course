/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.general;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.logging.Logger;
import mail.account.UserAccount;

/**
 *
 * @author timoteo
 */
public class FileUtils {

    private static final Logger logger = Logger.getLogger(FileUtils.class.getName());
    
    public static URL getResource(String resource){
        return Thread.currentThread().getContextClassLoader().getResource(resource);
    }

    public static byte[] fileToByteArray(String fileName) {
        InputStream inputStream = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            inputStream = new FileInputStream(fileName);
            byte[] buffer = new byte[1024];
            while (inputStream.read(buffer) != -1) {
                outputStream.write(buffer);
            }
            outputStream.flush();
        } catch (IOException e) {
            logger.warning("Swallowed exception : " + e);
        } finally {
            close(inputStream);
        }
        return outputStream.toByteArray();
    }

    public static boolean fileExists(String fileLocation) {
        File file = new File(fileLocation);
        return file.exists();
    }

    public static void appendToFile(String fileLocation, String line) {
        FileWriter writer = null;
        try{
            writer = new FileWriter(new File(fileLocation),true);
            writer.append(line);
            writer.flush();
        }catch(Exception e){
            logger.warning("Swallowed exception : " + e);
        }finally{
            close(writer);
        }
    }

    public static void createFolder(String folder) {
        new File(folder).mkdir();
    }

    public interface FileBlock {

        void processLine(String line);
    }

    public static void readFile(String fileLocation, FileBlock block) {
        try {
            File file = new File(fileLocation);
            Reader reader = null;
            if (file.exists()) {
                reader = new FileReader(file);
            } else {
                reader = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream("mail.config"));
            }
            readFile(reader, block);
        } catch (IOException e) {
            logger.warning("Swallowed exception : " + e);
        }
    }

    public static void readFile(Reader reader, FileBlock block) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(reader);
            // Lectura del fichero
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                block.processLine(line);
            }
        } catch (IOException e) {
            logger.warning("Swallowed exception : " + e);
        } finally {
            close(bufferedReader);
            close(reader);
        }
    }

    public static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            logger.warning("Swallowed exception : " + e);
        }
    }
}
