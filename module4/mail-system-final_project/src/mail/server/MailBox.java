/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.util.List;
import mail.account.UserAccount;
import mail.attribute.MailItem;

/**
 *
 * @author timoteo
 */
public interface MailBox {

    MailItem getNextMail(UserAccount account);

    int howManyMailItems(UserAccount who);

    /**
     * Add the given mail item to the message list.
     * @param item The mail item to be stored on the server.
     */
    void post(MailItem item);

    void remove(UserAccount account, MailItem item);

    List<MailItem> getAllMails(UserAccount user);
}
