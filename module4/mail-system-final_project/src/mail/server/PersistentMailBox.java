/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;
 
import java.util.logging.Level;
import java.util.logging.Logger;
import mail.account.UserAccount;
import mail.attribute.MailItem;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roberto
 */
public class PersistentMailBox implements MailBox {
    private static final Logger logger = Logger.getLogger(PersistentMailBox.class.getName());
    static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd/hh/mm/ss/SSSS");
    private int position;
    private final String folderPath;
    private MailItemWriter itemWriter;
    private MailItemReader itemReader;

    public PersistentMailBox(String folderPath) {
        position = 0;
        this.folderPath = folderPath;
        this.itemWriter = new MailItemWriter(folderPath);
        this.itemReader = new MailItemReader();
    }

    public PersistentMailBox() {
        this(MailServer.DEFAULT_FOLDER);
    }

    @Override
    public List<MailItem> getAllMails(UserAccount account) {
        List<MailItem> mailsList = new ArrayList<MailItem>();
        try {
            File directory = new File(folderPath + "/" + account.toString());
            for (File file : directory.listFiles()) {                
                if (file.isFile()) {
                    mailsList.add(itemReader.read(account, file));                    
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PersistentMailBox.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mailsList;
    }

    @Override
    public MailItem getNextMail(UserAccount account) {
        int itemsCount = howManyMailItems(account);
        MailItem item = null;
        if (position < itemsCount) {
            try {
                File directory = new File(folderPath + "/" + account.toString());
                File[] files = directory.listFiles();
                item = itemReader.read(account, files[position]);
                position++;
            } catch (Exception ex) {
                Logger.getLogger(PersistentMailBox.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return item;
    }

    @Override
    public void remove(UserAccount account, MailItem item) {
        File fileToDelete = new File(folderPath + "/" + item.getTo().toString() + "/"+item.getDate().getTime()+".txt");        
        itemWriter.delete(fileToDelete);        
    }

    /**
     * Add the given mail item to the message list.
     * @param item The mail item to be stored on the server.
     */
    @Override
    public void post(MailItem item) {
        itemWriter.save(item);        
    }

    @Override
    public int howManyMailItems(UserAccount who) {
        File file = new File(folderPath + "/" + who.toString());
        if (!file.exists() || !file.isDirectory()) {
            return 0;
        }
        return file.listFiles().length;
    }

}
