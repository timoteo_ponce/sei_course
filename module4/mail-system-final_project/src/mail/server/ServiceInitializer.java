/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.server;

import java.io.File;
import java.io.IOException;
import mail.account.FileUserDatabase;
import mail.account.UserAccount;
import mail.account.UserAccountList;

/**
 * Initializes the service and its configuration files if there are not present.
 * @author timoteo
 */
public class ServiceInitializer {
    
    public static void init() throws IOException{
        File configFile = new File(FileUserDatabase.CONFIG_FILE);        
        if(!configFile.exists()){
            configFile.getParentFile().mkdirs();
            configFile.createNewFile();
        }
        UserAccountList accountList = new UserAccountList();
        UserAccount guest = new UserAccount("guest");
        if(!accountList.contains(guest)){
            accountList.addAccount(guest.toString());
        }
    }
}
