/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

/**
 *
 * @author timoteo
 */
public class NullMailItem extends MailItem {

    public static final NullMailItem NULL_ITEM = new NullMailItem();

    private NullMailItem() {
        super("", "", "", "");
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof NullMailItem);
    }
    
    
}
