/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import java.io.PrintStream;
import mail.account.UserAccount;
import mail.account.UserAccountList;

/**
 *
 * @author Roberto
 */
public class MailFromTo {
    // The sender of the item.

    private UserAccount from;
    // The intended recipient.
    private UserAccount to;

    public MailFromTo(UserAccount from, UserAccount to) {
        this.from = from;
        this.to = to;
    }

    /**
     * Print from and to attribute
     */
    public void print() {
        PrintStream outFile = System.out;
        outFile.println("From: " + from);
        outFile.println("To: " + to);
    }

    public UserAccount getTo() {
        return to;
    }

    public UserAccount getFrom() {
        return from;
    }

    public void setTo(UserAccount to) {
        this.to = to;
    }
    
    public void setFrom(UserAccount from) {
        this.from = from;
    }
    
    public boolean equalsTo(UserAccount count) {
        return to.equals(count);
    }

    public boolean equalsFrom(UserAccount count) {
        return from.equals(count);
    }

    public boolean accountExists(UserAccountList accountsList) {
        return ((accountsList.contains(from)) && (accountsList.contains(to)));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailFromTo other = (MailFromTo) obj;
        if (this.from != other.from && (this.from == null || !this.from.equals(other.from))) {
            return false;
        }
        return this.to.equals(other.to);
    }

    
   
}
