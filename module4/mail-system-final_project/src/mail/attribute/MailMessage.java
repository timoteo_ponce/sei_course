/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import mail.attribute.attach.AttachmentsList;

/**
 *
 * @author Roberto
 */
public class MailMessage {
    // The text of the message.

    private Message message;
    private AttachmentsList attachs;

    public MailMessage(String message) {
        this.message = new Message(message);
        attachs = new AttachmentsList();
    }

    public void attachFile(String file) {
        attachs.attachAFile(file);
    }

    /**
     * Print this mail message to the text terminal.
     */
    public void print() {
        message.print();
    }

    public AttachmentsList getAttachs() {
        return attachs;
    }

    public Message getMessage() {
        return message;
    }
    
    public void setContent(String content){
        this.message = new Message(content);
    }

    @Override
    public boolean equals(Object obj) {
        final MailMessage other = (MailMessage) obj;
        if (this.message != other.message && (this.message == null || !this.message.equals(other.message))) {
            return false;
        }
        return true;
    }
}
