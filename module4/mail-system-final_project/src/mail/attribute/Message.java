/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute;

import java.io.PrintStream;

/**
 *
 * @author Roberto
 */
public class Message 
{
    private String message;

    public Message(String message) 
    {
        this.message = message;
    }
    @Override
    public String toString()
    {
        return message;
    }
    
    public void print() 
    {
        PrintStream outFile  = System.out;
        outFile.println("Message: " + message);
    }

    @Override
    public boolean equals(Object o) {
        Message other = (Message) o;
        return other.message.equals(this.message);
    }
    
    
}
