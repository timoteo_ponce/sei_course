/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute.attach;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roberto
 * @author Timoteo Ponce - yet again, a lot of modifications
 */
public class AttachmentsList {

    private List<Attachment> attachments;

    public AttachmentsList() {
        attachments = new ArrayList<Attachment>();
    }

    public void attachAFile(String fileName) {
        Attachment attach = new Attachment(fileName);
        if (!attachmentExists(attach)) {
            attachments.add(attach);            
        }        
    }

    public boolean attachmentExists(Attachment attach) {
        return attachments.contains(attach);
    }

    public int size() {
        return attachments.size();
    }

    public Attachment getAttachFile(int index) {
        return attachments.get(index);
    }

    public List<Attachment> getAttachmentsList() {
        return this.attachments;
    }

    public void add(Attachment attachment) {
        this.attachments.add(attachment);
    }

    public void remove(Attachment attachment) {
        this.attachments.remove(attachment);
    }

}