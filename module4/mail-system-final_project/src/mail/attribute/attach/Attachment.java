/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.attribute.attach;

import java.io.File;
import java.util.Arrays;
import mail.general.FileUtils;

/**
 *
 * @author Roberto
 * @author Timoteo Ponce - corrections and several modifications
 */
public class Attachment {

    private final String fileName;
    private final byte[] data;

    public Attachment(String fileName) {
        this.fileName = new File(fileName).getName();
        this.data = FileUtils.fileToByteArray(fileName);
    }

    public Attachment(String fileName, byte[] contents) {
        this.fileName = fileName;
        this.data = contents;
    }

    public byte[] getData() {
        return data;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Attachment other = (Attachment) obj;
        if ((this.fileName == null) ? (other.fileName != null) : !this.fileName.equals(other.fileName)) {
            return false;
        }
        if (!Arrays.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public String toString() {
        return fileName;
    }
}
