package mail.attribute;

import java.util.Date;
import mail.account.UserAccount;
import mail.account.UserAccountList;

/**
 * A class to model a simple mail item. The item has sender and recipient
 * addresses and a message string.
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class MailItem {
    // The sender and intended recipient of the item.

    private MailFromTo fromTo;
    // The text of the message.
    private MailMessage message;
    private String subject;
    private Date date;
    private boolean read;

    public MailItem(String from, String to, String subject, String message) {
        this(new UserAccount(from),new UserAccount(to), subject, new MailMessage(message));
    }

    public MailItem(UserAccount from, String to, String subject, MailMessage message) {
        this(from, new UserAccount(to), subject, message);
    }

    public MailItem(UserAccount from, UserAccount to, String subject, MailMessage message) {
        fromTo = new MailFromTo(from, to);
        this.message = message;
        this.subject = subject;
        this.date = new Date(System.nanoTime());
    }

    public boolean isRead() {
        return read;
    }

    public void markAsRead() {
        read = true;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MailMessage getMessage() {
        return message;
    }

    public UserAccount getTo() {
        return fromTo.getTo();
    }

    public UserAccount getFrom() {
        return fromTo.getFrom();
    }

    public void setFrom(UserAccount from) {
        fromTo.setFrom(from);
    }

    public void setTo(UserAccount to) {
        fromTo.setTo(to);
    }

    public boolean equalsTo(UserAccount count) {
        return fromTo.equalsTo(count);
    }

    public boolean accountsExist(UserAccountList accountsList) {
        return fromTo.accountExists(accountsList);
    }

    public MailFromTo getFromTo() {
        return fromTo;
    }

    /**
     * Print this mail message to the text terminal.
     */
    public void print() {
        fromTo.print();
        message.print();
    }

    public void copyFrom(MailItem newItem) {
        this.fromTo = newItem.fromTo;
        this.message = newItem.message;
        this.subject = newItem.subject;
        this.date = newItem.date;
        this.read = newItem.read;
    }

    @Override
    public boolean equals(Object obj) {
        final MailItem other = (MailItem) obj;
        if (this.fromTo != other.fromTo && (this.fromTo == null || !this.fromTo.equals(other.fromTo))) {
            return false;
        }
        if (this.message != other.message && (this.message == null || !this.message.equals(other.message))) {
            return false;
        }
        return this.subject.equals(other.subject);
    }
}
