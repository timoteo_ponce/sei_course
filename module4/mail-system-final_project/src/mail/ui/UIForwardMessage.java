/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import java.awt.Frame;
import mail.attribute.MailItem;
import mail.client.MailClient;

/**
 *
 * @author timoteo
 */
public class UIForwardMessage extends UINewMessage {
    
    private MailItem item;
    private MailClient client;
    
    public UIForwardMessage(Frame parent, MailItem originalItem, MailClient client, MainScreen mainScreen) {
        super(parent, true, client, mainScreen);        
        this.client = client;
        init(originalItem);
    }
    
    private void init(MailItem originalItem) {        
        String subject = "FWD: " + originalItem.getSubject();
        item = new MailItem(client.getUserAccount(), "", subject, originalItem.getMessage());
        super.setItem(item);
    }

}
