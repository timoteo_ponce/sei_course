/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui;

import java.awt.Frame;
import mail.attribute.MailItem;
import mail.client.MailClient;

/**
 *
 * @author timoteo
 */
public class UIReplyMessage extends UINewMessage {

    private MailItem item;
    private MailClient client;
    
    
    public UIReplyMessage(Frame parent, MailItem originalItem, MailClient client, MainScreen mainScreen) {
        super(parent, true, client, mainScreen);
        this.client = client;
        init(originalItem);
    }

    private void init(MailItem originalItem) {
        String subject = "RE: " + originalItem.getSubject();
        item = new MailItem(client.getUserAccount(), originalItem.getFrom(), subject, originalItem.getMessage());
        super.setItem(item);
    }
}
