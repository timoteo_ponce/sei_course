/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.ui.event;

import mail.attribute.MailItem;

/**
 *
 * @author timoteo
 */
public interface MailItemEventListener extends java.util.EventListener{

    void itemChanged(MailItem item);
}
