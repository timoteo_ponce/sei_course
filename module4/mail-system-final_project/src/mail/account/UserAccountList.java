/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import java.util.List;
import mail.server.MailServer;

/**
 *
 * @author Roberto
 * @author Timoteo Ponce - a complete rewrite
 */
public class UserAccountList {

    private List<UserAccount> accounts;
    private UserDatabase userDatabase;

    public UserAccountList(String ruta) {
        userDatabase = new FileUserDatabase(ruta);
        accounts = userDatabase.getUserList();
    }

    public UserAccountList() {
        this(FileUserDatabase.CONFIG_FILE);
    }

    public void addAccount(String user) {
        UserAccount userAccount = new UserAccount(user);
        if (!contains(userAccount)) {
            accounts.add(userAccount);
            userDatabase.add(userAccount);
        }        
    }

    public boolean contains(UserAccount userAccount) {
        return accounts.contains(userAccount);
    }
}
