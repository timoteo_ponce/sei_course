/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.account;

import java.util.List;

/**
 *
 * @author timoteo
 */
public interface UserDatabase {

    List<UserAccount> getUserList();

    Boolean userExists(String user);

    void add(UserAccount userAccount);
    
}
