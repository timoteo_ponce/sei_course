/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import junit.framework.TestCase;
import mail.attribute.MailItem;

/**
 *
 * @author Marines L S
 */
public class MailDtoTest extends TestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * Test of getUpdatedMailItem method, of class MailDto.
     */
    public void testGetUpdatedMailItem() {
        System.out.println("getUpdatedMailItem");
        MailItem mailEnviado =new MailItem("mary", "roger", "hola", "hola que tal");
        MailDto instance = new MailDto(mailEnviado);
        instance.setSender("yenny");
        instance.setReceiver("mary");
        instance.setSubject("como estas");
        instance.setContent("hola como haz estado");
        MailItem expResult = new MailItem("yenny", "mary", "como estas", "hola como haz estado");
        MailItem result = instance.getUpdatedMailItem();
        assertEquals(expResult, result);
    }

    /**
     * Test of asReply method, of class MailDto.
     */
    public void testAsReply() {
        System.out.println("asReply");
        MailItem mailEnviado =new MailItem("mary", "roger", "hola", "hola que tal");
        MailDto instance = new MailDto(mailEnviado);
        MailDto instance2 = new MailDto(mailEnviado);
        instance2.setSender("roger");
        instance2.setReceiver("mary");
        instance2.setSubject("RE: hola");
        instance2.setContent("hola que tal");
        MailDto result = instance.asReply();
        boolean comparacion = false;
        if(result.getContent().equals(instance2.getContent())){
            if(result.getReceiver().equals(instance2.getReceiver())){
                if(result.getSender().equals(instance2.getSender())){
                    if(result.getSubject().equals(instance2.getSubject())){
                        comparacion = true;
                    }                
                }                
            }
        }
        assertEquals(true, comparacion);
    }

    /**
     * Test of asForward method, of class MailDto.
     */
    public void testAsForward() {
        System.out.println("asForward");
        MailItem mailEnviado =new MailItem("mary", "roger", "hola", "hola que tal");
        MailDto instance = new MailDto(mailEnviado);
        instance.setSender("roger");
        instance.setSubject("FWD: hola");
        MailDto result = instance.asForward();
        boolean comparacion = false;
        if(result.getSender().equals("roger")){
            if(result.getSubject().equals("FWD: hola")){
                comparacion = true;
            }                
        }  
        assertEquals(true, comparacion);
    }

}
