/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import mail.account.UserAccount;
import mail.attribute.MailItem;

/**
 *
 * @author Marines L S
 */
public class MailSessionTest extends TestCase {
    public static final String TEST_RECEIVER = "test_receiver";
    public static final String TEST_SENDER = "test_sender";
    public static UserAccount receiverAccount = new UserAccount(TEST_RECEIVER);
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cleanup();
    }

    public static void cleanup() {
        File userFolder = new File(System.getProperty("user.home") + "/myconf" + "/" + TEST_RECEIVER + "/");
        if (userFolder.exists()) {
            for (File file : userFolder.listFiles()) {
                file.delete();
            }
        }
    }
    
    /**
     * Test of getAllMails method, of class MailSession.
     */
    public void testGetAllMails() {
        try {
        cleanup();
            System.out.println("getAllMails");
            MailSession instance = new MailSession();
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello3", "hello"));
            
            List<MailItem> expResult = new ArrayList<MailItem>();
            expResult.add(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello"));
            expResult.add(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            expResult.add(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello3", "hello"));
            
            List result = instance.getAllMails();
            System.out.println("****result.size()"+result.size());
            assertEquals(3, result.size());
            assertEquals(expResult, result);
        } catch (Exception ex) {
            Logger.getLogger(MailSessionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        cleanup();
    }

    /**
     * Test of delete method, of class MailSession.
     */
    public void testDelete() {
        cleanup();
        try {
            System.out.println("delete");
            MailItem mailItem = new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello");
            MailSession instance = new MailSession();
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.delete(mailItem);
            boolean result=instance.getAllMails().contains(mailItem);
            int cant=instance.getAllMails().size();
            System.out.println("****cant"+cant +"  "+result );
            assertEquals(false, result);
            assertEquals(cant, 2);
        } catch (Exception ex) {
            Logger.getLogger(MailSessionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        cleanup();
    }

    /**
     * Test of deleteAll method, of class MailSession.
     */
    public void testDeleteAll() {
        cleanup();
        try {
            System.out.println("deleteAll");
            MailSession instance = new MailSession();
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.deleteAll();
            int cant=instance.getAllMails().size();
            System.out.println("****cant"+cant);
            assertEquals(instance.getAllMails().size(), 0);
        } catch (Exception ex) {
            Logger.getLogger(MailSessionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        cleanup();
    }

    /**
     * Test of send method, of class MailSession.
     */
    public void testSend() {
        cleanup();
        try {
            System.out.println("send");
            MailSession instance = new MailSession();
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello1", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            instance.send(new MailItem(TEST_SENDER, TEST_RECEIVER, "hello2", "hello"));
            int cant=instance.getAllMails().size();
            System.out.println("****cant"+cant);
            assertEquals(3, cant);
        } catch (Exception ex) {
            Logger.getLogger(MailSessionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        cleanup();
    }
}
