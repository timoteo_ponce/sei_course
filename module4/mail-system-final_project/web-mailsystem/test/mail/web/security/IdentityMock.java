/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import java.util.Collection;
import java.util.Set;
import org.jboss.seam.security.Authenticator;
import org.jboss.seam.security.Identity;
import org.picketlink.idm.api.Group;
import org.picketlink.idm.api.Role;
import org.picketlink.idm.api.User;

/**
 *
 * @author timoteo
 */
public class IdentityMock implements Identity {

    private String username;

    public IdentityMock(String username) {
        this.username = username;
    }

    @Override
    public boolean isLoggedIn() {
        return username != null;
    }

    @Override
    public boolean isVerified() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean tryLogin() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public User getUser() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void checkRestriction(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String login() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void quietLogin() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void logout() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasRole(String string, String string1, String string2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addRole(String string, String string1, String string2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean inGroup(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addGroup(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeGroup(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeRole(String string, String string1, String string2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void checkRole(String string, String string1, String string2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void checkGroup(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void checkPermission(Object o, String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void filterByPermission(Collection<?> clctn, String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasPermission(Object o, String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<Role> getRoles() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<Group> getGroups() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Class<Authenticator> getAuthenticatorClass() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setAuthenticatorClass(Class<Authenticator> type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getAuthenticatorName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setAuthenticatorName(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
