/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import javax.faces.context.FacesContext;
import junit.framework.TestCase;
import mail.server.MailServer;
import mail.web.MailSession;
import org.jboss.seam.security.Authenticator.AuthenticationStatus;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.CredentialsImpl;

/**
 *
 * @author timoteo
 */
public class LoginHandlerTest extends TestCase {

    MailServer server = new MailServer();
    MailSession mailSession = new MailSession(server);
    Credentials credentials = new CredentialsMock();
    FacesContext context = new FacesContextMock();
    LoginHandler loginHandler = new LoginHandler(mailSession, credentials, context);

    public void testAuthenticateValidUser() {
        server.addAccountsServer("guest");
        credentials.setUsername("guest");
        loginHandler.authenticate();
        assertEquals(loginHandler.getStatus(), AuthenticationStatus.SUCCESS);
    }

    public void testAuthenticateInvalidUser() {
        credentials.setUsername("invalid" + System.currentTimeMillis());
        loginHandler.authenticate();
        assertEquals(loginHandler.getStatus(), AuthenticationStatus.FAILURE);
    }
}
