/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import org.jboss.seam.security.Credentials;
import org.picketlink.idm.api.Credential;

/**
 *
 * @author timoteo
 */
public class CredentialsMock implements Credentials {

    private String username;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String string) {
        this.username = string;
    }

    @Override
    public Credential getCredential() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setCredential(Credential crdntl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSet() {
        return username != null;
    }

    @Override
    public boolean isInvalid() {
        return username == null;
    }

    @Override
    public void invalidate() {
    }

    @Override
    public void clear() {
    }
}
