/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseStream;
import javax.faces.context.ResponseWriter;
import javax.faces.render.RenderKit;

/**
 *
 * @author timoteo
 */
public class FacesContextMock extends FacesContext {

    @Override
    public Application getApplication() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<String> getClientIdsWithMessages() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ExternalContext getExternalContext() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Severity getMaximumSeverity() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<FacesMessage> getMessages() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<FacesMessage> getMessages(String clientId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RenderKit getRenderKit() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean getRenderResponse() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean getResponseComplete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ResponseStream getResponseStream() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setResponseStream(ResponseStream responseStream) {
    }

    @Override
    public ResponseWriter getResponseWriter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setResponseWriter(ResponseWriter responseWriter) {
    }

    @Override
    public UIViewRoot getViewRoot() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setViewRoot(UIViewRoot root) {
    }

    @Override
    public void addMessage(String clientId, FacesMessage message) {
    }

    @Override
    public void release() {
    }

    @Override
    public void renderResponse() {
    }

    @Override
    public void responseComplete() {
    }
}