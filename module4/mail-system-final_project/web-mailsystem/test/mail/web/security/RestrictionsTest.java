/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import junit.framework.TestCase;
import org.jboss.seam.security.Identity;

/**
 *
 * @author timoteo
 */
public class RestrictionsTest extends TestCase {
    

    public void testValidUser() {
        Restrictions restrictions = new Restrictions();
        Identity identity = new IdentityMock("guest");        
        assertTrue(restrictions.usersChecker(identity));
    }
    
    public void testInvalidUser() {
        Restrictions restrictions = new Restrictions();
        Identity identity = new IdentityMock(null);        
        assertFalse(restrictions.usersChecker(identity));
    }
}
