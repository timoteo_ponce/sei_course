/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import javax.faces.context.FacesContext;
import junit.framework.TestCase;
import mail.server.MailServer;
import mail.web.security.FacesContextMock;

/**
 *
 * @author timoteo
 */
public class RegisterBeanTest extends TestCase {

    private MailServer mailServer = new MailServer();
    private FacesContext context = new FacesContextMock();
    RegisterBean registerBean = new RegisterBean(mailServer, context);

    public void testRegisterInvalidUser() {        
        registerBean.setUsername("");
        assertEquals(registerBean.registerUser(), "failed");
    }
    
    public void testRegisterAlreadyRegisteredUser() {        
        mailServer.addAccountsServer("guest");
        registerBean.setUsername("guest");
        assertEquals(registerBean.registerUser(), "failed");
    }
    
    public void testRegisterValidUser() {                
        registerBean.setUsername("guest"+System.currentTimeMillis());
        assertEquals(registerBean.registerUser(), "success");
    }
}
