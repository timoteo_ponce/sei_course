/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import mail.attribute.MailItem;
import mail.attribute.NullMailItem;
import mail.attribute.attach.Attachment;
import mail.web.security.Users;

/**
 *
 * @author timoteo
 */
@Named
@SessionScoped
@Users
public class MailListActions implements Serializable {

    private static final Logger logger = Logger.getLogger(MailListActions.class.getName());
    @Inject
    private MailSession mailSession;
    private MailDto editMail = new MailDto(NullMailItem.NULL_ITEM);
    private AttachmentContainer attachmentContainer = new AttachmentContainer();

    public MailListActions() {
    }

    public MailListActions(MailSession mailSession) {
        this.mailSession = mailSession;
    }

    public void delete(MailItem mailItem) {
        mailSession.delete(mailItem);
        attachmentContainer.updateAttachments(null);
        logger.info("Mail deleted : " + mailItem);
    }

    public void deleteAll() {
        mailSession.deleteAll();
        attachmentContainer.updateAttachments(null);
        logger.info("All mails deleted");
    }

    public void startNew() {
        MailItem item = new MailItem(mailSession.getUser(), "", "", "");
        this.editMail = new MailDto(item);
        attachmentContainer.updateAttachments(item);
    }

    public void startReply(MailItem mailItem) {
        this.editMail = new MailDto(mailItem).asReply();
        attachmentContainer.updateAttachments(mailItem);
    }

    public void startForward(MailItem mailItem) {
        this.editMail = new MailDto(mailItem).asForward();
        attachmentContainer.updateAttachments(mailItem);
    }

    public void send() {
        MailItem item = editMail.getUpdatedMailItem();
        for (Attachment attachment : attachmentContainer.getAttachmentsList()) {
            item.getMessage().getAttachs().add(attachment);
        }
        mailSession.send(editMail.getUpdatedMailItem());
        logger.info("Mail sent : " + editMail);
    }

    public MailDto getEditMail() {
        return editMail;
    }

    public AttachmentContainer getAttachmentContainer() {
        return attachmentContainer;
    }
}
