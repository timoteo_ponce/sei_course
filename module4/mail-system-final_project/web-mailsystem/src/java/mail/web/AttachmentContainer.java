/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import mail.attribute.MailItem;
import mail.attribute.attach.Attachment;
import mail.attribute.attach.AttachmentsList;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author timoteo
 */
public class AttachmentContainer {

    private static final Logger logger = Logger.getLogger(AttachmentContainer.class.getName());
    private AttachmentsList attachments;
    private Integer selectedAttachment;
    private List<SelectItem> attachmentList = new ArrayList<SelectItem>();
    private StreamedContent attachmentFile;

    public void updateAttachments(MailItem selectedMail) {
        if (selectedMail == null) {
            this.attachments = null;
        } else {
            this.attachments = selectedMail.getMessage().getAttachs();
        }
        reloadAttachmentList();
    }

    private SelectItem createSelectItem(int position, Attachment attachment) {
        return new SelectItem(position, attachment.getFileName());
    }

    public Integer getSelectedAttachment() {
        return selectedAttachment;
    }

    public void setSelectedAttachment(Integer selectedAttachment) {
        this.selectedAttachment = selectedAttachment;
        if (selectedAttachment != null) {
            Attachment attachment = getAttachment();
            InputStream stream = new ByteArrayInputStream(attachment.getData());
            attachmentFile = new DefaultStreamedContent(stream, "application/pdf", attachment.getFileName());
        }
    }

    Attachment getAttachment() {
        return attachments.getAttachFile(selectedAttachment);
    }

    public StreamedContent getAttachmentFile() {
        return attachmentFile;
    }

    public List<SelectItem> getAttachments() {
        return attachmentList;
    }

    public List<Attachment> getAttachmentsList() {
        return attachments.getAttachmentsList();
    }

    public void uploadFile(FileUploadEvent event) {
        UploadedFile uploadedFile = event.getFile();
        Attachment attachment = new Attachment(uploadedFile.getFileName(), uploadedFile.getContents());
        this.attachments.add(attachment);
        reloadAttachmentList();
        logger.info("Attachment added : " + uploadedFile.getFileName());
    }

    private void reloadAttachmentList() {
        attachmentList.clear();
        attachmentList.add(new SelectItem(null, "..."));
        if (attachments != null) {
            List<Attachment> attachmentsList = attachments.getAttachmentsList();
            for (int i = 0; i < attachmentsList.size(); i++) {
                Attachment attachment = attachmentsList.get(i);
                attachmentList.add(createSelectItem(i, attachment));
            }
        }
    }

    public void remove() {
        if (selectedAttachment != null) {
            attachments.remove(getAttachment());
            reloadAttachmentList();
            logger.info("Attachment removed : " + selectedAttachment);
        }
    }
}
