/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import mail.account.UserAccount;
import mail.attribute.MailItem;
import mail.attribute.MailMessage;

/**
 *
 * @author timoteo
 */
public class MailDto {

    private String sender;
    private String receiver;
    private String subject;
    private String content;
    private MailItem mailItem;

    public MailDto(MailItem mailItem) {
        this.mailItem = mailItem;
        sender = mailItem.getFrom().toString();
        receiver = mailItem.getTo().toString();
        subject = mailItem.getSubject();
        content = mailItem.getMessage().getMessage().toString();
    }

    public MailItem getUpdatedMailItem() {
        MailMessage message = this.mailItem.getMessage();
        message.setContent(content);
        this.mailItem = new MailItem(new UserAccount(sender), new UserAccount(receiver), subject, message);
        return mailItem;
    }

    public MailDto asReply() {
        receiver = mailItem.getFrom().toString();
        sender = mailItem.getTo().toString();
        subject = "RE: " + mailItem.getSubject();
        return this;
    }
    
    public MailDto asForward() {
        sender = mailItem.getTo().toString();
        subject = "FWD: " + mailItem.getSubject();
        return this;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
