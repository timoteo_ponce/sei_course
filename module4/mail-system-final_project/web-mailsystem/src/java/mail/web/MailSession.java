/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import mail.attribute.MailItem;
import mail.client.MailClient;
import mail.server.MailServer;

/**
 *
 * @author timoteo
 */
@Named
@SessionScoped
public class MailSession implements Serializable {

    @Inject
    private MailServer server;
    private MailClient client;
    private String user = "guest";
    private String password;

    public MailSession() {
    }

    public MailSession(MailServer server) {
        this.server = server;
    }

    public List<MailItem> getAllMails() {
        return client.getAllMails();
    }

    void delete(MailItem mailItem) {
        client.deleteMailItem(mailItem);
    }

    void deleteAll() {
        client.deleteAllMails();
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    void send(MailItem editMailItem) {
        client.sendMailItem(editMailItem);
    }

    public boolean authenticate(String username) throws Exception {
        boolean validAccount = server.existsAccountInServer(username);
        if (validAccount) {
            this.user = username;
            client = new MailClient(server, user);
        }
        return validAccount;
    }
}
