/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import org.jboss.seam.faces.view.config.AccessDeniedView;
import org.jboss.seam.faces.view.config.LoginView;
import org.jboss.seam.faces.view.config.ViewPattern;

/**
 *
 * @author timoteo
 */
@org.jboss.seam.faces.view.config.ViewConfig
public interface ViewConfig {

    static enum Pages {

        @ViewPattern("/mail_*")
        @Users
        @LoginView("/index.xhtml")
        @AccessDeniedView("/index.xhtml")
        ALL;
    }
}
