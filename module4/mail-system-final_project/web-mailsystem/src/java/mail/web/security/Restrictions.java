/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web.security;

import org.jboss.seam.security.Identity;
import org.jboss.seam.security.annotations.Secures;

/**
 *
 * @author timoteo
 */
public class Restrictions {

    public @Secures
    @Users
    boolean usersChecker(Identity identity) {
        return identity.isLoggedIn();
    }
}
