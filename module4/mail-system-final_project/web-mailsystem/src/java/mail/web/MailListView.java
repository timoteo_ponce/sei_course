/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import mail.attribute.MailItem;
import mail.web.security.Users;

/**
 *
 * @author timoteo
 */
@Named
@SessionScoped
@Users
public class MailListView implements Serializable {

    @Inject
    private MailSession mailSession;
    private MailItem selectedMail;
    private AttachmentContainer attachmentContainer = new AttachmentContainer();

    public MailListView() {
    }

    public MailListView(MailSession mailSession) {
        this.mailSession = mailSession;
    }

    public List<MailItem> getMailsList() {
        return mailSession.getAllMails();
    }

    public MailItem getSelectedMail() {
        return selectedMail;
    }

    public void setSelectedMail(MailItem selectedMail) {
        this.selectedMail = selectedMail;
        attachmentContainer.updateAttachments(selectedMail);
    }

    public AttachmentContainer getAttachmentContainer() {
        return attachmentContainer;
    }
}
