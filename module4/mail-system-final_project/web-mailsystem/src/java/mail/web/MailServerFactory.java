/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import mail.server.MailServer;
import mail.server.ServiceInitializer;

/**
 *
 * @author timoteo
 */
@Named
@SessionScoped
public class MailServerFactory implements Serializable {

    @Produces
    @ApplicationScoped
    public MailServer getMailServer() throws IOException {
        Logger.getLogger(getClass().getName()).info("Starting mailServer");
        ServiceInitializer.init();
        return new MailServer();
    }
}
