//---------------------------------------------
// Example program to test DNS resolution in C# and .NET
// Bill Nolde 2002
// billnolde@ieee.org
//-------------------------------
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Runtime.InteropServices ;





namespace CSDNS
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		[DllImport("user32.dll")]
		public static extern bool  MessageBeep(int Sound);
		[DllImport("user32.dll")]
		public static extern int   MessageBox(int hWnd, String text, 
			                           String caption, uint type);

		
		
		private System.Windows.Forms.TextBox m_host_name;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox m_ip_address;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox m_status;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button ok_button;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.TextBox m_resolve_text;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_host_name = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.m_ip_address = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.m_status = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ok_button = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.m_resolve_text = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// m_host_name
			// 
			this.m_host_name.Location = new System.Drawing.Point(16, 80);
			this.m_host_name.Name = "m_host_name";
			this.m_host_name.Size = new System.Drawing.Size(440, 22);
			this.m_host_name.TabIndex = 0;
			this.m_host_name.Text = "www.codeguru.com";
			this.m_host_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(192, 48);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Host Name";
			// 
			// m_ip_address
			// 
			this.m_ip_address.Location = new System.Drawing.Point(136, 144);
			this.m_ip_address.Name = "m_ip_address";
			this.m_ip_address.Size = new System.Drawing.Size(184, 22);
			this.m_ip_address.TabIndex = 2;
			this.m_ip_address.Text = "";
			this.m_ip_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(192, 120);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "IP Address";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(520, 104);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(136, 23);
			this.button1.TabIndex = 4;
			this.button1.Text = "Use Host Name";
			this.button1.Click += new System.EventHandler(this.HostNameButtonClick);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(520, 136);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(136, 23);
			this.button2.TabIndex = 5;
			this.button2.Text = "Use IP Address";
			this.button2.Click += new System.EventHandler(this.IPAddressButtonClick);
			// 
			// m_status
			// 
			this.m_status.Location = new System.Drawing.Point(24, 232);
			this.m_status.Name = "m_status";
			this.m_status.Size = new System.Drawing.Size(640, 22);
			this.m_status.TabIndex = 6;
			this.m_status.Text = "";
			this.m_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(304, 200);
			this.label3.Name = "label3";
			this.label3.TabIndex = 7;
			this.label3.Text = "Status";
			// 
			// ok_button
			// 
			this.ok_button.Location = new System.Drawing.Point(581, 24);
			this.ok_button.Name = "ok_button";
			this.ok_button.TabIndex = 8;
			this.ok_button.Text = "OK";
			this.ok_button.Click += new System.EventHandler(this.OkButtonClick);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(581, 56);
			this.button3.Name = "button3";
			this.button3.TabIndex = 9;
			this.button3.Text = "About";
			this.button3.Click += new System.EventHandler(this.AboutButtonClick);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(528, 184);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(88, 23);
			this.button4.TabIndex = 10;
			this.button4.Text = "Resolve";
			this.button4.Click += new System.EventHandler(this.ResolveButtonClick);
			// 
			// m_resolve_text
			// 
			this.m_resolve_text.Location = new System.Drawing.Point(368, 184);
			this.m_resolve_text.Name = "m_resolve_text";
			this.m_resolve_text.Size = new System.Drawing.Size(136, 22);
			this.m_resolve_text.TabIndex = 11;
			this.m_resolve_text.Text = "";
			this.m_resolve_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(696, 267);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.m_resolve_text,
																		  this.button4,
																		  this.button3,
																		  this.ok_button,
																		  this.label3,
																		  this.m_status,
																		  this.button2,
																		  this.button1,
																		  this.label2,
																		  this.m_ip_address,
																		  this.label1,
																		  this.m_host_name});
			this.Name = "Form1";
			this.Text = "Bill Nolde\'s C# DNS Lookup example 1.0";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
//-------------------------------------------------------------------
private void OkButtonClick(object sender, System.EventArgs e)
{
		Application.Exit();
}
//-------------------------------------------------------
private void HostNameButtonClick(object sender, System.EventArgs e)
{
	m_status.Text		 = "Trying....";
	m_status.Update();
	try 
	{
		IPHostEntry hostInfo = Dns.GetHostByName(m_host_name.Text);
		m_ip_address.Text	 = hostInfo.AddressList[0].ToString();
		m_status.Text		 = "ok";
	}
	catch (Exception excpt)
	{
		m_status.Text = excpt.Message;
		m_ip_address.Text = "";
	}
	MessageBeep(1000);
}
//------------------------------------------------------------------
private void IPAddressButtonClick(object sender, System.EventArgs e)
{
IPAddress ipaddr =  IPAddress.Parse(m_ip_address.Text);
	m_status.Text		 = "Trying....";
	m_status.Update();
	try 
	{
		IPHostEntry hostInfo = Dns.GetHostByAddress (ipaddr);
		m_host_name.Text	 = hostInfo.HostName;
		m_status.Text		 = "ok";
	}
	catch (Exception excpt)
	{
		m_status.Text    = excpt.Message; 
		m_host_name.Text = "";
	}		
	MessageBeep(1000);
}
//--------------------------------------------------------------
private void ResolveButtonClick(object sender, System.EventArgs e)
{
	m_status.Text		 = "Trying....";
	m_status.Update();
	try 
	{
		IPHostEntry hostInfo = Dns.Resolve(m_resolve_text.Text);
		m_ip_address.Text	 = hostInfo.AddressList[0].ToString();
		m_host_name.Text	 = hostInfo.HostName;
		m_status.Text		 = "ok";
	}
	catch (Exception excpt)
	{
		m_status.Text = excpt.Message;
		m_ip_address.Text = "";
	}
	MessageBeep(1000);		
}
//--------------------------------------------------------------
private void AboutButtonClick(object sender, System.EventArgs e)
{
	MessageBox(0,"Host Address Resolution Example" +
		         "\n      for the .NET Framework" +
		         "\n              Bill Nolde" +
				 "\n        billnolde@ieee.org","",0);

}
//-------------------------------------------------------------------
	} //end class
}    //end namespace
