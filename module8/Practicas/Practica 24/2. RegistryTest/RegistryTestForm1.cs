//---------------------------------------------
// Example program to test Registry functions in C# and .NET
// Bill Nolde 2002
// billnolde@ieee.org
//-------------------------------
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Microsoft.Win32;
using System.Runtime.InteropServices;

namespace RegistryTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		[DllImport("user32.dll")]
		public static extern bool  MessageBeep(int Sound);
		[DllImport("user32.dll")]
		public static extern int   MessageBox(int hWnd, String text, 
			String caption, uint type);

		
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox m_current_key;
		private System.Windows.Forms.ListBox m_subkey_list;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.TextBox m_status;
		private System.Windows.Forms.ComboBox m_root_key_listbox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		listView1.View = View.Details;
        listView1.GridLines = true;
		listView1.Columns.Add("Name",  120, HorizontalAlignment.Left);
        listView1.Columns.Add("Type",  75, HorizontalAlignment.Left);
        listView1.Columns.Add("Value", 700, HorizontalAlignment.Left);

		GetKeysAndValues();		//Initialize ListBox
		m_root_key_listbox.Items.Add(Registry.ClassesRoot.Name);
		m_root_key_listbox.Items.Add(Registry.CurrentConfig.Name);
		m_root_key_listbox.Items.Add(Registry.CurrentUser.Name);
		m_root_key_listbox.Items.Add(Registry.LocalMachine.Name);
		m_root_key_listbox.Items.Add(Registry.Users.Name);
		m_root_key_listbox.SelectedIndex = 2;
		m_root_key_listbox.Focus();
//		listView1.Click += (EventHandler)new EventHandler(this.MouseClick);
  

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		}
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.m_subkey_list = new System.Windows.Forms.ListBox();
			this.m_current_key = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.m_status = new System.Windows.Forms.TextBox();
			this.m_root_key_listbox = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.listView1 = new System.Windows.Forms.ListView();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(760, 16);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.TabStop = false;
			this.button1.Text = "Exit";
			this.button1.Click += new System.EventHandler(this.ExitButtonClick);
			// 
			// m_subkey_list
			// 
			this.m_subkey_list.ItemHeight = 16;
			this.m_subkey_list.Location = new System.Drawing.Point(32, 144);
			this.m_subkey_list.Name = "m_subkey_list";
			this.m_subkey_list.Size = new System.Drawing.Size(288, 180);
			this.m_subkey_list.TabIndex = 2;
			this.m_subkey_list.TabStop = false;
			this.m_subkey_list.SelectedIndexChanged += new System.EventHandler(this.SubkeyListBoxIndexChanged);
			// 
			// m_current_key
			// 
			this.m_current_key.Location = new System.Drawing.Point(48, 88);
			this.m_current_key.Name = "m_current_key";
			this.m_current_key.Size = new System.Drawing.Size(664, 22);
			this.m_current_key.TabIndex = 3;
			this.m_current_key.TabStop = false;
			this.m_current_key.Text = "";
			this.m_current_key.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(128, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "Keys";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(328, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 8;
			this.label2.Text = "Current Key";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(408, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 9;
			this.label3.Text = "Values";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(336, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 11;
			this.label4.Text = "Root Key";
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.Red;
			this.button4.Image = ((System.Drawing.Bitmap)(resources.GetObject("button4.Image")));
			this.button4.Location = new System.Drawing.Point(352, 176);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(32, 32);
			this.button4.TabIndex = 12;
			this.button4.Click += new System.EventHandler(this.RemoveKeyButtonClick);
			// 
			// m_status
			// 
			this.m_status.Location = new System.Drawing.Point(120, 336);
			this.m_status.Multiline = true;
			this.m_status.Name = "m_status";
			this.m_status.Size = new System.Drawing.Size(632, 40);
			this.m_status.TabIndex = 13;
			this.m_status.TabStop = false;
			this.m_status.Text = "";
			this.m_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// m_root_key_listbox
			// 
			this.m_root_key_listbox.Location = new System.Drawing.Point(272, 32);
			this.m_root_key_listbox.Name = "m_root_key_listbox";
			this.m_root_key_listbox.Size = new System.Drawing.Size(240, 24);
			this.m_root_key_listbox.TabIndex = 14;
			this.m_root_key_listbox.SelectedIndexChanged += new System.EventHandler(this.RootKeyComboboxIndexChanged);
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.DarkBlue;
			this.label5.Location = new System.Drawing.Point(352, 216);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 15;
			this.label5.Text = "UP";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.listView1.FullRowSelect = true;
			this.listView1.GridLines = true;
			this.listView1.Location = new System.Drawing.Point(416, 144);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(416, 184);
			this.listView1.TabIndex = 16;
			this.listView1.View = System.Windows.Forms.View.Details;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(760, 64);
			this.button2.Name = "button2";
			this.button2.TabIndex = 17;
			this.button2.Text = "About";
			this.button2.Click += new System.EventHandler(this.AboutButtonClick);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(856, 391);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.button2,
																		  this.listView1,
																		  this.label5,
																		  this.m_root_key_listbox,
																		  this.m_status,
																		  this.button4,
																		  this.label4,
																		  this.label3,
																		  this.label2,
																		  this.label1,
																		  this.m_current_key,
																		  this.m_subkey_list,
																		  this.button1});
			this.Name = "Form1";
			this.Text = "Registry Test Program in C#   2002  ";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
//------------------------------------------------------------
private void ExitButtonClick(object sender, System.EventArgs e)
{
	Application.Exit();
}
//-------------------------------------------------------------
private void SubkeyListBoxIndexChanged(object sender, System.EventArgs e)
{
	String newkey =  m_subkey_list.SelectedItem.ToString();		
	if (m_current_key.Text.Length == 0)	m_current_key.Text = newkey;
	else
		m_current_key.Text =m_current_key.Text + "\\" + newkey;
	GetKeysAndValues();
}
//---------------------------------------------------------
private void RemoveKeyButtonClick(object sender, System.EventArgs e)
{
	String new_key	= m_current_key.Text;
	int nDx			= new_key.LastIndexOf('\\');
	if (nDx == -1) m_current_key.Text = "";
	else	       m_current_key.Text = new_key.Substring(0,nDx); 	
	GetKeysAndValues();
}
//----------------------------------------------------------
private void RootKeyComboboxIndexChanged(object sender, System.EventArgs e)
{
	m_root_key_listbox.Text = m_root_key_listbox.SelectedItem.ToString();
	m_current_key.Text = "";		//we're at the root
	GetKeysAndValues();				//get top level keys
}
//-------------------------------------------------------
void GetKeysAndValues()
{
	RegistryKey rkey =  GetSelectedKey();
try
{
	m_root_key_listbox.Text = rkey.Name.ToString();
	int nDx = m_root_key_listbox.Text.IndexOf('\\');
	if (nDx != -1) 	m_root_key_listbox.Text = rkey.Name.Substring(0,nDx);

	m_subkey_list.Items.Clear();
	string[] sknames = rkey.GetSubKeyNames();
	foreach(String str in sknames ) {
		String vstring = (String) rkey.GetValue(str, typeof(string).ToString());
		m_subkey_list.Items.Add(str);
	}
	m_status.Text = "OK";
	DisplayTypesAndValues(rkey);
	rkey.Close();
}
	catch (Exception except)
	{
		m_status.Text = "Exception occured " + except.Message;
	}
}
//------------------------------------------------------
RegistryKey GetSelectedKey()
{
	RegistryKey rkey =  null;
try 
{
	int nDx = m_root_key_listbox.SelectedIndex;
	switch (nDx)
	{
		case 0:
			rkey = Registry.ClassesRoot.OpenSubKey(m_current_key.Text);
			break;
		case 1:
			rkey = Registry.CurrentConfig.OpenSubKey(m_current_key.Text);
			break;
		case 2:
			rkey = Registry.CurrentUser.OpenSubKey(m_current_key.Text);
			break;
		case 3:
			rkey = Registry.LocalMachine.OpenSubKey(m_current_key.Text);
			break;
		case 4:
			rkey = Registry.Users.OpenSubKey(m_current_key.Text);
			break;
		default:
			break;
	}
}
	catch (Exception except)
	{
		m_status.Text = "Exception occured " + except.Message;
	}
	return rkey;
}
//-------------------------------------------------------
void DisplayTypesAndValues(RegistryKey rkey)
{
	listView1.Items.Clear();
	string[] SubKeys  = rkey.GetValueNames();
	foreach(String vstr in SubKeys ) {			//subkey value name
		object robj =  rkey.GetValue(vstr, typeof(string));
		String vstring  = "";
		String vtype    = "";
		GetTypeAndValue(robj, ref vtype, ref vstring);
		ListViewItem item1 = new ListViewItem();
		if (vstr == "" ) item1.Text= "Default";
		else			 item1.Text= vstr;

		listView1.Items.Add(item1);	
		item1.SubItems.Add(vtype);
        item1.SubItems.Add(vstring);
	}
}
//-------------------------------------------------------
void GetTypeAndValue(object robj, ref String Type, ref String Value)
{
	Value  = robj.ToString();    //get object value 
	Type    = robj.GetType().ToString();  //get object type
	Type    = Type.Substring(7, Type.Length-7); //strip off "System."

	if (Type == "Byte[]") {
		Value = "";
		byte[] Bytes = (byte[]) robj;
		foreach(byte bt in Bytes) {
			string hexval = bt.ToString();
			if (hexval == "") hexval = "0";
			Value         = Value + hexval+ " ";
		}
	}
}
//-----------------------------------------------------
private void AboutButtonClick(object sender, System.EventArgs e)
{
	MessageBox(0,"Registry Test Example" +
		         "\n      for the .NET Framework" +
		         "\n              Bill Nolde" +
				 "\n        billnolde@ieee.org","",0);
}
//------------------------------------------------------
	}  //end class
}     //end namespace
