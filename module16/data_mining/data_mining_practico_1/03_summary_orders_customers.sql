SELECT
	Customers.Country as "customerCountry",
	Customers.City as "customerCity",
	Customers.CustomerID,
	(SELECT COUNT(*) FROM Orders o WHERE o.CustomerID = Customers.CustomerID ) as amount_orders,
	(SELECT COUNT(od.Quantity) FROM  "Order Details" od, Orders o 
		WHERE od.OrderID = o.OrderID
		AND o.CustomerID = Customers.CustomerID) as amount_products,
	(SELECT 
		CASE 
			when mycount < 10 then 'eventual'
			when mycount >= 10 AND mycount < 20 then 'frequent'
			when mycount >= 20 then 'loyal'
		END
	FROM (SELECT COUNT(*) as mycount FROM Orders o WHERE o.CustomerID = Customers.CustomerID) as testt) as myclass
FROM Orders 
	INNER JOIN "Order Details"
		ON Orders.OrderID = "Order Details".OrderID
	INNER JOIN Customers
		ON Customers.CustomerID = Orders.CustomerID
GROUP BY Customers.Country, Customers.City,Customers.CustomerID,
	Customers.CompanyName