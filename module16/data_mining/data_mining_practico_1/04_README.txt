Clasificación
-------------
	PreProcess: Dejar los campos
		- country
		- customer
		- orders
		- products
		- loyalty
		- discount
	
	Classifier: J48

Clustering
-------------
	PreProcess: Igual que en clasificación
	
	Clusterer: SimpleKMeans

Asociación
-------------
	PreProcess:
		- Eliminar todos los datos numericos, dejando al final solamente
			- country
			- customer
			- loyalty
			- discount

	Associator: APriori
